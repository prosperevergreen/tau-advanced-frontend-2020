interface Result {
  periodLength: number
  trainingDays: number
  success: boolean
  rating: number
  ratingDescription: string
  target: number
  average: number
}

const calculateExercises = (dailyHours: number[], target: number): Result => {
  const periodLength = dailyHours.length;
  const trainingDays = dailyHours.filter(d => d !== 0).length;
  const average = dailyHours.reduce((acc, d) => acc + d, 0) / periodLength;

  let rating: number;
  if(average >= target) {
    rating = 3;
  } else if(Math.round(target - average) === 0) {
    rating = 2;
  } else {
    rating = 1;
  }

  let ratingDescription: string;
  switch(rating) {
    case 3:
      ratingDescription = "awesome!";
      break;
    case 2:
      ratingDescription = "not too bad but could be better";
      break;
    case 1:
    default:
      ratingDescription = "not so great";
      break;
  }

  return {
    periodLength,
    trainingDays,
    success: rating === 3,
    rating,
    ratingDescription,
    target,
    average
  };
};

export { calculateExercises };
