/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import express, { Request, Response } from 'express';
import { calculateExercises } from './exercise-calculator';

const app = express();
app.use(express.json());

app.post("/calc", (req: Request, res: Response): Response => {
  const dailyHours = req.body["daily_exercises"];
  const target = req.body["target"];

  if(!dailyHours || !target)
    return res.status(400).json({
      error: "parameters missing"
    });

  if(isNaN(Number(target)) 
    || !Array.isArray(dailyHours)
    || dailyHours.reduce((acc: boolean, h) => acc || isNaN(Number(h)), false)) 
  {
    return res.status(400).json({
      error: "malformatted parameters"
    });
  }

  return res.status(200)
    .json(calculateExercises(dailyHours, target));
});

const PORT = 3003;
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
