interface ExerciseData {
	periodLength: number;
	trainingDays: number;
	success: boolean;
	rating: number;
	ratingDescription: string;
	target: number;
	average: number;
}

const calculateExercises = (
	data: Array<number>,
	target: number
): ExerciseData => {
	const average =
		data.reduce((accumulator, currentValue) => accumulator + currentValue) /
		data.length;
	const success = average >= target;
	const ratingDescription = success
		? "well done!!! keep it up."
		: "not too bad but could be better";

	return {
		periodLength: data.length,
		trainingDays: data.filter((value) => value != 0).length,
		success,
		rating: Math.floor((average / target) * 3),
		ratingDescription,
		target,
		average,
	};
};

export { calculateExercises };
