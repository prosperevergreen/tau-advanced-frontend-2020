import express from "express";
import { calculateExercises } from "./exercise-calculator";

const app = express();
app.use(express.json());

// ...
app.post("/exercises", (req, res) => {
	// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
	const { target, daily_exercises } = req.body;

	if (!target || !daily_exercises)
		return res.status(400).json({
			error: "parameters missing",
		});
    
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	if (typeof target !== "number" || !Array.isArray(daily_exercises) || daily_exercises.some(function(el: any) {return typeof el !== 'number';}))
		return res.status(400).json({
			error: "malformatted parameters",
		});
  
  const data = calculateExercises(daily_exercises, target);
	return res.json(data);
});

const PORT = 3003;
app.listen(PORT, () => {
	console.log(`Server running on port ${PORT}`);
});
