import { calculateExercises } from "./exercise-calculator";

/*

>>> test case 1:

npm start cli  2 3 0 2 4.5 0 3 1


{ 
  periodLength: 7,
  trainingDays: 5,
  success: false,
  rating: 2,
  ratingDescription: 'not too bad but could be better',
  target: 2,
  average: 1.9285714285714286 
}

>>> test case 2:

npm start cli 2 1 0 2 4.5 0 3 1 0 4

{ periodLength: 9,
  trainingDays: 6,
  success: false,
  rating: 2,
  ratingDescription: 'not too bad but could be better',
  target: 2,
  average: 1.7222222222222223 
}

*/

interface ExerciseValues {
	data: Array<number>;
	target: number;
}

const parseArguments = (args: Array<string>): ExerciseValues => {
	if (args.length < 4) throw new Error("Not enough arguments");

	const [, , ...params] = args;
	const paramsNumber = params.map((el) => {
		if (isNaN(Number(el))) throw new Error("Provided values were not numbers!");
		return Number(el);
	});
	const [target, ...data] = paramsNumber;
	return {
		target,
		data,
	};
};

try {
	const { data, target } = parseArguments(process.argv);
	console.log(calculateExercises(data, target));
} catch (e) {
	console.log("Error, something bad happened, message: ", e);
}
