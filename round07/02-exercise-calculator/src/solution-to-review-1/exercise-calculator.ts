export interface IResult {
    periodLength: number,
    trainingDays: number,
    success: boolean,
    rating: number,
    ratingDescription: string,
    target: number,
    average: number
}


const calculateExercises = (exerciseLog: number[], target: number): IResult => {
    const trainingDays: number = exerciseLog.filter(hours => hours > 0).length;
    const average: number = exerciseLog.reduce((totalHours, hours) => totalHours + hours, 0) / exerciseLog.length;

    const [rating, ratingDescription] = getRating(target - average);

    return {
        periodLength: exerciseLog.length,
        trainingDays,
        success: average >= target,
        rating, 
        ratingDescription,
        target,
        average
    };
};

// The return type is Tuple
const getRating = (difference: number): [number, string] => {
    if (difference > 2) {
        return [3, "you did good"];
    } else if (difference > 0) {
        return [2, "you did the least amount to pass"];
    } else if (difference > -0.5) {
        return [2, "almost there"];
    }
    return [1, "you need to try harder"];
};


export { calculateExercises };
