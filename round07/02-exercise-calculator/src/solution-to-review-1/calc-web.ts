import express from 'express';
import { calculateExercises, IResult } from './exercise-calculator';

const app = express();
app.use(express.json());

type TRequestBody = {
  daily_exercises: number[],
  target: number
};

type TError = {
  error: string
};

app.post("/calc", (request: express.Request<null, null, TRequestBody>, response: express.Response<IResult | TError>) => {
  try {
    if (request.body && (!request.body.daily_exercises || !request.body.target)) {
      response.send({ error: "parameters missing" });
    } else if (request.body) {
      const containsNonString: boolean = request.body.daily_exercises.find(
        (hours: number) => typeof hours !== "number"
      ) !== undefined;
      if (containsNonString) {
        throw new Error("Invalid parameters");
      }
      const exerciseLog: number[] = request.body.daily_exercises;
      const target = Number(request.body.target);
      response.send(calculateExercises(exerciseLog, target));
    }
  } catch (error) {
    console.error(error);
    response.send({ error: "malformatted parameters" });
  }
});



const PORT = 3003;
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
