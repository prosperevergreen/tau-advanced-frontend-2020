
import React from 'react';
import { CoursePart } from './course-data'

interface HeaderProps {
  courseName: string;
}

interface ContentProps {
  courseParts: Array<CoursePart>;
}

interface PartProps {
  coursePart: CoursePart;
}

const assertNever = (value: never): never => {
  throw new Error(
    `Unhandled discriminated union member: ${JSON.stringify(value)}`
  );
};

export const Header = (props: HeaderProps) => {

  return (
    <div>
      <h1>{props.courseName}</h1>
    </div>
  );

};

export const Part = (props: PartProps) => {
  switch (props.coursePart.type) {
    case "normal":
      return (
        <p>
          <b>{props.coursePart.name} {props.coursePart.exerciseCount}</b>
          <br></br>
          <i>{props.coursePart.description}</i>
        </p>
      );
    case "groupProject":
      return (
        <p>
          <b>{props.coursePart.name} {props.coursePart.exerciseCount}</b>
          <br></br>
          <i>project exercises {props.coursePart.groupProjectCount}</i>
        </p>
      );
    case "submission":
      return (
        <p>
          <b>{props.coursePart.name} {props.coursePart.exerciseCount}</b>
          <br></br>
          <i>submit to {props.coursePart.exerciseSubmissionLink}</i>
        </p>
      );
    case "special":
      return (
        <p>
          <b>{props.coursePart.name} {props.coursePart.exerciseCount}</b>
          <br></br>
          <i>{props.coursePart.description}</i>
          <br></br>
          <span>required skills: {props.coursePart.requirements.toString()}</span>
        </p>
      );
    default:
      return assertNever(props.coursePart);
  }
}

export const Content = (props: ContentProps) => {

  return (
    <div>
     {props.courseParts.map((coursePart) => (
        <Part key={coursePart.name} coursePart={coursePart} />
    ))}
    </div>
  );

};



export const Total = (props: ContentProps) => {

  return (
    <div>
      <p>
        Number of exercises{" "}
        {props.courseParts.reduce((carry, part) => carry + part.exerciseCount, 0)}
      </p>
    </div>
  );

};
