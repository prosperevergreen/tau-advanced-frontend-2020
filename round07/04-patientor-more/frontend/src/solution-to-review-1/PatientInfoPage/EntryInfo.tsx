import React from "react";
import { Entry } from "../types";

interface EntryProps {
  entry: Entry;
}

export const EntryInfo = (props: EntryProps) => {

  const diagnosisCodes = () => {
    if (props.entry.diagnosisCodes) {
      return (<ul>
        {props.entry.diagnosisCodes.map((code: string) => (
          <li key={code}>{code}</li>
        ))}
      </ul>);
    } else {
      return null;
    }
  };

  return (
    <div>
      <p>{props.entry.date} <i>{props.entry.description}</i></p>
      {diagnosisCodes()}
    </div>
  );
};