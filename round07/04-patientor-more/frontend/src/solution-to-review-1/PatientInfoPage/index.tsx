import React from "react";
import axios from "axios";

import { Container, Icon } from "semantic-ui-react";
import { Patient, Entry} from "../types";
import { apiBaseUrl } from "../constants";
import { useStateValue } from "../state";
import { useParams } from "react-router-dom";
import { setPatientInfo } from "../state/reducer";
import { EntryInfo } from './EntryInfo';


const PatientInfoPage = () => {
  const [{ patient }, dispatch] = useStateValue();

  const { id } = useParams<{ id: string }>();

  React.useEffect(() => {
    void axios.get<void>(`${apiBaseUrl}/ping`);

    const fetchPatientInfo = async () => {
      try {
        const { data: patientInfoFromApi } = await axios.get<Patient>(
          `${apiBaseUrl}/patients/${id}`
        );
        dispatch(setPatientInfo(patientInfoFromApi));
      } catch (e) {
        console.error(e);
      }
    };
    void fetchPatientInfo();
  }, [dispatch]);

  const genderIcon = () => {
    if (patient && patient.gender === "male") {
      return <Icon name="mars" size="big" />;
    } else if (patient && patient.gender === "female") {
      return <Icon name="venus" size="big" />;
    } else if (patient && patient.gender === "other") {
      return <Icon name="genderless" size="big" />;
    } else {
      return null;
    }
  };

  const patientEntries = () => {
    if (patient && patient.entries) {
      return (<div>
        {patient.entries.map((entry: Entry) => (
          <EntryInfo key={entry.id} entry={entry}/>
        ))}
      </div>);
    } else {
      return null;
    }
  };

  if (patient) {
    return (
      <div>
        <Container textAlign="left">
          <h2>{patient.name} {genderIcon()}</h2>
          <p>ssn: {patient.ssn}</p>
          <p>occupation: {patient.occupation}</p>
          <h3>Entries:</h3>
          {patientEntries()}
        </Container>
      </div>
    );
  } else {
    return (<div>No patient data found with the ID :(</div>);
  }
};

export default PatientInfoPage;