import React, { useState, useEffect } from "react";
import { Container, Header, Icon } from "semantic-ui-react";
import axios from "axios";
import EntryDetail from "./Entry";
import { useParams } from "react-router-dom";
import { Patient, Gender} from "../types";
import { apiBaseUrl } from "../constants";
import { useStateValue, updatePatient } from "../state";

const PatientDetailPage = () => {
	const [{ patients }, dispatch] = useStateValue();
	const { id } = useParams<{ id: string }>();
	const [patient, setPatient] = useState<Patient>(patients[id]);

	if (!patient) window.location.href = "http://localhost:3000/";

	useEffect(() => {
		const fetchUserById = async (id: string) => {
			try {
				const { data: patientFromApi } = await axios.get<Patient>(
					`${apiBaseUrl}/patients/${id}`
				);
				setPatient(patientFromApi);
				dispatch(updatePatient(patientFromApi));
			} catch (e) {
				console.error(e);
			}
		};
		if (!isFullPatient(patient)) {
			void fetchUserById(id);
		}
	}, []);

	const isFullPatient = (param: Patient): boolean => {
		return (
			typeof param.ssn === "string" && typeof param.dateOfBirth === "string"
		);
	};

	return (
		<Container>
			{patient && isFullPatient(patient) ? (
				<>
					<Header as="h2">
						{patient.name}{" "}
						{patient.gender === Gender.Male ? (
							<Icon name="man" />
						) : patient.gender === Gender.Female ? (
							<Icon name="woman" />
						) : (
							<Icon name="genderless" />
						)}
					</Header>
					<p>
						ssn: {patient.ssn}
						<br />
						occupation: {patient.occupation}
					</p>
					<Header as="h3">entries</Header>

					{patient.entries && patient.entries.length > 0
						? patient.entries.map((entry) => (
								<EntryDetail key={entry.id} entry={entry} />
						))
						: "No Entries made"}
				</>
			) : (
				<div>loading....</div>
			)}
		</Container>
	);
};

export default PatientDetailPage;
