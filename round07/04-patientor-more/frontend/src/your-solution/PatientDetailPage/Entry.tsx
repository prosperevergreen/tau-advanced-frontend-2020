import React, { useEffect } from "react";
import { Icon, Card } from "semantic-ui-react";
import axios from "axios";
import { Entry, Diagnosis } from "../types";
import { apiBaseUrl } from "../constants";
import { addDiagnose, useStateValue } from "../state";

const EntryDetail = ({ entry }: { entry: Entry }) => {
	const [{ diagnosis }, dispatch] = useStateValue();
	// const [diagnose, setDiagnose] = useState<Diagnosis[]>([]);

	const fetchDiagnoseByCode = async (code: string) => {
		try {
			const { data: diagnoseFromApi } = await axios.get<Diagnosis>(
				`${apiBaseUrl}/diagnoses/${code}`
			);
			// setDiagnose(diagnose.concat(diagnoseFromApi));
			// dispatch(updatePatient(patientFromApi));
			return diagnoseFromApi;
		} catch (e) {
			// throw new Error(e);
			console.error(e);
		}
	};
	// console.log(diagnosis);

	useEffect(() => {
		if (entry.diagnosisCodes) {
			const notFound: string[] = [];
			entry.diagnosisCodes.map((code) => {
				// Check if diagnose is in app state
				const data = diagnosis[code];
				// Else add to fetch list
				if (!data) notFound.push(code);
			});
			if (notFound.length === 0) return;
			// Fetch all not found data
			void Promise.all(notFound.map((code) => fetchDiagnoseByCode(code)))
				.then((result) => {
					const data = result as Diagnosis[];
					data.map((diagnose) => dispatch(addDiagnose(diagnose)));
				})
				.catch((err) => console.log(err));
		}
	}, []);

	/**
	 * Helper function for exhaustive type checking
	 */
	const assertNever = (value: never): never => {
		throw new Error(
			`Unhandled discriminated union member: ${JSON.stringify(value)}`
		);
	};

	const getInfo = (entry: Entry) => {
		switch (entry.type) {
			case "HealthCheck":
				return <Icon name="doctor"></Icon>;
			case "Hospital":
				return <Icon name="hospital"></Icon>;
			case "OccupationalHealthcare":
				return (
					<>
						<Icon name="stethoscope"></Icon> {entry.employerName}
					</>
				);
			default:
				return assertNever(entry);
		}
	};

	return (
		<Card fluid>
			<Card.Content>
				<Card.Header>
					{entry.date} {getInfo(entry)}
				</Card.Header>
				<Card.Description>{entry.description}</Card.Description>
				{entry.diagnosisCodes && (
					<Card.Description>
						<h3>Diagnose</h3>
						<ul>
							{entry.diagnosisCodes.map((code) => (
								<React.Fragment key={code}>
									{diagnosis[code] && (
										<li >
											{diagnosis[code].code} - {diagnosis[code].name}
										</li>
									)}
								</React.Fragment>
							))}
						</ul>
					</Card.Description>
				)}
				{entry.type === "HealthCheck" && (
					<Card.Meta>
						{" "}
						<Icon
							name="heart"
							style={{
								color:
									entry.healthCheckRating === 0
										? "green"
										: entry.healthCheckRating === 1
										? "orange"
										: "red",
							}}
						></Icon>{" "}
					</Card.Meta>
				)}
			</Card.Content>
		</Card>
	);
};

export default EntryDetail;
