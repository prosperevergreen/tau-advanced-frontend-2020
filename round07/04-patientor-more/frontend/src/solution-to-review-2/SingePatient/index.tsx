import React from "react";
import { useParams } from "react-router-dom";
import { useStateValue } from "../state";
import { Icon } from 'semantic-ui-react';


const SinglePatient = () => {
    const [{ patients }] = useStateValue();
    const { id } = useParams<{ id: string }>();
    const myPatient = patients[id];
    const genderIcon = 
        myPatient.gender === "male" ? "mars"
        : myPatient.gender === "female" ? "venus"
        : "neuter";

    return(
      <div>
        <h2>{myPatient.name} <Icon name={genderIcon} /></h2>
        <h3>SSN: {myPatient.ssn}</h3>
        <h3>Occupation: {myPatient.occupation}</h3>
      </div>
    );
  };

export default SinglePatient;
