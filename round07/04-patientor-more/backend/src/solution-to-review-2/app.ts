
export { default as diagnoseRouter } from './routes-diagnoses';
export { default as patientRouter } from './routes-patients';

// ** enter commit sha of your repository in here **
export const commitSHA = '3e3b5a7f4cb981dfc307b19b07c562482d8b0188';
