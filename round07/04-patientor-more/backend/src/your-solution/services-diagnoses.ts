import diagnoseData from "../../data/diagnoses.json";
import { DiagnoseEntry } from "./types";

const diagnoses: Array<DiagnoseEntry> = diagnoseData;

const getEntries = (): Array<DiagnoseEntry> => {
	return diagnoses;
};

const addEntry = () => {
	return null;
};

const getDiagnoseByCode = (code: string): DiagnoseEntry => {
	const diagnose = diagnoseData.find((entry) => entry.code === code);
	if (!diagnose)
		throw new Error("Diagnose code is incorrect");
	return diagnose;
};

export default {
	getEntries,
	addEntry,
	getDiagnoseByCode,
};
