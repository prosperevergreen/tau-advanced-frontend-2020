import express from 'express';

import diagnoseService from './services-diagnoses';

const router = express.Router();

router.get('/:code', (req, res) => {
  const code = req.params.code;
  try{
    const diagnose = diagnoseService.getDiagnoseByCode(code);
    res.json(diagnose);
  }catch (e) {
		res.status(400).send(e);
	}
});
router.get('/', (_req, res) => {
  res.json(diagnoseService.getEntries());
});

export default router;
