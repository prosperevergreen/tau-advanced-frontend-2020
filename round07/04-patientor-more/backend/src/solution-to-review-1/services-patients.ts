
import { v4 as uuidv4 } from 'uuid';
import patientData from '../../data/patients.json';
import allPatients from './data-patients';
import { PatientEntry, NonSensitivePatientEntry, NewPatientEntry, Patient } from './types';
import toNewPatientEntry from './utils';

const patients: Array<PatientEntry> = patientData.map(obj => {
  const object = toNewPatientEntry(obj) as PatientEntry
  object.id = obj.id
  return object
});

const getEntries = (): Array<PatientEntry> => {
  return patients;
};

/*const getEntriesForPatient = (patientId: string): Array<PatientEntry> => {
  const patientEntries = patients.filter(({ id }) => patientId === id );
  return patientEntries;
};*/

const getNonSensitiveEntries = (): NonSensitivePatientEntry[] => {
  return patients.map(({ id, name, dateOfBirth, gender, occupation }) => (
    { id, name, dateOfBirth, gender, occupation }
  ))
};

const addEntry = (entry: NewPatientEntry): PatientEntry => {
  const newPatientEntry = { id: uuidv4(), ...entry };
  patients.push(newPatientEntry);
  return newPatientEntry; 
};

const getPatient = (patientId: string): Patient | undefined => {
  return allPatients.find(({ id }) => patientId === id);
}

export default {
  getEntries,
  getNonSensitiveEntries,
  addEntry,
  getPatient
};
