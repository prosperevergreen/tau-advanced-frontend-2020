
import express from 'express';
import { calculateBmi } from './bmi-calculator';

const app = express();
app.use(express.json());

app.get('/ping', (_req, res) => 
{
  res.send('pong');
});

app.get('/about', (_req, res) => 
{
  res.send('This is my awsome Body Mass Index calculator!');
});

app.get('/bmi', (req, res) =>
{
    const h = req.query.height;
    const w = req.query.weight;
    const height = Number(h); 
    const weight = Number(w);
    let responseStr = "";

    if(!isNaN(height) && !isNaN(weight))
    {
        responseStr = calculateBmi(height, weight);
    }
    else
    {
        responseStr = `Could not convert [height: ${String(h)}] or [weight: ${String(w)}] to numbers!`;
    }
    
    res.send(responseStr);
});

const PORT = 3003;
app.listen(PORT, () => 
{
  console.log(`Server running on port ${PORT}`);
});


