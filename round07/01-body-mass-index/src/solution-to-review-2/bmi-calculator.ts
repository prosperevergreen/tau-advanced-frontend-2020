
enum BMIClassifications 
{
    NONE = "No BMI class specified",
    VSU = "Very severely underweight",
    SU = "Severely underweight",
    U = "Underweight",
    N = "Normal (healthy weight)",
    OV = "Overweight",
    OC1 = "Obese Class I (Moderately obese)",
    OC2 = "Obese Class II (Severely obese)",
    OC3 = "Obese Class III (Very severely obese)"
}

//type BMIClassification = BMIClassifications;

interface iBounds
{
    lower: number,
    upper: number,
    includes: (num: number) => boolean
}

interface iBMIClasses
{
    range: iBounds
    classification: BMIClassifications
}

class BMIRange implements iBounds
{
    lower: number;
    upper: number;

    constructor(lower: number, upper: number)
    {
        this.lower = lower;
        this.upper = upper;
    }

    includes(num: number): boolean
    {
        return num >= this.lower &&  num <= this.upper; 
    }
}

class BMIClass implements iBMIClasses
{
    range: iBounds;
    classification: BMIClassifications;

    constructor(classification: BMIClassifications, range: iBounds)
    {        
        this.classification = classification;
        this.range = range;
    }
}

const bmiTable: Array<iBMIClasses> = 
[
    new BMIClass(BMIClassifications.VSU,    new BMIRange(0, 15)),
    new BMIClass(BMIClassifications.SU,     new BMIRange(15, 16)),
    new BMIClass(BMIClassifications.U,      new BMIRange(16, 18.5)),
    new BMIClass(BMIClassifications.N,      new BMIRange(18.5, 25)),
    new BMIClass(BMIClassifications.OV,     new BMIRange(25, 30)),
    new BMIClass(BMIClassifications.OC1,    new BMIRange(30, 35)),
    new BMIClass(BMIClassifications.OC2,    new BMIRange(35, 40)),
    new BMIClass(BMIClassifications.OC3,    new BMIRange(40, Number.MAX_SAFE_INTEGER))
];

interface iCalculateBmiParameters
{
    height: number,
    mass: number
}

// Would make sense to change calculateBmi to accept iCalculateBmiParameters
// but then 9.1 would not work exactly as asked :(

const calculateBmi = (height: number, mass: number): string => 
{
  const bmiValue: number = mass / ((height / 100) ** 2);
  let ret: BMIClassifications = BMIClassifications.NONE;
  
  for(let i = 0; i < bmiTable.length; ++i)
  {
    const cl: BMIClass = bmiTable[i];
    if(cl.range.includes(bmiValue))
    {
        ret = cl.classification;
        break;
    }
  }
  
  return ret;
};


export { calculateBmi, iCalculateBmiParameters };
