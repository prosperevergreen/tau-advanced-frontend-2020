
import { calculateBmi, iCalculateBmiParameters } from './bmi-calculator';


const parseArguments = (args: Array<string>): iCalculateBmiParameters => 
{
    if(args.length < 4) throw new Error('Not enough arguments');
    if(args.length > 4) throw new Error('Too many arguments');
  
    if(!isNaN(Number(args[2])) && !isNaN(Number(args[3])))
    {
      return {
        height: Number(args[2]),
        mass:   Number(args[3])
      };
    } 
    else 
    {
      throw new Error('Provided values were not numbers!');
    }
};

try
{
    const { height, mass } = parseArguments(process.argv);
    console.log(calculateBmi(height, mass));
}
catch(e) 
{
    // Great start for Typescript! (sarcasm)
    // Why cant we catch errors by type or just any in general (like in C++ for example?)
    console.log('Error, something bad happened, message: ', e instanceof Error ? e.message : "");
}
