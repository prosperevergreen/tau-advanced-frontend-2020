import express from "express";
import { calculateBmi } from "./bmi-calculator";

const app = express();
app.use(express.json());

interface BMIResponse {
	height: number;
	weight: number;
	bmi: string;
}

app.get("/bmi", (req, res) => {
	const height = Number(req.query.height);
	const weight = Number(req.query.weight);

	if (isNaN(height) || isNaN(weight))
		return res.status(400).json({
			error: "malformatted parameters",
		});

	const data: BMIResponse = {
		height,
		weight,
		bmi: calculateBmi(height, weight),
	};
	return res.json(data);
});

const PORT = 3002;

app.listen(PORT, () => {
	console.log(`Server running on port ${PORT}`);
});
