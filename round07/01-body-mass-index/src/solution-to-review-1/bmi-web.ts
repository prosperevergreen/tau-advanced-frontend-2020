import express from 'express';
import { calculateBmi } from './bmi-calculator';

interface bmiValues {
  height: number;
  weight: number;
}

const parseArguments = (height: number, weight: number): bmiValues => {
  return {
    height: Number(height),
    weight: Number(weight)
  };
};

const app = express();

app.get('/bmi', (req, res) => {
  const { height, weight } = parseArguments(Number(req.query.height), Number(req.query.weight));

  if (!isNaN(height) && !isNaN(weight)) {
    res.json({ weight: weight, height: height, bmi: calculateBmi(height, weight) });
  } else {
    res.json({error: "malformatted parameters"});
  }
});

const PORT = 3003;

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});