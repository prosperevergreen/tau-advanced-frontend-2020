
import { useState } from 'react';

// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE                        //
// ------------------------------------------------------------ //
export const commitSHA = '3c60f8d';
// ------------------------------------------------------------ //


export const App = () => {

  const [good, setGood] = useState(0)
  const [neutral, setNeutral] = useState(0)
  const [bad, setBad] = useState(0)
  const [all, setAll] = useState(0)
  const [sum, setSum] = useState(0)

  const handleGoodClick = () => {
    setGood(good +1)
    setAll(all +1)
    setSum(sum +1)
  }
  const handleNeutralClick = () => {
    setNeutral(neutral +1)
    setAll(all +1)
  }
  const handleBadClick = () => {
    setBad(bad +1)
    setAll(all +1)
    setSum(sum -1)
  }
  const average = () => {
    if (all === 0) return 0
    return sum / all
  }
  const positives = () => {
    if (all === 0) return 0
    return good / all * 100
  }

  return (
    <div>
      <h1>Give feedback</h1>
      <Button onClick={handleGoodClick} text="Good" />
      <Button onClick={handleNeutralClick} text="Neutral" />
      <Button onClick={handleBadClick} text="Bad" />
      <p><strong>Statistics</strong></p>
      <Statistics all={all} good={good} neutral={neutral} bad={bad} average={average()} positives={positives()}/>
    </div>
  )
}

const Button = ({onClick, text}) => {
  return (
  <button onClick={onClick}>
    {text}
  </button>
  )
}
const Statistics = (props) => {
  if (props.all === 0) return <p>No feedback given</p>
  return (
      <table>
        <tbody>
          <tr><td>Good</td><td>{props.good}</td></tr>
          <tr><td>Neutral</td><td>{props.neutral}</td></tr>
          <tr><td>Bad</td><td>{props.bad}</td></tr>
          <tr><td>All</td><td>{props.all}</td></tr>
          <tr><td>Average</td><td>{props.average}</td></tr>
          <tr><td>Positives</td><td>{props.positives}%</td></tr>
        </tbody>
      </table>
    )  
}  


