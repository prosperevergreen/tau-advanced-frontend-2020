import { useState } from "react";

// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE                        //
// ------------------------------------------------------------ //
export const commitSHA = "bdd99ac";
// ------------------------------------------------------------ //

const Statistic = ({ text, value }) => (
	<tr>
		<td>{text}</td>
		<td>{value}</td>
	</tr>
);

const Statistics = ({ good, neutral, bad }) => {
	const sumFeedback = good + neutral + bad;
	return (
		<div>
			<h1>statictics</h1>
			{sumFeedback === 0 ? (
				<div>No feedback given</div>
			) : (
				<table>
					<tbody>
					<Statistic text="good" value={good} />
					<Statistic text="neutral" value={neutral} />
					<Statistic text="bad" value={bad} />
					<Statistic text="all" value={sumFeedback} />
					<Statistic text="average" value={(good - bad) / sumFeedback} />
					<Statistic
						text="positive"
						value={(good * 100) / sumFeedback + " %"}
					/>
					</tbody>
				</table>
			)}
		</div>
	);
};

const Button = ({ text, incStateHandler }) => (
	<button onClick={incStateHandler}>{text}</button>
);

export const App = () => {
	// save clicks of each button to its own state
	const [good, setGood] = useState(0);
	const [neutral, setNeutral] = useState(0);
	const [bad, setBad] = useState(0);

	const incGoodState = () => setGood(good + 1);
	const incNeutralState = () => setNeutral(neutral + 1);
	const incBadState = () => setBad(bad + 1);

	return (
		<div>
			<h1>give feedback</h1>
			<div>
				<Button incStateHandler={incGoodState} text="good" />
				<Button incStateHandler={incNeutralState} text="neutral" />
				<Button incStateHandler={incBadState} text="bad" />
			</div>
			<Statistics good={good} neutral={neutral} bad={bad} />
		</div>
	);
};
