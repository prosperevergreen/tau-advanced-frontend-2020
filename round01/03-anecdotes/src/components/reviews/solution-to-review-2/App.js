
import { useState } from 'react'

// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE                        //
// ------------------------------------------------------------ //
export const commitSHA = 'f7d2ae9';
// ------------------------------------------------------------ //

//Note: I'd rather do it with classes and abstractions, but the exercise
//strongly suggests using separate arrays "connected" via indexes,
//which in my experience is a recepie for disaster...
//But anyway, lets stick to what is asked :(

const Anecdote = (props) =>
{
  const {text, votes} = props;
  return(
    <div>
      <div>
      {text}
      </div>
      Has {votes} votes
    </div>);
}

const Button = (props) =>
{
  const {label, onclickCb} = props;
  return (<button onClick={onclickCb}>{label}</button>);
}

export const App = ({anecdotes}) =>
{
  function constructDefaultPoints()
  {
    let ret = [];
    for( let i = 0; i < anecdotes.length; ++i)
    {
      ret[i] = 0;
    }
    return ret;
  }

  const [selected, setSelected] = useState(0);
  const [previous, setPrevious] = useState(-1);
  const [points, setPoints] = useState(constructDefaultPoints());

  //I prefer small(-ish) separate named functions instead of inlined lambdas (which JS is made for)
  function random(rStart, rEnd)
  {
    return Math.floor((Math.random() * rEnd) + rStart);
  }

  //Tecnically, "random" is requested, however true random can select
  //same index multiple times in a row... lets avoid this!
  function selectRandomElement(arr, previous = -1)
  {
    let l = arr.length;
    let nextIdx = 0;
    if(!l)
    {
      throw Error("Array of anecdotes is empty :(");
    }

    if(l > 1)
    {
      let max = arr.length - 1;
      for(nextIdx = random(0, max); nextIdx === previous; nextIdx = random(0, max))
      { }
      
    }
    setSelected(nextIdx);
    setPrevious(nextIdx);
    //console.log(nextIdx);
    return arr[nextIdx];
  }

  function selectNext()
  {
    selectRandomElement(anecdotes, previous);
  }

  function vote()
  {
    let copy = [...points];
    copy[selected] ? copy[selected] += 1 : copy[selected] = 1;
    setPoints(copy);
  }

  function getMostVotedIdx()
  {
    let mostVotedIdx = -1;
    let mostPoints = 0;
    for(let i = 0; i < points.length; ++i)
    {
      let currentPoints = points[i];
      if(currentPoints > mostPoints)
      {
        mostPoints = currentPoints;
        mostVotedIdx = i;
      }
    }
    return mostVotedIdx >= 0 ? mostVotedIdx : 0;
  }
  function getAnecdoteByIdx(idx)
  {
    let ret;
    
    if(idx >= 0 && idx < anecdotes.length - 1)
    {
      ret = anecdotes[idx];
    }
    else
    {
      throw Error(`Anecdote index ${idx} out of range, max is ${anecdotes.length ? anecdotes.length - 1 : 0}`)
    }
    return ret;
  }
  function getPointsByIdx(idx)
  {
    let ret;
    
    if(idx >= 0 && idx < points.length - 1)
    {
      ret = points[idx];
    }
    else
    {
      throw Error(`Points index ${idx} out of range, max is ${points.length ? points.length - 1 : 0}`)
    }
    return ret;
  }

  const mostWantedIdx = getMostVotedIdx();

  return (
    <div>
      <div>
        <h1> Anecdote of the day </h1>
        <Anecdote text = {getAnecdoteByIdx(selected)} votes = {getPointsByIdx(selected)}/>
        <div>
          <Button label = "vote"          onclickCb = {vote}  />
          <Button label = "next anecdote" onclickCb = {selectNext}  />          
        </div>
      </div>
      <div>
      <h1> Anecdote with most votes </h1>
        <Anecdote text = {getAnecdoteByIdx(mostWantedIdx)} votes = {getPointsByIdx(mostWantedIdx)}/>
      </div>
    </div>
  );
}

