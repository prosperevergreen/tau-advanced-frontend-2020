import React, { useState } from 'react'
// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE                        //
// ------------------------------------------------------------ //
export const commitSHA = '99d4285';
let points = [0, 0, 0, 0, 0, 0];
// ------------------------------------------------------------ //
const Button = (props) => {
  return (
    <button onClick={props.onClick}>{props.text}</button>
  )
}

const MostVotes = (props) => {
  return (<div>
    <p>
      {props.mostVotes}
    </p>
    <p>Has {props.number} votes</p>
  </div>
  )
}

export const App = ({ anecdotes }) => {

  const [selected, setSelected] = useState(0)

  //Check that no number is generated twice
  function generateRandom(min, max) {
    var num = Math.floor(Math.random() * (max - min + 1)) + min;
    return (num === selected) ? generateRandom(min, max) : num;
  }

  function voteSelected(selected) {
    const copy = [...points]
    // increment the value in position 2 by one
    copy[selected] += 1
    points = copy;

  }


  return (
    <div>

      <p>{anecdotes[selected]}</p>
      <Button text={'Next anecdote'} onClick={() => setSelected(generateRandom(0, 5))} />
      <Button text={'Vote'} onClick={() => voteSelected(selected)} />
      {points.some(num => num > 0) ? <MostVotes mostVotes={anecdotes[points.indexOf(Math.max.apply(0, points))]} number={(Math.max.apply(null, points))} /> : <div />
      }
    </div>

  )}

