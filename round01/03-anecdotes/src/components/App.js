import { useState } from "react";

// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE                        //
// ------------------------------------------------------------ //
export const commitSHA = "4dc0fa0";
// ------------------------------------------------------------ //

const Anacdote = ({ heading, text, points }) => (
	<div>
		<h1>{heading}</h1>
		<div>
			{text}
			<br />
			has {points} votes
		</div>
	</div>
);

const Button = ({ text, clickHandler }) => (
	<button onClick={clickHandler}>{text}</button>
);

export const App = ({ anecdotes }) => {
	const [selected, setSelected] = useState(0);
	const [maxPointIndex, setMaxPointIndex] = useState(0);
	const [points, setPoints] = useState(Array(anecdotes.length).fill(0));

	const randAnecdote = () =>
		setSelected(Math.floor(Math.random() * anecdotes.length));

	const incPoints = () => {
		const newPoints = [...points];
		newPoints[selected] += 1;
		if (newPoints[selected] > newPoints[maxPointIndex]) {
			setMaxPointIndex(selected);
		}
		setPoints(newPoints);
	};

	return (
		<div>
			<Anacdote
				heading="Anecdote of the day"
				text={anecdotes[selected]}
				points={points[selected]}
			/>
			<Button text="vote" clickHandler={incPoints} />
			<Button text="next anecdote" clickHandler={randAnecdote} />
			<Anacdote
				heading="Anecdote with most votes"
				text={anecdotes[maxPointIndex]}
				points={points[maxPointIndex]}
			/>
		</div>
	);
};
