const Myact = {
	createElement: function (tag, props, ...childNodes) {
		let element = null;
		// Check if tag is a HTML element (String) or a function (Object)
		if (typeof tag === "string") {
			// Create tag element string
			// For each of the childnode append the elements to the tag element
			element = `<${tag}>
      ${childNodes
				.map((element) => {
					if (typeof element === "function") {
						return element();
					} else {
						return element;
					}
				})
				.join("")}
      </${tag}>`;
		} else {
			// Get element from component
			element = tag(props);
		}

		return element;
	},
};

const MyactDOM = {
	render: function (element, parent) {
		parent.innerHTML = element;
	},
};
