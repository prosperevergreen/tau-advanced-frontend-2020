"use strict";

// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE                        //
// ------------------------------------------------------------ //
const commitSHA = "133b535"; //
// ------------------------------------------------------------ //

const Header = (props) => {
	return createElement("h1", null, props.course);
};

const Part = (props) => {
	return createElement("p", null, `${props.part} ${props.exercises}`);
};

const Content = (props) => {
	return createElement(
		"div",
		null,
		createElement(Part, {
			part: props.parts[0].name,
			exercises: props.parts[0].exercises,
		}, null),
		createElement(Part, {
			part: props.parts[1].name,
			exercises: props.parts[1].exercises,
		}, null),
		createElement(Part, {
			part: props.parts[2].name,
			exercises: props.parts[2].exercises,
		}, null)
	);
};

const Total = (props) =>
	createElement(
		"p",
		null,
		`Number of exercises 
		${
			props.parts[0].exercises +
			props.parts[1].exercises +
			props.parts[2].exercises
		}`
	);

const App = ({ course }) => {
	return createElement(
		"div",
		null,
		createElement(Header, {
			course: course.name,
		}, null),
		createElement(Content, {
			parts: course.parts,
		}, null),
		createElement(Total, {
			parts: course.parts,
		}, null)
	);
};
