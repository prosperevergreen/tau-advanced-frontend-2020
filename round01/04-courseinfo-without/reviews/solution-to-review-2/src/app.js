
'use strict';

// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE                        //
// ------------------------------------------------------------ //
const commitSHA = '628a91e';   //
// ------------------------------------------------------------ //

const Header = props => React.createElement(
    'h1', null, props
);
const Header2 = () => React.createElement(
    'h2', null, 'Hello world, it is 2'
);

const Part = props => React.createElement(
    'p', null, props.name, ' ', props.exercises
);

const Content = props => React.createElement(
    'div', null, Part(props[0]), Part(props[1]), Part(props[2])
);

const Total = props => React.createElement(
    'p', null, 'Number of exercises ', (props[0].exercises + props[1].exercises + props[2].exercises)
);

const App = ({course}) => {
    return React.createElement(
        'div',
        null,
        Header(course.name),
        Content(course.parts),
        Total(course.parts)
      )
}
