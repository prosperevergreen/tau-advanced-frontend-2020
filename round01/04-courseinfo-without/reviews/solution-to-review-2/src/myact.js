
//Its first parameter (tag) is either a string (html element) or a function ("component").

// - If the tag is a string, createElement creates the corresponding html element 
//   using the document object's appropriate method
//   --> var btn = document.createElement("BUTTON");
// - If the tag is a function, createElement calls this function, giving it the "props" as the parameter.

//When an element is created in either of the above ways, createElement appends  
//the "childNodes" as children of the element, and finally returns the element it created.
 
const Myact = {

  createElement: function (tag, props, ...childNodes) {
    console.log('tag type',typeof tag);
    console.log('tag',tag);
    console.log('props',props);
    console.log('children', ...childNodes);

    if (typeof tag === 'function') {
      var ts = tag.toString().split(',');     
      var ct = ts[0].split('\'')[1];
      var el = document.createElement(ct);

      ts.shift();
      ts.shift();
      ts.forEach(il => {
        var i0 = il.indexOf('(');
        
        if (il.substring(0,i0).trim() === 'Header') {
          var el0 = document.createElement("h1");
          el0.innerHTML = props.course.name; 
          el.appendChild(el0);
        }
        else if (il.substring(0,i0).trim() === 'Content') {
          var el0 = document.createElement("div");
          var el01 = document.createElement("p");
          var el02 = document.createElement("p");
          var el03 = document.createElement("p");
          el01.innerHTML = props.course.parts[0].name + " " + props.course.parts[0].exercises; 
          el02.innerHTML = props.course.parts[1].name + " " + props.course.parts[1].exercises; 
          el03.innerHTML = props.course.parts[2].name + " " + props.course.parts[2].exercises; 
          el.appendChild(el01);
          el.appendChild(el02);
          el.appendChild(el03);
        }
        else if (il.substring(0,i0).trim() === 'Total') {
          var el0 = document.createElement("p");
          el0.innerHTML = 'Number of exercises ' + (props.course.parts[0].exercises + props.course.parts[1].exercises + props.course.parts[2].exercises); 
          el.appendChild(el0);
        }
      });
    }
    
    document.body.appendChild(el);
    return el;
  }
  
}

const MyactDOM = {

  render: function (element, parent) {
    console.log('render element: ', element);
    console.log('render parent: ', parent);
  }

}
