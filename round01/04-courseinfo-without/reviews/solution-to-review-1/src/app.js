
'use strict';

// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE                        //
// ------------------------------------------------------------ //
const commitSHA = 'dee50e9';   //
// ------------------------------------------------------------ //

// create e for createElement to make code easier to read
//const e = react.createElement;
const e = createElement;

// header
const Header = props => {
    return  e('h1',null,`${props.header}`)
  };

// single part
const Part = props => {
    return e('p', null, `${props.parts} ${props.exercises}`)
    };

// content is separeted to theree parts
const Content = props => {
    return e('div', null, 
    e(Part, {parts: props.parts[0].name, exercises: props.parts[0].exercises}),
    e(Part, {parts: props.parts[1].name, exercises: props.parts[1].exercises}),
    e(Part, {parts: props.parts[2].name, exercises: props.parts[2].exercises})
    )
};

// calculate total
const Total = props => {
    const total = props.parts[0].exercises + props.parts[1].exercises+ props.parts[2].exercises;
    return e('p', null, `Total ${total}`)
};

// main application
const App = ({ course }) => {
    const header = course.name;
    const parts = course.parts;
    return  e ('div', null, 
    e( Header, { header }),
    e( Content, { parts }),
    e( Total, { parts})
    )

};
