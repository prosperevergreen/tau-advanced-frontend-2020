/** @format */

// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE                        //
// ------------------------------------------------------------ //
// ------------------------------------------------------------ //

import React from 'react';
export const commitSHA = '37fc028';

const Header = ({ course }) => {
	return <h1>{course}</h1>;
};

const Part = ({ name, exercises }) => {
	return (
		<p>
			{name} {exercises}
		</p>
	);
};

const Content = ({ parts }) => {
	// Object destructuring:
	return (
		<>
			{parts.map((part, index) => {
				const { name, exercises } = part;
				return <Part key={index} name={name} exercises={exercises} />;
			})}
		</>
	);
};

const Total = ({ parts }) => {
	let sum = 0;
	parts.forEach(({ exercises }) => {
		sum += exercises;
	});

	return <p>Number of exercises {sum}</p>;
};

export const App = () => {
	const course = {
		name: 'Half Stack application development',
		parts: [
			{
				name: 'Fundamentals of React',
				exercises: 10,
			},
			{
				name: 'Using props to pass data',
				exercises: 7,
			},
			{
				name: 'State of a component',
				exercises: 14,
			},
		],
	};

	return (
		<div>
			<Header course={course.name} />
			<Content parts={course.parts} />
			<Total parts={course.parts} />
		</div>
	);
};
