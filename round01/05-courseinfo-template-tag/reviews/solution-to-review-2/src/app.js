
// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE                        //
// ------------------------------------------------------------ //
const commitSHA = 'e13f7fb';   //
// ------------------------------------------------------------ //

const App = ({ data, template }) => {
  const totalExercises = data.parts
    .map(part => part.exercises)
    .reduce((a, current) => {
      return a + current
    })
  const clone = template.content.cloneNode(true)

  const headerFragment = clone.querySelector('.header')
  const contentFragments = clone.querySelectorAll('.content p')
  const totalFragment = clone.querySelector('.total')

  headerFragment.textContent = data.name

  data.parts.forEach((part, i) => {
    contentFragments[i].textContent = part.name
  })

  totalFragment.textContent = `Number of exercises ${totalExercises}`

  return clone
};
