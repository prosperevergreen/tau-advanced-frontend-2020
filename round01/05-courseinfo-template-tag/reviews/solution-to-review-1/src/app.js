// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE                        //
// ------------------------------------------------------------ //
const commitSHA = "b607498"; //
// ------------------------------------------------------------ //

const App = ({ data, template }) => {
  var templateInstance = document.importNode(template.content, true);
  var templateInstanceHeader = templateInstance.querySelector("h1"),
    templateInstanceContent = templateInstance.querySelectorAll(".content p"),
    templateInstanceTotal = templateInstance.querySelector(".total");

  templateInstanceHeader.textContent = data.name;
  templateInstanceContent.forEach(
    (element, index) =>
      (element.textContent = `${course.parts[index].name} ${course.parts[index].exercises}`)
  );
  templateInstanceTotal.textContent = `Number of exercises: ${data.parts.reduce(
    (acc, curr) => acc + curr.exercises,
    0
  )}`;

  return templateInstance;
};