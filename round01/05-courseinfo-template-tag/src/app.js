// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE                        //
// ------------------------------------------------------------ //
const commitSHA = "6fa7811"; //
// ------------------------------------------------------------ //

const setHeader = (elem, text) => {
  // Attach header to DOM
	elem.innerText = text;
};

const setPart = (elem, props) => {
	// Attach content part to DOM
	elem.innerText = `${props.text} ${props.value}`;
};

const setContent = ({ content, total }, parts) => {
	// Create the exercise section
	parts.map((part, i) => {
		setPart(content[i], { text: part.name, value: part.exercises });
	});

	// Get the exercise total
	const totalEx = parts.reduce((sum, part) => sum + part.exercises, 0);
	// Create total section
	setPart(total, { text: "Number of exercises", value: totalEx });
};

const App = ({ data, template }) => {
  // Clone template
	const clone = template.content.cloneNode(true);

  // Get reference to various sections
	const header = clone.querySelector(".header");
	const content = clone.querySelectorAll(".content p");
	const total = clone.querySelector(".total");

  // Set header
	setHeader(header, data.name);

  // Set content
  setContent({content, total}, data.parts)

	return clone;
};
