
import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom"

import { Blog, Notification, Togglable, BlogForm, Users, User } from './components'
import { setNotification } from './reducer-notification'
import { initializeBlogs, createBlog, updateBlog, removeBlogpost } from './reducer-blog'
import { initializeUsers, } from "./reducer-user"
import { setLoggedUser, initSignIn, logoutUser } from './reducer-login'

export { default as store } from './store.js'

// *** ENTER COMMIT SHA OF YOUR REPO IN HERE ***
export const commitSHA = 'bb280fd5e2f62f918ff7df2427462366b67b5c1c';


// [x] stage01: (7.09) "notifications use redux"
// [x] stage02: (7.10) "blogs initialized into redux store"
// [x] stage03: (7.10) "blog create with async action"
// [x] stage04: (7.11) "blog likes with async action"
// [x] stage05: (7.11) "blog delete with async action"
// [x] stage06: (7.12) "signed in user in redux store"
// [x] stage07: (7.13) "users view"
// [x] stage08: (7.14) "single user view"
// [] stage09: (7.14) "links from users view to single user view"
// [] stage10: (7.15) "single blog view"
// [] stage11: (7.15) "links from blogs and single user view to single blog view"
// [x] stage12: (7.16) "navigation bar"
// [] stage13: (7.17) "comments in single blog view"
// [] stage14: (7.18) "form for creating comments"


export const App = () => {

  const blogFormRef = React.createRef()

  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')

  const dispatch = useDispatch()
  const blogs = useSelector(state => state.blogs)
  const loginUser = useSelector(state => state.loginUser)


  useEffect(() => {
    dispatch(initializeUsers())
    dispatch(initializeBlogs())
    dispatch(initSignIn())
  }, [dispatch])

  /**
   * handleLogin
   * @param {*} event 
   */
  const handleLogin = async (event) => {
    event.preventDefault()
    try {
      const user = { username, password }
      dispatch(setLoggedUser(user))
      setUsername('')
      setPassword('')
    } catch (exception) {
      setUsername('')
      setPassword('')
    }
  }

  const handleLogout = () => {
    dispatch(logoutUser())
  }

  const addBlog = ({ newTitle, newAuthor, newUrl }) => {
    blogFormRef.current.toggleVisibility()
    try {
      dispatch(createBlog({ title: newTitle, author: newAuthor, url: newUrl, likes: 0 }))
      dispatch(setNotification({ text: `blog "${newTitle}" added` }))
    } catch (error) {
      console.log('error', error)
      dispatch(setNotification({ text: error.response.data.error, error: true }))
    }
  }


  const updateBlogLike = (blog) => {
    console.log(blog)
    try {
      dispatch(updateBlog(blog))
    } catch (error) {
      console.log('error: ', error)
      dispatch(setNotification({ text: error.response.data.error, error: true }))
    }
  }

  const removeBlog = (id) => {
    const blogToDelete = blogs.find(blog => blog.id === id)
    if (!window.confirm(`Remove blog "${blogToDelete.title}" by ${blogToDelete.author}`)) {
      return
    }
    try {
      dispatch(removeBlogpost(blogToDelete))
      dispatch(setNotification({ text: `blog "${blogToDelete.title}" deleted` }))
    } catch (error) {
      dispatch(setNotification({ text: error.response.data.error, error: true }))
    }
  }


  /*
   * Login form
   */

  if (loginUser === null) {
    return (
      <div>
        <h2>log in to application</h2>

        <Notification setMessage={setNotification} />

        <form onSubmit={handleLogin}>
          <div>username &nbsp;
          <input
              type="text"
              value={username}
              name="Username"
              onChange={({ target }) => setUsername(target.value)}
            />
          </div>
          <div>password &nbsp;
          <input
              type="password"
              value={password}
              name="Password"
              onChange={({ target }) => setPassword(target.value)}
            />
          </div>
          <div>
            <button type="submit">login</button>
          </div>
        </form>

      </div>
    )
  }

  /*
   * Bloglist
   */

  const blogsToShow = blogs.sort((a, b) => (b.likes - a.likes))

  return (
    <div>
      <h2>Blogs App</h2>
      {/* router stuff */}

      <Notification setMessage={setNotification} />

      <Router>
        <div style={{ backgroundColor: '#92a8d1', fontWeight: 'bold', fontSize: '1.5em', display: "inline-block"}}>
          <Link to='/' style={{color: 'red'}}>Blogs </Link> 
          <Link to='/users' style={{color: 'red'}}>Users</Link>
        </div>
        <p>
          {loginUser.name} logged in &nbsp;
      <button type="button" onClick={handleLogout}>Logout</button>
        </p>

        <Switch>
          <Route path='/users/:id'><User/></Route>

          <Route path='/users' component={Users} />
          <Route path='/'>
            <Togglable buttonLabel='new blog' ref={blogFormRef}>
              <BlogForm addBlog={addBlog} />
            </Togglable>
            <br />
            {blogsToShow.map(blog =>
              <Blog
                key={blog.id}
                blog={blog}
                updateBlog={updateBlogLike}
                removeBlog={removeBlog}
                user={loginUser}
              />
            )}
          </Route>
        </Switch>
      </Router>

    </div>
  )
}

