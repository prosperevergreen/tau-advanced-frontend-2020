import { userService } from './services'

export const setUser = (user) => {
  return {
    type: 'SET_USER',
    data: user
  }
}

export const initializeUsers = () => {
  return async dispatch => {
    const users = await userService.getAll()
    dispatch({
      type: 'INIT_USERS',
      data:  users 
    })
  }
}

const reducer = (state = null, action) => {
  switch (action.type) {
    case 'SET_USER': 
      return action.data
    case 'INIT_USERS':
      return action.data
    default:
      return state
  }
}

export default reducer

