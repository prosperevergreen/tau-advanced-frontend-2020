import { blogService } from './services'

export const initializeBlogs = () => {
  return async dispatch => {
    const blogs = await blogService.getAll()
    dispatch({
      type: 'INIT_BLOGS',
      data:  blogs 
    })
  }
}

export const createBlog = (data) => {
  return async dispatch => {
    const blog = await blogService.create(data)
    dispatch({
      type: 'CREATE_BLOG',
      data: blog
    })
  }
}

export const updateBlog = (data) => {
  return async dispatch => {
    const blog = await blogService.update(data.id, data)
    dispatch({
      type: 'UPDATE_BLOG',
      data: blog
    })
  }
}

export const removeBlogpost = (data) => {
  return async dispatch => {
     await blogService.remove(data.id)
    dispatch({
      type: 'REMOVE_BLOG',
      data: data.id
    })
  }
}

const reducer = (state = [], action) => {

  switch (action.type) {
    case 'INIT_BLOGS':
      return action.data
    case 'CREATE_BLOG':
      return [...state, action.data]
    case 'UPDATE_BLOG':
      return [...state]
    case 'REMOVE_BLOG':
      const newState = state.filter(blog => blog.id !== action.data)
      return newState
    default:
      return state
  }

}

export default reducer