import { blogService, loginService } from './services'

import { setNotification } from './reducer-notification'

export const logoutUser = () => {
  window.localStorage.removeItem('loggedBlogAppUser')
  return {
    type: 'LOG_OUT',
    data: null
  }
}

export const initSignIn = () => {
  const loggedUserJSON = window.localStorage.getItem('loggedBlogAppUser')
  if (loggedUserJSON) {
    const user = JSON.parse(loggedUserJSON)
    blogService.setToken(user.token)
    return {
      type: 'INIT_SIGN',
      data: user
    }
  }
  return {
    type: 'INIT_SIGN',
    data: null
  }
}


export const setLoggedUser = (user) => {
  return async dispatch => {
    try {
      const loggedUser = await loginService.login(user)
      blogService.setToken(loggedUser.token)
      window.localStorage.setItem('loggedBlogAppUser', JSON.stringify(loggedUser))
      dispatch(setNotification({ text: `${loggedUser.name} logged in` }))
      dispatch({
        type: 'SET_LOG_USER',
        data: loggedUser
      })
    } catch (error) {
      dispatch(setNotification({ error: true, text: 'wrong username or password' }))
    }
  }

}

const reducer = (state = null, action) => {

  switch (action.type) {
    case 'SET_LOG_USER':
      return action.data
    case 'LOG_OUT':
      return action.data
    case 'INIT_SIGN':
      return action.data
    default:
      return state
  }

}

export default reducer
