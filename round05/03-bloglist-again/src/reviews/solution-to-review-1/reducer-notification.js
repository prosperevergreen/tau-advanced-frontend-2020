

export const setNotification = (data) => {
  return {
    type: 'SET_NOTIFICATION',
    data
  }
}


const reducer = (state = null, action) => {

  switch (action.type) {
    case 'SET_NOTIFICATION':
      return action.data

    default:
      return state
  }

}

export default reducer

