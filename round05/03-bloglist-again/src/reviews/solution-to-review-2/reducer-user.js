/** @format */

// CONSTANTS
const GET_USERS = 'GET_USERS';
const REMOVE_USERS = 'REMOVE_USER';

// ACTION CREATORS
export const getUsers = (blogs) => async (dispatch) => {
	const users = {};
	blogs.forEach((blog) => {
		const title = blog.title;
		const name = blog.user.name;
		const id = blog.user.id;
		const blogId = blog.id;
		if (!users[id]) {
			users[id] = { blogs: [{ title, blogId }], name };
		} else users[id].blogs.push({ title, blogId });
	});
	dispatch({
		type: GET_USERS,
		payload: users,
	});
};

export const removeUser = () => ({
	type: REMOVE_USERS,
});

// REDUCER
const reducer = (state = null, action) => {
	switch (action.type) {
		case GET_USERS:
			// Add them to redux
			return action.payload;
		case REMOVE_USERS:
			// Remove them from redux:
			return null;
		default:
			return state;
	}
};

export default reducer;
