/** @format */

// CONSTANTS
const NEW_NOTIFICATION = 'NEW_NOTIFICATION';
const REMOVE_NOTIFICATION = 'REMOVE_NOTIFICATION';

// ACTION CREATORS
export const newNotification = (notice) => {
	return {
		type: NEW_NOTIFICATION,
		payload: notice,
	};
};

export const removeNotification = () => {
	return {
		type: REMOVE_NOTIFICATION,
	};
};

// REDUCER
const reducer = (state = null, action) => {
	switch (action.type) {
		case NEW_NOTIFICATION:
			return action.payload;
		case REMOVE_NOTIFICATION:
			return null;
		default:
			return state;
	}
};

export default reducer;
