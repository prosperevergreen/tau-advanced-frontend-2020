/** @format */

import React, {
	forwardRef,
	useEffect,
	useImperativeHandle,
	useState,
} from 'react';

import PropTypes from 'prop-types';
import { removeNotification } from './reducer-notification';
import { useSelector, useDispatch } from 'react-redux';
import {
	addCommentToBlog,
	createBlog,
	likeBlog,
	removeBlog,
} from './reducer-blog';
import { login, logout } from './reducer-login';
import { Link, useHistory, useParams } from 'react-router-dom';

export const Notification = () => {
	const notice = useSelector((state) => state.notification);
	const dispatch = useDispatch();
	useEffect(() => {
		let timeOutId;
		if (notice) {
			timeOutId = setTimeout(() => {
				dispatch(removeNotification());
			}, 3000);
		}
		return () => {
			clearTimeout(timeOutId);
		};
	}, [notice, dispatch]);

	const error = {
		color: 'red',
		background: 'lightgrey',
		fontSize: 20,
		borderStyle: 'solid',
		borderRadius: 5,
		padding: 10,
		marginBottom: 10,
	};

	const success = {
		color: 'green',
		background: 'lightgrey',
		fontSize: 20,
		borderStyle: 'solid',
		borderRadius: 5,
		padding: 10,
		marginBottom: 10,
	};

	if (!notice) {
		return null;
	}

	return (
		<div style={notice.type === 'success' ? success : error}>
			{notice.message}
		</div>
	);
};

// eslint-disable-next-line react/display-name
export const Togglable = forwardRef(
	({ children, openBtn, altText, closeBtn }, ref) => {
		const [visible, setVisible] = useState(false);

		const toggleVisibility = () => {
			setVisible(!visible);
		};

		useImperativeHandle(ref, () => {
			return { toggleVisibility };
		});

		const renderChildren = () => {
			//Check if there were multiple children
			if (Array.isArray(children)) {
				return (
					<div className='children'>
						{children[0]}
						<button onClick={toggleVisibility}>{closeBtn}</button>
						{children[1]}
					</div>
				);
			} else {
				return (
					<div className='children'>
						{children}
						<button onClick={toggleVisibility}>{closeBtn}</button>
					</div>
				);
			}
		};

		const renderHidden = () => (
			<div className='hidden'>
				{altText}
				<button onClick={toggleVisibility}>{openBtn}</button>
			</div>
		);

		return <>{visible ? renderChildren() : renderHidden()}</>;
	}
);

Togglable.propTypes = {
	openBtn: PropTypes.string.isRequired,
	closeBtn: PropTypes.string.isRequired,
};

/**
 * LabelledInput component
 * @param {*} param0
 * @returns {JSX}
 */
export const LabelledInput = ({ label, value, onChange, type }) => (
	<div className='input'>
		<label htmlFor={label}>{label}: </label>
		<br />
		<input type={type} id={label} value={value} onChange={onChange} />
		<br />
	</div>
);

/**
 * Custom hook: useLabelledInput
 * @returns {Object}
 */
const useLabelledInput = (type, label) => {
	const [value, setValue] = useState('');

	const onChange = (event) => {
		setValue(event.target.value);
	};

	const clear = () => {
		setValue('');
	};

	return [
		{
			type,
			label,
			value,
			onChange,
		},
		clear,
	];
};

/**
 * Login component
 * @returns {JSX}
 */
export const Login = ({ nameHandler, passHandler }) => {
	const dispatch = useDispatch();
	const [password, clearPassword] = useLabelledInput('password', 'Password');
	const [username, clearUsername] = useLabelledInput('text', 'Username');

	const submitHandler = async (e) => {
		e.preventDefault();
		dispatch(login({ username: username.value, password: password.value }));
		clearUsername();
		clearPassword();
	};

	return (
		<div className='login'>
			<h2>Log in to application</h2>
			<form onSubmit={submitHandler}>
				<LabelledInput {...username} />
				<LabelledInput {...password} />
				<button type='submit'>Login</button>
			</form>
		</div>
	);
};

/**
 * BlogForm-component
 * @returns {JSX}
 */
export const BlogForm = () => {
	const [title, clearTitle] = useLabelledInput('text', 'Title');
	const [author, clearAuthor] = useLabelledInput('text', 'Author');
	const [url, clearUrl] = useLabelledInput('url', 'Url');
	const dispatch = useDispatch();

	const handleSubmit = (event) => {
		event.preventDefault();
		const newBlog = {
			title: title.value,
			author: author.value,
			url: url.value,
			likes: 0,
			comments: [],
		};
		dispatch(createBlog(newBlog));
		clearTitle();
		clearAuthor();
		clearUrl();
	};

	return (
		<form onSubmit={handleSubmit} className='blog-creator'>
			<h2>create new</h2>
			<LabelledInput {...title} />
			<LabelledInput {...author} />
			<LabelledInput {...url} />
			<button type='submit'>Create new blog</button>
		</form>
	);
};

/**
 * Blog-component
 * @param {Object} blog
 * @returns
 */
export const Blog = () => {
	const history = useHistory();
	const id = useParams().id;
	const dispatch = useDispatch();
	const currentUser = useSelector((state) => state.loginUser);
	const blog = useSelector((state) =>
		state.blogs.find((blog) => blog.id === Number(id))
	);
	const [comment, clearComment] = useLabelledInput('text', 'Comment');

	if (!blog) return null;
	const { title, author, url, likes, user, comments } = blog;

	const handleLike = () => {
		dispatch(likeBlog(blog));
	};
	const handleDelete = () => {
		if (window.confirm(`Remove blog ${blog.title} by ${blog.author}?`)) {
			dispatch(removeBlog(blog));
			history.push('/');
		}
	};

	const handleNewComment = (e) => {
		e.preventDefault();
		dispatch(addCommentToBlog(blog, comment.value));
		clearComment();
	};

	const renderDeleteBtn = () => {
		//It'll check if the user exists with blog.user? before checking if the names are equal.
		if (user.name === currentUser.name)
			return <button onClick={handleDelete}>remove</button>;
	};

	return (
		<div className='blog'>
			<h2>
				{title} {author}
			</h2>
			<div>
				<div>{url}</div>
				<div>
					likes: {likes || 0} <button onClick={handleLike}>like</button>
				</div>
				<div>added by {user.name}</div>
				<div>{renderDeleteBtn()}</div>
			</div>
			<div>
				<h3>comments</h3>
				<form onSubmit={handleNewComment}>
					<LabelledInput {...comment} />
					<button>add comment</button>
				</form>
				{comments && (
					<ul>
						{comments.map((comment) => (
							<li className=''>{comment}</li>
						))}
					</ul>
				)}
			</div>
		</div>
	);
};

/**
 * BlogList-component
 * @returns {JSX}
 */
export const BlogList = () => {
	const blogs = useSelector((state) => state.blogs);

	const blogStyle = {
		paddingTop: 10,
		paddingLeft: 2,
		border: 'solid',
		borderWidth: 1,
		marginBottom: 5,
	};
	return (
		<>
			<h2>Blogs</h2>
			{blogs.map((blog) => (
				<div style={blogStyle}>
					<Link to={`/blogs/${blog.id}`}>
						{blog.title} {blog.author}
					</Link>
				</div>
			))}
		</>
	);
};

export const User = () => {
	const id = useParams().id;
	const user = useSelector((state) => state.users[id]);
	if (!user) return null;
	return (
		<>
			<h2>{user.name}</h2>
			<h3>added blogs</h3>
			<ul>
				{user.blogs.map(({ title, blogId }) => (
					<li key={blogId}>{title}</li>
				))}
			</ul>
		</>
	);
};

export const Users = () => {
	const users = useSelector((state) => state.users);
	return (
		<>
			<h2>Users</h2>
			<table>
				<tbody>
					<tr>
						<th>Name</th>
						<th>Blogs created</th>
					</tr>
					{Object.keys(users).map((id) => (
						<tr key={id}>
							<td>
								<Link to={`users/${id}`}>{users[id]['name']}</Link>
							</td>
							<td>{users[id]['blogs'].length}</td>
						</tr>
					))}
				</tbody>
			</table>
		</>
	);
};

export const NavBar = () => {
	const dispatch = useDispatch();
	const user = useSelector((state) => state.loginUser);
	const NavBar = {
		backgroundColor: '#c9c9c9',
	};
	const NavItem = {
		margin: '10px',
	};
	const handleLogout = () => {
		// Log out the current user.
		dispatch(logout());
	};

	return (
		<div style={NavBar}>
			{user && (
				<>
					<Link to='/' style={NavItem}>
						Blogs
					</Link>
					<Link to='/users' style={NavItem}>
						Users
					</Link>
					{user.name} logged in
					<button onClick={handleLogout} style={NavItem}>
						Logout
					</button>
				</>
			)}
		</div>
	);
};
