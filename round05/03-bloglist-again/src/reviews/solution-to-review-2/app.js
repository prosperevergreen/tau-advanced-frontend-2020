/** @format */

import React, { useEffect, useRef } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import {
	BlogList,
	BlogForm,
	Login,
	Notification,
	Togglable,
	Users,
	User,
	Blog,
	NavBar,
} from './components';
import { getAllBlogs } from './reducer-blog';
import { useSelector, useDispatch } from 'react-redux';
import { attemptLogin } from './reducer-login';
import { getUsers } from './reducer-user';
export { default as store } from './store';

// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE//
// ------------------------------------------------------------ //
export const commitSHA = '-93430b5-';
// ------------------------------------------------------------ //

export const App = () => {
	const dispatch = useDispatch();
	const user = useSelector((state) => state.loginUser);
	const blogs = useSelector((state) => state.blogs);

	const creatorRef = useRef();

	useEffect(() => {
		// If There are blogs, get users.
		if (blogs) {
			dispatch(getUsers(blogs));
		}
	}, [blogs, dispatch]);

	useEffect(() => {
		// If user state changes and exists, get all blogs to Redux-store.
		if (user) {
			dispatch(getAllBlogs());
		}
	}, [user, dispatch]);

	useEffect(() => {
		// Attempt to login when first rendered.
		dispatch(attemptLogin());
	}, [dispatch]);

	const renderApp = () => {
		if (user === null) {
			return (
				<div>
					<Login />
				</div>
			);
		} else {
			return (
				<>
					<Switch>
						<Route exact path='/'>
							<Togglable ref={creatorRef} openBtn='Creator' closeBtn='close'>
								<BlogForm />
							</Togglable>
							<BlogList />
						</Route>
						<Route exact path='/users'>
							<Users />
						</Route>
						<Route exact path='/users/:id'>
							<User />
						</Route>
						<Route exact path='/blogs/:id'>
							<Blog />
						</Route>
					</Switch>
				</>
			);
		}
	};

	return (
		<Router>
			<NavBar />
			<h1>blog app</h1>
			<Notification />
			{renderApp()}
		</Router>
	);
};
