/** @format */

import { newNotification } from './reducer-notification';
import { blogService } from './services';

//CONSTANTS
const GET_ALL_BLOGS = 'GET_ALL_BLOGS';
const CREATE_BLOG = 'CREATE_BLOG';
const LIKE_BLOG = 'LIKE_BLOG';
const COMMENT_BLOG = 'COMMENT_BLOG';
const REMOVE_BLOG = 'REMOVE_BLOG';

//ACTION CREATORS
export const getAllBlogs = () => {
	return async (dispatch) => {
		const blogs = await blogService.getAll();

		dispatch({
			type: GET_ALL_BLOGS,
			payload: blogs,
		});
	};
};

export const addCommentToBlog = (blog, comment) => {
	return async (dispatch) => {
		try {
			blog.comments = blog.comments.concat(comment);

			const updatedBlogs = await blogService.update(blog.id, blog);
			dispatch({
				type: COMMENT_BLOG,
				payload: updatedBlogs,
			});
		} catch (e) {}
	};
};

export const likeBlog = (blog) => async (dispatch) => {
	try {
		blog.likes += 1;
		const updatedBlog = await blogService.update(blog.id, blog);
		dispatch({ type: LIKE_BLOG, payload: updatedBlog });
	} catch (e) {
		console.log(e);
	}
};

export const removeBlog = (blog) => async (dispatch) => {
	try {
		await blogService.remove(blog.id);
		dispatch({ type: REMOVE_BLOG, payload: blog.id });
		dispatch(
			newNotification({
				message: `blog ${blog.title} by ${blog.author} removed`,
				type: 'success',
			})
		);
	} catch (e) {
		console.log(e);
	}
};

export const createBlog = (newBlog) => {
	return async (dispatch) => {
		const savedBlog = await blogService.create(newBlog);

		dispatch({
			type: CREATE_BLOG,
			payload: savedBlog,
		});
		dispatch(
			newNotification({
				message: `a new blog ${newBlog.title} by ${newBlog.author} added`,
				type: 'success',
			})
		);
	};
};

//REDUCER
const reducer = (state = [], action) => {
	switch (action.type) {
		case GET_ALL_BLOGS:
			const blogs = action.payload;
			sortByLikes(blogs);
			return blogs;
		case CREATE_BLOG:
			const newBlogs = state.concat(action.payload);
			sortByLikes(newBlogs);
			return newBlogs;
		case LIKE_BLOG || COMMENT_BLOG:
			const updatedBlog = action.payload;
			const updatedBlogs = state
				.filter((blog) => blog.id !== updatedBlog.id)
				.concat(updatedBlog);
			sortByLikes(updatedBlogs);
			return updatedBlogs;
		case REMOVE_BLOG:
			return state.filter((blog) => blog.id !== action.payload);
		default:
			return state;
	}
};

// UTILS
const sortByLikes = (blogs) => {
	blogs.sort((a, b) => {
		const compareB = b.likes || 0;
		const compareA = a.likes || 0;
		return compareB - compareA;
	});
};

export default reducer;
