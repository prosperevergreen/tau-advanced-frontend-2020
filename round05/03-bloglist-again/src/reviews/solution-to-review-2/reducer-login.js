/** @format */

import { newNotification } from './reducer-notification';
import { blogService, loginService } from './services';

// CONSTANTS
const LOGIN = 'LOGIN';
const ATTEMPT_LOGIN = 'ATTEMPT_LOGIN';
const LOGOUT = 'LOGOUT';
const NEW_NOTIFICATION = 'NEW_NOTIFICATION';

// ACTION CREATORS
export const login = (user) => async (dispatch) => {
	try {
		const loggedInUser = await loginService.login(user);
		dispatch({
			type: LOGIN,
			payload: loggedInUser,
		});
		dispatch(
			newNotification({ message: 'Logged in succesfully', type: 'success' })
		);
	} catch (e) {
		dispatch({
			type: NEW_NOTIFICATION,
			payload: { message: 'wrong username or password', type: 'error' },
		});
	}
};

export const attemptLogin = () => ({
	type: ATTEMPT_LOGIN,
});

export const logout = () => ({
	type: LOGOUT,
});

// REDUCER
const reducer = (state = null, action) => {
	switch (action.type) {
		case LOGIN:
			// Add them to local storage.
			window.localStorage.setItem('user', JSON.stringify(action.payload));
			// Set their token
			blogService.setToken(action.payload.token);
			// Add them to redux
			return action.payload;
		case LOGOUT:
			// Remove them from local storage
			window.localStorage.removeItem('user');
			// Remove their token
			blogService.setToken(null);
			// Remove them from redux:
			return null;
		case ATTEMPT_LOGIN:
			// Get the user from local storage
			const user = JSON.parse(window.localStorage.getItem('user'));
			// Set their token
			if (user) blogService.setToken(user.token);
			return user;
		default:
			return state;
	}
};

export default reducer;
