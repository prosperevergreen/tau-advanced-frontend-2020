import { userService } from "./services";

const ACTION_TYPE = { get: "GET_USERS", clear: "CLEAR_USERS" };

const reducer = (state = [], action) => {
	switch (action.type) {
		case ACTION_TYPE.get: {
			return action.data;
		}
		case ACTION_TYPE.clear: {
			return [];
		}
		default:
			return state;
	}
};

export const getUsers = () => async (dispatch) => {
	const users = await userService.getAll();
	dispatch({
		type: ACTION_TYPE.get,
		data: users,
	});
};

export const clearUsers = () => ({
	type: ACTION_TYPE.clear,
});

export default reducer;
