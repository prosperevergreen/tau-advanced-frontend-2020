import { useSelector } from "react-redux";
import {
	BrowserRouter as Router,
	Switch,
	Route
} from "react-router-dom";

import { LoginPage, AppContent } from "./components";

export { default as store } from "./store";

// *** ENTER COMMIT SHA OF YOUR REPO IN HERE ***
export const commitSHA = "dc5e1a3";

export const App = () => {
	const loginUser = useSelector((state) => state.loginUser);

	return (
		<Router>
			<Switch>
				<Route path="/">
					{loginUser ? <AppContent user={loginUser} /> : <LoginPage />}
				</Route>
			</Switch>
		</Router>
	);
};
