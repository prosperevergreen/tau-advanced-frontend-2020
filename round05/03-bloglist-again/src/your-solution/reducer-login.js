import { loginService,blogService } from "./services";
import { setNotification } from "./reducer-notification";


const initialState = null

const ACTION_TYPE = { login: "LOGIN", logout: "LOGOUT" };

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case ACTION_TYPE.login: {
			return action.data;
		}
		case ACTION_TYPE.logout: {
			window.localStorage.clear();
			return null;
		}
		default:
			return state;
	}
};

export const loginUserAsync = (userCred) => async (dispatch) => {
	try {
		const user = await loginService.login(userCred);

    window.localStorage.setItem(
      "loggedBlogAppUser",
      JSON.stringify(user)
    );
    blogService.setToken(user.token)
		dispatch({
			type: ACTION_TYPE.login,
			data: user,
		});
    const notification = {
      msg: `${user.name} logged in`,
      error: false
    }
		dispatch(setNotification(notification, 5));
	} catch (error) {
    const notification = {
      msg: "wrong username or password",
      error: true
    }
		dispatch(setNotification(notification, 5));
	}
};

export const loginUserSync =(userJSON)=>({
  type: "LOGIN",
  data: JSON.parse(userJSON),
})

export const logoutUser = () => ({
	type: ACTION_TYPE.logout,
});

export default reducer;
