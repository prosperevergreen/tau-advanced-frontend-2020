import React, { useState, useEffect, useImperativeHandle } from "react";
import { useSelector, useDispatch } from "react-redux";
import PropTypes from "prop-types";
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Link,
	useParams,
	useHistory,
} from "react-router-dom";

import { loginUserAsync, loginUserSync, logoutUser } from "./reducer-login";
import {
	getBlogs,
	createBlog,
	likeBlog,
	deleteBlog,
	commentBlog,
  clearBlogs
} from "./reducer-blog";
import { getUsers, clearUsers} from "./reducer-user";

//
// useField
//
export const useField = (type, name) => {
	const [value, setValue] = useState("");

	const onChange = (event) => {
		setValue(event.target.value);
	};
	const reset = () => {
		setValue("");
	};

	return {
		name,
		type,
		value,
		onChange,
		reset,
	};
};

//
// Togglable
//
export const Togglable = React.forwardRef((props, ref) => {
	const [visible, setVisible] = useState(false);

	const hideWhenVisible = { display: visible ? "none" : "" };
	const showWhenVisible = { display: visible ? "" : "none" };

	const toggleVisibility = () => {
		setVisible(!visible);
	};

	useImperativeHandle(ref, () => {
		return {
			toggleVisibility,
		};
	});

	return (
		<div>
			<div style={hideWhenVisible}>
				<button onClick={toggleVisibility}>{props.buttonLabel}</button>
			</div>

			<div style={showWhenVisible}>
				{props.children}
				<button onClick={toggleVisibility}>cancel</button>
			</div>
		</div>
	);
});

Togglable.propTypes = {
	buttonLabel: PropTypes.string.isRequired,
};

//
// Notification
//
export const Notification = () => {
	const notification = useSelector((state) => state.notification);

	if (notification.msg === "") {
		return null;
	}

	const style = {
		color: notification.msg && notification.error ? "red" : "green",
		background: "lightgrey",
		fontSize: 20,
		borderStyle: "solid",
		borderRadius: 5,
		padding: 10,
		marginBottom: 10,
	};

	return <div style={style}>{notification.msg}</div>;
};

//
// LoginPage
//
export const LoginPage = () => {
	const username = useField("text", "username");
	const password = useField("password", "password");
	const dispatch = useDispatch();
  const history = useHistory();

	useEffect(() => {
		const loggedUserJSON = window.localStorage.getItem("loggedBlogAppUser");
		if (loggedUserJSON) {
			dispatch(loginUserSync(loggedUserJSON));
		}
    history.replace("/")
	}, []);

	// Remove the reset field
	const noReset = ({ reset, ...others }) => others;
	const handleLogin = async (event) => {
		event.preventDefault();
		dispatch(loginUserAsync({ username: username.value, password: password.value }));
	};

	return (
		<div>
			<h2>log in to application</h2>
			<Notification />
			<form onSubmit={handleLogin}>
				<div>
					username &nbsp;
					<input {...noReset(username)} required />
				</div>
				<div>
					password &nbsp;
					<input {...noReset(password)} required />
				</div>
				<div>
					<button type="submit">login</button>
				</div>
			</form>
		</div>
	);
};

//
// BlogForm
//
export const BlogForm = () => {
	const dispatch = useDispatch();
	const newTitle = useField("text", "title");
	const newAuthor = useField("text", "author");
	const newUrl = useField("text", "url");
	const blogFormRef = React.createRef();

	const handleSubmit = (e) => {
		e.preventDefault();
		const data = {
			title: newTitle.value,
			author: newAuthor.value,
			url: newUrl.value,
			likes: 0,
			comments: [],
		};
		dispatch(createBlog(data));
		newAuthor.reset();
		newTitle.reset();
		newUrl.reset();
		blogFormRef.current.toggleVisibility();
	};

	// Remove the reset field
	const noReset = ({ reset, ...others }) => others;

	return (
		<Togglable buttonLabel="create blog" ref={blogFormRef}>
			<h3>new blog</h3>

			<form onSubmit={handleSubmit}>
				<div>
					title: <input {...noReset(newTitle)} required />
				</div>

				<div>
					author: <input {...noReset(newAuthor)} required />
				</div>

				<div>
					url: <input {...noReset(newUrl)} required />
				</div>

				<div>
					<button type="submit">create</button>
				</div>
			</form>
		</Togglable>
	);
};

//
// BlogList
//
export const BlogList = ({ blogs }) => {
	const style = {
		border: "1px solid #ccc",
		padding: "8px",
		margin: "4px auto",
	};
	return blogs
		.sort((blog1, blog2) => blog2.likes - blog1.likes)
		.map((blog) => (
			<div key={blog.id} style={style}>
				<Link to={`/blogs/${blog.id}`}>{blog.title}</Link>
			</div>
		));
};

//
// Blog
//
export const Blog = () => {
	const dispatch = useDispatch();
	const comment = useField("text", "comment");
	const matchedBlogId = useParams().id;
	const blogs = useSelector((state) => state.blogs);
	const loginUser = useSelector((state) => state.loginUser);
	const history = useHistory();
	const blog = blogs.find(
		(blog) => parseInt(blog.id, 10) === parseInt(matchedBlogId, 10)
	);

	const isUserBlog = blog.user.name === loginUser.name && blog.user.username === loginUser.username;

	const handleLike = () => {
		const data = {
			...blog,
			likes: blog.likes + 1,
		};
		dispatch(likeBlog(data));
	};

	const handleDelete = () => {
		dispatch(deleteBlog(blog));
		history.push("/blogs");
	};

	const handleComment = (e) => {
		e.preventDefault();
		const data = {
			...blog,
			comments: blog.comments.concat(comment.value),
		};
		dispatch(commentBlog(data));
		comment.reset();
	};

	// Remove the reset field
	const noReset = ({ reset, ...others }) => others;

	if (!blog) return <div>Loading...</div>;
	return (
		<div>
			<h2>{blog.title}</h2>
			<p>
				<a href={blog.url}>{blog.url}</a>
				<br />
				{blog.likes} likes <button onClick={handleLike}>like</button>{" "}
				{isUserBlog && (
					<button onClick={handleDelete}>delete</button>
				)}
				<br />
				added by {blog.author}
			</p>
			<div>
				<h3>Comments</h3>
				<form onSubmit={handleComment}>
					<input {...noReset(comment)} required />{" "}
					<button type="submit">add comment</button>
				</form>

				{blog.comments.length === 0 ? (
					<h3>No comments for this blog</h3>
				) : (
					<ul>
						{blog.comments.map((comment, i) => (
							<li key={`${blog.id}-${i}`}>{comment}</li>
						))}
					</ul>
				)}
			</div>
		</div>
	);
};


//
// Users
//
const UserList = () => {
	const dispatch = useDispatch();
	const users = useSelector((state) => state.users);

	useEffect(() => {
		dispatch(getUsers());
	}, []);

	if (users.length === 0) return <div>Loading...</div>;

	return (
		<table>
			<thead>
				<tr>
					<th>Users</th>
					<th>blogs created</th>
				</tr>
			</thead>
			<tbody>
				{users
					.sort((user1, user2) => user2.blogs.length - user1.blogs.length)
					.map((user) => (
						<tr key={user.id}>
							<td>
								<Link to={`/users/${user.id}`}>{user.name}</Link>
							</td>
							<td>{user.blogs.length}</td>
						</tr>
					))}
			</tbody>
		</table>
	);
};

//
// User
//
const User = () => {
	const matchedUserId = useParams().id;
	const users = useSelector((state) => state.users);
	const user = users.find(
		(user) => parseInt(user.id, 10) === parseInt(matchedUserId, 10)
	);

	if (!user) return <div>Loading...</div>;
	return (
		<div>
			<h2>{user.name}</h2>
			{user.blogs.length === 0 ? (
				<h3>No blogs added</h3>
			) : (
				<>
					<h3>added blogs</h3>
					<ul>
						{user.blogs.map((blog) => (
							<li key={blog.id}>{blog.title}</li>
						))}
					</ul>
				</>
			)}
		</div>
	);
};



//
// AppContent
//
export const AppContent = ({ user }) => {
	const blogs = useSelector((state) => state.blogs);

	const dispatch = useDispatch();
	const handleLogout = () => {
		dispatch(logoutUser());
    dispatch(clearBlogs())
    dispatch(clearUsers())
	};

	useEffect(() => {
		dispatch(getBlogs());
	}, []);

	return (
		<Router>
			<div style={{ backgroundColor: "#ccc", padding: "16px" }}>
				<Link to="/">blogs</Link>
				{" | "}
				<Link to="/users">user</Link>
				{" | "}
				{user.name} logged in <button onClick={handleLogout}>logout</button>
			</div>
			<h2>blog app</h2>
			<Notification />

			<Switch>
				<Route path="/users/:id">
					<User />
				</Route>
				<Route path="/users">
					<UserList />
				</Route>
				<Route path="/blogs/:id">
					<Blog />
				</Route>
				<Route path="/">
					<BlogForm />
					<BlogList blogs={blogs} />
				</Route>
			</Switch>
		</Router>
	);
};
