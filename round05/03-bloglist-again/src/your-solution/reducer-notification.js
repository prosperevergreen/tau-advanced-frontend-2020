const initialState = { msg: "", timeout: null, error: false };

const ACTION_TYPES = {
	notify: "SET_NOTIFICATION",
	clear: "CLEAR_NOTIFICATION",
};

const reducer = (state = initialState, action) => {

	switch (action.type) {
		case ACTION_TYPES.notify: {
			return {...action.data};
		}
		case ACTION_TYPES.clear: {
			const timeout = state.timeout;
			timeout && clearTimeout(timeout)
			return {
				msg: "",
				timeout: null,
        error:false
			};
		}
		default:
			return state;
	}
};

export const setNotification = (data, time) => {
	return (dispatch) => {
		dispatch(clearNotification())
		const timeout = setTimeout(() => dispatch(clearNotification()), time * 1000);
		dispatch({
			type: ACTION_TYPES.notify,
			data: {...data, timeout},
		});
	};
};

export const clearNotification = () => {
	return {
		type: ACTION_TYPES.clear,
	};
};

export default reducer;
