import { blogService } from "./services";
import { setNotification } from "./reducer-notification";

const initialState = [];

const ACTION_TYPES = {
	like: "LIKE_BLOG",
	create: "ADD_BLOG",
	get: "GET_BLOGS",
	delete: "DELETE_BLOG",
	comment: "COMMENT_BLOG",
	clear: "CLEAR_BLOGS",
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case ACTION_TYPES.like: {
			const blog = action.data;
			return state.map((item) => (item.id === blog.id ? blog : item));
		}
		case ACTION_TYPES.comment: {
			const blog = action.data;
			return state.map((item) => (item.id === blog.id ? blog : item));
		}
		case ACTION_TYPES.create: {
			const blog = action.data;
			return state.concat(blog);
		}
		case ACTION_TYPES.get: {
			return action.data;
		}
		case ACTION_TYPES.delete: {
			return state.filter((blog) => blog.id !== action.data);
		}
		case ACTION_TYPES.clear: {
			return [];
		}

		default:
			return state;
	}
};

export const clearBlogs = () => ({
	type: ACTION_TYPES.clear,
});

export const likeBlog = (data) => async (dispatch) => {
	try {
		const blog = await blogService.update(data.id, data);
		dispatch({
			type: ACTION_TYPES.like,
			data: blog,
		});
	} catch (err) {
		const notification = {
			msg: "Action was unsuccessful",
			error: true,
		};
		dispatch(setNotification(notification, 5));
	}
};

export const commentBlog = (data) => async (dispatch) => {
	try {
		const blog = await blogService.update(data.id, data);
		dispatch({
			type: ACTION_TYPES.comment,
			data: blog,
		});
		const comment = blog.comments[blog.comments.length - 1];
		const notification = {
			msg: `Commented '${comment}' on blog '${blog.title}'`,
			error: false,
		};
		dispatch(setNotification(notification, 5));
	} catch (err) {
		const notification = {
			msg: "Action was unsuccessful",
			error: true,
		};
		dispatch(setNotification(notification, 5));
	}
};

export const createBlog = (blogContent) => async (dispatch) => {
	try {
		const blog = await blogService.create(blogContent);
		dispatch({
			type: ACTION_TYPES.create,
			data: blog,
		});
		const notification = {
			msg: `Added blog '${blog.title}' by '${blog.author}' was added`,
			error: false,
		};
		dispatch(setNotification(notification, 5));
	} catch (err) {
		const notification = {
			msg: "Action was unsuccessful",
			error: true,
		};
		dispatch(setNotification(notification, 5));
	}
};

export const deleteBlog = (blog) => async (dispatch) => {
	try {
		const blogId = blog.id;
		await blogService.remove(blogId);
		dispatch({
			type: ACTION_TYPES.delete,
			data: blogId,
		});
		const notification = {
			msg: `Deleted blog '${blog.title}' by ${blog.author}`,
			error: false,
		};
		dispatch(setNotification(notification, 5));
	} catch (err) {
		const notification = {
			msg: "Action was unsuccessful",
			error: true,
		};
		dispatch(setNotification(notification, 5));
	}
};

export const getBlogs = () => async (dispatch) => {
	const blogs = await blogService.getAll();
	dispatch({
		type: ACTION_TYPES.get,
		data: blogs,
	});
};

export default reducer;
