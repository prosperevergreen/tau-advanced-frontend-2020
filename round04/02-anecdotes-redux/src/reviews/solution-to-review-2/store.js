import { createStore, combineReducers } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import anecdoteReducer from './anecdoteReducer'
import filterReducer from './filterReducer'
//import notificationReducer from './notificationReducer'



export const reducer = combineReducers({
    anecdotes: anecdoteReducer,
    filter: filterReducer,
    //notifications: notificationReducer
  })

const store = createStore(reducer,
    composeWithDevTools())


export default store