import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { voteForAnecdote, createAnecdote } from './anecdoteReducer'
import { filterChange } from './filterReducer'


export const Notification = () => {
  const style = {
    border: 'solid',
    padding: 10,
    borderWidth: 1
  }
  return (
    <div style={style}>
      render here notification...
    </div>
  )
}

export const AnecdotesForm = (props) => {
  const dispatch = useDispatch() 
  const createNew = (event) => {
    event.preventDefault()
    const content = event.target.note.value
    event.target.note.value = ''
    dispatch(createAnecdote(content))
  }


  return (
    <form onSubmit={createNew}>
     <h2>create new</h2>
      <input name="note" />
      <button type="submit">create</button>
    </form>
  )
}



export const AnecdotesList = () => {
  const dispatch = useDispatch()
  const anecdotes = useSelector(state => {    
    if ( state === "ALL") {      
      return state.anecdotes
    }    
      console.log(state.anecdotes.filter(note => note !== state.filter))
      return state.anecdotes.filter(note => note !== state.filter)      
    })
  return(
    <div>
    {anecdotes.map(anecdote =>
        <div key={anecdote.id}>
          <div>
            {anecdote.content}
          </div>
          <div>
            has {anecdote.votes}
            <button onClick={() => dispatch(voteForAnecdote(anecdote.id))}>vote</button>
          </div>
        </div>
      )}
    </div>
  )
  }


export const Filter = () => {
  const dispatch = useDispatch() 
  const handleChange = (event) => {
    const toFilter = event.target.value
    dispatch(filterChange(toFilter))
  }
  const style = {
    marginBottom: 10
  }

  return (
    <div style={style}>
      filter <input onChange={handleChange} />
   </div> 
  )
}




