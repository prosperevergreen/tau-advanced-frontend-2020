import React from 'react'
import { createStore } from 'redux'
import { AnecdotesForm, AnecdotesList, Filter } from './components'
import { reducer } from './store'


// ** enter commit sha of your repository in here **
export const commitSHA = 'd2119f1';

export const store = createStore(reducer)


export const App = () => {

  return (
    <div>
      <h2>Anecdotes</h2>
      <Filter/>
      <AnecdotesList/>
      <AnecdotesForm/>
    </div>
  )
}
