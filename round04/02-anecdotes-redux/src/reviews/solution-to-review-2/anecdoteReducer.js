const anecdotesAtStart = [
  'If it hurts, do it more often',
  'Adding manpower to a late software project makes it later!',
  'The first 90 percent of the code accounts for the first 90 percent of the development time...The remaining 10 percent of the code accounts for the other 90 percent of the development time.',
  'Any fool can write code that a computer can understand. Good programmers write code that humans can understand.',
  'Premature optimization is the root of all evil.',
  'Debugging is twice as hard as writing the code in the first place. Therefore, if you write the code as cleverly as possible, you are, by definition, not smart enough to debug it.'
]

const getId = () => (100000 * Math.random()).toFixed(0)

const asObject = (anecdote) => {
  return {
    content: anecdote,
    id: getId(),
    votes: 0
  }
}

export const createAnecdote = (content) => {
  return {
    type: 'NEW_NOTE',
    content
  }
}

export const voteForAnecdote = (id) => {
  return {
    type: 'ADD_VOTE',
    id
  }
}


export const initialState = anecdotesAtStart.map(asObject)

const anecdoteReducer = (state = initialState, action) => {
  console.log('state now: ', state)
  console.log('action', action)
  switch (action.type) {
    case 'ADD_VOTE':
      const toVote = state.find(theID => action.id === theID.id)
      const newState = state.filter(theID => action.id !== theID.id);
      const toBeSorted = [...newState , 
        {
        ...toVote,
        votes: toVote.votes +1
        }
      ];
      return toBeSorted.sort((a, b) => a.votes < b.votes)
    case 'NEW_NOTE': 
      return [...state, {
      content: action.content,
      id: getId(),
      votes: 0}
      ]
    default:
     return state
  }
}

export default anecdoteReducer