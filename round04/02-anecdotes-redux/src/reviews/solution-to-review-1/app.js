import React from 'react';
import { AnecdoteList, AnecdoteForm, Notification, Filter } from "./components";

export { default as store } from './store';

// ** enter commit sha of your repository in here **
export const commitSHA = 'e014806';

export const App = () => {
  return (
    <div>
      <Notification />
      <Filter />
      <AnecdoteList />
      <AnecdoteForm />
    </div>
  )
}