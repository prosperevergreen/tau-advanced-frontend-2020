export const addNoti = content => {
    return {
        type: "ADD",
        data: { content }
    }
}

export const removeNoti = () => {
    return {
        type: "REMOVE",
    }
}

const notificationReducer = (state = null, action) => {
    switch(action.type) {
        case "ADD":
            return action.data.content;
        case "REMOVE":
            return null;
        default:
            return state;
    }
}

export default notificationReducer
