import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { voteOf, newNote } from "./anecdoteReducer";
import { addNoti, removeNoti } from "./notificationReducer"
import { filterText } from "./filterReducer"

export const Notification = () => {
  const notification = useSelector(state => state.notification)
  const style = {
    border: 'solid',
    padding: 10,
    borderWidth: 1
  }
  if (!notification) return <></>;

  return (
    <div style={style}>
      {notification}
    </div>
  )
}

const Anecdote = ({ anecdote, vote }) => (
  <div key={anecdote.id}>
    <div>
      {anecdote.content}
    </div>
    <div>
      has {anecdote.votes}
      <button onClick={() => vote(anecdote.id)}>vote</button>
    </div>
  </div>
)

export const AnecdoteList = () => {
  const anecdotes = useSelector(state => state.anecdote
    .filter(item => item.content.toLowerCase().includes(state.filter))
    .sort((a,b) => b.votes -a.votes))
  const dispatch = useDispatch()

  const vote = (id) => {
    console.log('vote', id)
    dispatch(voteOf(id))
    dispatch(addNoti(`You vote "${anecdotes.find(item => item.id === id).content}"`));
    setTimeout(() =>dispatch(removeNoti()), 5000);
  }

  return (
    anecdotes.map(anecdote => 
      <Anecdote key={anecdote.id} anecdote={anecdote} vote={vote} />
    )
  )
}

export const AnecdoteForm = () => {
  const dispatch = useDispatch()
  const createNote = e => {
    e.preventDefault();
    const content = e.target.note.value;
    e.target.note.value = "";
    dispatch(newNote(content));
    dispatch(addNoti(`You add "${addNoti(content)}"`));
    setTimeout(() =>dispatch(removeNoti()), 5000);
  }

 return(
   <>
    <h2>create new</h2>
    <form onSubmit={createNote}>
      <div><input name="note"/></div>
      <button type="submit">create</button>
    </form>
  </>
)}

export const Filter = () => {
  const dispatch = useDispatch();

  const style = {
    marginBottom: 10
  };

  const handleChange = e => {
    e.preventDefault();
    dispatch(filterText(e.target.value))
  };

  return (
    <div style={style}>
      filter <input onChange={handleChange} />
    </div>
  )
}
