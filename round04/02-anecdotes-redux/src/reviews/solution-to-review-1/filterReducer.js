export const filterText = content => {
    return {
        type: "NEW_FILTER",
        data: {content}
    }
}

const filterReducer = (state = "", action) => {
    switch(action.type) {
        case "NEW_FILTER":
            return action.data.content;
        default:
            return state;
    }
}

export default filterReducer;