const anecdotesAtStart = [
	"If it hurts, do it more often",
	"Adding manpower to a late software project makes it later!",
	"The first 90 percent of the code accounts for the first 90 percent of the development time...The remaining 10 percent of the code accounts for the other 90 percent of the development time.",
	"Any fool can write code that a computer can understand. Good programmers write code that humans can understand.",
	"Premature optimization is the root of all evil.",
	"Debugging is twice as hard as writing the code in the first place. Therefore, if you write the code as cleverly as possible, you are, by definition, not smart enough to debug it.",
];

const getId = () => (100000 * Math.random()).toFixed(0);

const asObject = (anecdote) => {
	return {
		content: anecdote,
		id: getId(),
		votes: 0,
	};
};

const initialState = anecdotesAtStart.map(asObject);

const ACTION_TYPES = { vote: "INC_VOTE", create: "ADD_ANECDOTE" };

const reducer = (state = initialState, action) => {
	console.log("state now: ", state);
	console.log("action", action);

	switch (action.type) {
		case ACTION_TYPES.vote: {
			const id = action.data;
			return state.map((item) =>
				item.id === id ? { ...item, votes: item.votes + 1 } : item
			);
		}
		case ACTION_TYPES.create: {
      const anecdote = action.data;
      return state.concat(anecdote)
		}

		default:
			return state;
	}
};

export const increaseVote = (id) => {
	return {
		type: ACTION_TYPES.vote,
		data: id,
	};
};

export const createAnecdote = (anecdote) => {
	return {
		type: ACTION_TYPES.create,
		data: asObject(anecdote),
	};
};

export default reducer;
