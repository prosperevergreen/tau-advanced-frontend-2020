import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { createAnecdote, increaseVote } from "./anecdoteReducer";
import { setNotification, clearNotification } from "./notificationReducer";
import { setFilter } from "./filterReducer";

export const Notification = () => {
	const notification = useSelector((state) => state.notification);
	const style = {
		border: "solid",
		padding: 10,
		borderWidth: 1,
	};
	if (notification === "") return null;
	return <div style={style}>{notification}</div>;
};

export const AnecdoteForm = () => {
	const [newAnecdote, setNewAnecdote] = useState("");
	const dispatch = useDispatch();

	const handleCreateAnecdote = (e) => {
		e.preventDefault();
		if (newAnecdote === "") return;
		dispatch(createAnecdote(newAnecdote));
    dispatch(setNotification(`you created '${newAnecdote}'`))
    setTimeout(() => {
			dispatch(clearNotification());
		}, 5000);
		setNewAnecdote("");
	};

	return (
		<div>
			<h2>create new</h2>
			<form onSubmit={handleCreateAnecdote}>
				<div>
					<input
						value={newAnecdote}
						onChange={(e) => setNewAnecdote(e.target.value)}
					/>
				</div>
				<button type="submit">create</button>
			</form>
		</div>
	);
};

const sortAnecdote = (item1, item2) => {
	return item2.votes - item1.votes;
};

export const AnecdoteList = () => {
	const anecdotes = useSelector((state) => state.anecdotes);
	const filter = useSelector((state) => state.filter);
	const dispatch = useDispatch();

	const vote = (anecdote) => {
		console.log("vote", anecdote);
		dispatch(increaseVote(anecdote.id));
		dispatch(setNotification(`you voted '${anecdote.content}'`));
		setTimeout(() => {
			dispatch(clearNotification());
		}, 5000);
	};

	return (
		<div>
			{anecdotes
				.filter((anecdote) => anecdote.content.toLowerCase().includes(filter.toLowerCase()))
				.sort(sortAnecdote)
				.map((anecdote) => (
					<div key={anecdote.id}>
						<div>{anecdote.content}</div>
						<div>
							has {anecdote.votes}
							<button onClick={() => vote(anecdote)}>vote</button>
						</div>
					</div>
				))}
		</div>
	);
};

export const Filter = () => {
	const dispatch = useDispatch();
	const handleChange = (event) => {
		dispatch(setFilter(event.target.value));
	};
	const style = {
		marginBottom: 10,
	};

	return (
		<div style={style}>
			filter <input onChange={handleChange} />
		</div>
	);
};
