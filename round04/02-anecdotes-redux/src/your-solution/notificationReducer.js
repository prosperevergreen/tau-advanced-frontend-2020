const initialState = "";

const ACTION_TYPES = { notify: "SET_NOTIFICATION", clear: "CLEAR_NOTIFICATION" };

const reducer = (state = initialState, action) => {
	console.log("state now: ", state);
	console.log("action", action);

	switch (action.type) {
		case ACTION_TYPES.notify: {
			const msg = action.data;
			return msg;
		}
		case ACTION_TYPES.clear: {
			return "";
		}
		default:
			return state;
	}
};

export const setNotification = (msg) => {
	return {
		type: ACTION_TYPES.notify,
		data: msg,
	};
};

export const clearNotification = () => {
	return {
		type: ACTION_TYPES.clear,
	};
};

export default reducer;
