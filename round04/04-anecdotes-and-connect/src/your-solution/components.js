import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { createAnecdote, increaseVote, initAnecdotes } from "./anecdoteReducer";
import { setNotification } from "./notificationReducer";
import { setFilter } from "./filterReducer";

const Notification = ({ notification }) => {
	const style = {
		border: "solid",
		padding: 10,
		borderWidth: 1,
	};
	if (notification === "") return null;
	return <div style={style}>{notification}</div>;
};

const notificationMSTP = (state) => {
	return { notification: state.notification.msg };
};

export const ConnNotification = connect(notificationMSTP)(Notification);

const AnecdoteForm = ({ createAnecdote, setNotification }) => {
	const [newAnecdote, setNewAnecdote] = useState("");

	const handleCreateAnecdote = async (e) => {
		e.preventDefault();
		if (newAnecdote === "") return;
		createAnecdote(newAnecdote);
		setNotification(`you created '${newAnecdote}'`, 5);
		setNewAnecdote("");
	};

	return (
		<div>
			<h2>create new</h2>
			<form onSubmit={handleCreateAnecdote}>
				<div>
					<input
						value={newAnecdote}
						onChange={(e) => setNewAnecdote(e.target.value)}
					/>
				</div>
				<button type="submit">create</button>
			</form>
		</div>
	);
};

const anecdoteFormMDTP = {
	createAnecdote,
	setNotification,
};

export const ConnAnecdoteForm = connect(null, anecdoteFormMDTP)(AnecdoteForm);

const sortAnecdote = (item1, item2) => {
	return item2.votes - item1.votes;
};

const AnecdoteList = ({
	anecdotes,
	initAnecdotes,
	increaseVote,
	setNotification,
}) => {
	useEffect(() => {
		initAnecdotes();
	}, []);

	const vote = (anecdote) => {
		console.log("vote", anecdote);
		increaseVote(anecdote.id);
		setNotification(`you voted '${anecdote.content}'`, 5);
	};

	return (
		<div>
			{anecdotes.map((anecdote) => (
				<div key={anecdote.id}>
					<div>{anecdote.content}</div>
					<div>
						has {anecdote.votes}
						<button onClick={() => vote(anecdote)}>vote</button>
					</div>
				</div>
			))}
		</div>
	);
};

const anecdoteListMSTP = (state) => {
	return {
		anecdotes: state.anecdotes
			.filter((anecdote) => anecdote.content.toLowerCase().includes(state.filter.toLowerCase()))
			.sort(sortAnecdote),
	};
};

const anecdoteListMDTP = {
	initAnecdotes,
	increaseVote,
	setNotification,
};

export const ConnAnecdoteList = connect(
	anecdoteListMSTP,
	anecdoteListMDTP
)(AnecdoteList);

export const Filter = ({setFilter}) => {
	const handleChange = (event) => {
		setFilter(event.target.value);
	};
	const style = {
		marginBottom: 10,
	};

	return (
		<div style={style}>
			filter <input onChange={handleChange} />
		</div>
	);
};

const filterMDTP = {
	setFilter,
};

export const ConnFilter = connect(null, filterMDTP)(Filter);
