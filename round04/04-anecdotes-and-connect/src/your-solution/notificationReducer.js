const initialState = { msg: "", timeout: null };

const ACTION_TYPES = {
	notify: "SET_NOTIFICATION",
	clear: "CLEAR_NOTIFICATION",
};

const reducer = (state = initialState, action) => {
	console.log("state now: ", state);
	console.log("action", action);

	switch (action.type) {
		case ACTION_TYPES.notify: {
			const data = {msg : action.data.msg, timeout: action.data.timeout}
			return data;
		}
		case ACTION_TYPES.clear: {
			const timeout = state.timeout;
			timeout && clearTimeout(timeout)
			return {
				msg: "",
				timeout: null
			};
		}
		default:
			return state;
	}
};

export const setNotification = (msg, time) => {
	return (dispatch) => {
		dispatch(clearNotification())
		const timeout = setTimeout(() => dispatch(clearNotification()), time * 1000);
		dispatch({
			type: ACTION_TYPES.notify,
			data: {msg, timeout},
		});
	};
};

export const clearNotification = () => {
	return {
		type: ACTION_TYPES.clear,
	};
};

export default reducer;
