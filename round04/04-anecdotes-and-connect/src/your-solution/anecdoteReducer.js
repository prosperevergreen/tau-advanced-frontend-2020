import anecdoteServices from "./services";
const initialState = [];

const ACTION_TYPES = {
	vote: "INC_VOTE",
	create: "ADD_ANECDOTE",
	init: "INITIALIZE",
};

const reducer = (state = initialState, action) => {
	console.log("state now: ", state);
	console.log("action", action);

	switch (action.type) {
		case ACTION_TYPES.vote: {
			const id = action.data;
			return state.map((item) =>
				item.id === id ? { ...item, votes: item.votes + 1 } : item
			);
		}
		case ACTION_TYPES.create: {
			const anecdote = action.data;
			return state.concat(anecdote);
		}
		case ACTION_TYPES.init: {
			return action.data;
		}

		default:
			return state;
	}
};

export const increaseVote = (id) => {
	return {
		type: ACTION_TYPES.vote,
		data: id,
	};
};

export const createAnecdote = (anecdoteContent) => {
	return async (dispatch) => {
		const anecdote = await anecdoteServices.createNew(anecdoteContent);
		dispatch({
			type: ACTION_TYPES.create,
			data: anecdote,
		});
	};
};

export const initAnecdotes = () => {
	return async (dispatch) => {
		const anecdotes = await anecdoteServices.getAll();
		dispatch({
			type: ACTION_TYPES.init,
			data: anecdotes,
		});
	};
};

export default reducer;
