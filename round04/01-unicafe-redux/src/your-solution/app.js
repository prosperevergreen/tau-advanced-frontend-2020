import ReactDOM from "react-dom";
import { createStore } from "redux";
import reducer from "./reducer";

// ** enter commit sha of your repository in here **
export const commitSHA = "bc435b2";

export const store = createStore(reducer);

const ACTION_TYPE = { good: "GOOD", bad: "BAD", ok: "OK", reset: "ZERO" };

export const App = () => {

	const good = () => {
		store.dispatch({
			type: ACTION_TYPE.good,
		});
	};
	const ok = () => {
		store.dispatch({
			type: ACTION_TYPE.ok,
		});
	};
	const bad = () => {
		store.dispatch({
			type: ACTION_TYPE.bad,
		});
	};
	const reset = () => {
		store.dispatch({
			type: ACTION_TYPE.reset,
		});
	};

	return (
		<div>
			<button onClick={good}>good</button>
			<button onClick={ok}>neutral</button>
			<button onClick={bad}>bad</button>
			<button onClick={reset}>reset stats</button>
			<div>good {store.getState().good}</div>
			<div>neutral {store.getState().ok}</div>
			<div>bad {store.getState().bad}</div>
		</div>
	);
};
