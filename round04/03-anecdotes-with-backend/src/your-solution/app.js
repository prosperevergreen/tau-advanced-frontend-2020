import React from "react";
import { AnecdoteForm, AnecdoteList, Notification, Filter } from "./components";

// ** enter commit sha of your repository in here **
export const commitSHA = "f846ed2";
export { default as store } from "./store";

export const App = () => {
	return (
		<div>
			<h2>Anecdotes</h2>
			<Notification />
			<Filter />
			<AnecdoteList />
			<AnecdoteForm />
		</div>
	);
};
