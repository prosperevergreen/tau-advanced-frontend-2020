const initialState = "";

const ACTION_TYPES = { filter: "SET_FILTER"};

const reducer = (state = initialState, action) => {
	console.log("state now: ", state);
	console.log("action", action);

	switch (action.type) {
		case ACTION_TYPES.filter: {
			const term = action.data;
			return term;
		}
		default:
			return state;
	}
};

export const setFilter = (term) => {
	return {
		type: ACTION_TYPES.filter,
		data: term,
	};
};

export default reducer;
