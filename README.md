# COMP.CS.520: Advanced Web Front-ends (2021 spring) - Solution

This are my solution to the course  offered at Tampere university, [COMP.CS.520: Advanced Web Front-ends (2021 spring)](https://moodle.tuni.fi/course/view.php?id=10874). The course is a subset of the full course [Full Stack open 2021](https://fullstackopen.com/) offered by the University of Helsinki.