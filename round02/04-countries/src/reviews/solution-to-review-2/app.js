//import React, { Component } from 'react'
import React, { useState, useEffect } from 'react'
import Countries from './components.js'
import axios from 'axios'

// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE                        //
// ------------------------------------------------------------ //
export const commitSHA = '0f0ef4d';
// ------------------------------------------------------------ //
/*
  [X] step1a: "shows a list of countries"
  [X] step1b: "shows detailed country information"
  [X] step2: "show buttons / functions in the list"
*/

export const App = () => {
  const [countries, setCountries] = useState([])
  

  useEffect(() => { 
    axios
    .get('https://restcountries.eu/rest/v2/all')      
    .then(response => {              
       setCountries(response.data)   
      })  
    }, [])
  return (
    <div>
      <Countries country = {countries}/>
    </div>
  );
}

