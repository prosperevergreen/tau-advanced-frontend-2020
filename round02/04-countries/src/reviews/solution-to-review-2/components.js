import React, { useState } from 'react';

const Countries = (props) => {
  const [ searching, setSearching ] = useState(false);
  const [ filterValue, setFilterValue] = useState("");
  const [ show, toggleShow] = useState(true);
  const [ showCountry, countryShow] = useState("");


  // Search countries
  const handleSearch = event => {
    event.preventDefault();
    const active = event.target.value;
    if (active.trim().length > 0) {
      setSearching(true);
    } else {
      setSearching(false);
    }
    setFilterValue(active);
  };

  // List of coutries
  const GetList = (props) => {
    let currentCountry = null;   
    if (searching) {
      currentCountry = props.getlist.filter(country => {
        return(
          country.name.toLowerCase().includes(filterValue.toLowerCase())
          );
        });
      } 
    else {
      currentCountry = [...props.getlist];
    }
    if (currentCountry.length > 10){
      return (
      <div>
        <p>Too many matches, spesify another filter</p>
      </div>  
      )
    }
    else if (currentCountry.length > 2) {
      return currentCountry.map(country => (
      <li key={country.name}>
        {country.name}
        <button onClick={() => {toggleShow(!show); countryShow(country.name);}}>
             {/*show ? 'show' : 'hide'*/}
             show/hide
        </button>
          {!show && country.name ==showCountry && <OneCountry country = {country}/>}
    </li>
    ));
  }

  else if (currentCountry.length === 1){
    return(
      <OneCountry country = {currentCountry[0]}/>
    );
   
  }
  else{
    return(
      <p>Too many matches, spesify another filter</p>
      ) 
  };
};
return(
    <div>
       <h1>Countries</h1>
       <div>Find Countries <input onChange={handleSearch} value={filterValue}/> </div>
       <br></br>
       <div>
         <GetList getlist = {props.country}/>
       </div>
    </div> 
  )
};

 // Display one country data
const OneCountry = (props) =>{
    const currentCountry = props.country;
    const name = currentCountry.name;
    const capital = currentCountry.capital;
    const population = currentCountry.population;
    const languages = currentCountry.languages;
    const flag = currentCountry.flag;
    return(
      <div>
        <h2>{name}</h2>
        <p>
          Capital {capital} <br></br>
          Population {population}
        </p>
        <h4>Languages</h4>
        <ul>
          {languages.map(language => (
             <li key = {language.name}>
             {language.name}
             </li>))
          }
        </ul>
        <img src={flag} alt="flag" width="9%" height="9%" />
        <p></p>
      </div>
  ) 
}


export default Countries;

