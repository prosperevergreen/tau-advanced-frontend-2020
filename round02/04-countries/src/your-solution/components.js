import React from 'react'

const SearchArea = ({ searchTerm, setSearchTerm }) => (
	<div>
		find countries
		<input
			value={searchTerm}
			onChange={(ev) => setSearchTerm(ev.target.value)}
		/>
	</div>
);

const Country = ({ country, showCountry }) => (
	<>
		{country.name}{" "}
		<input type="button" value="show" onClick={() => showCountry(country)} />
		<br />
	</>
);

const CountryDetail = ({ country }) => (
	<div>
		<h2>{country.name}</h2>
		<p>
			capital {country.capital} <br />
			population {country.population}
		</p>
		<h2>languages</h2>
		<ul>
			{country.languages.map((language) => (
				<li>{language.name}</li>
			))}
		</ul>
		<img src={country.flag} alt={`${country.name} flag`} width="200px" />
	</div>
);

const Result = ({ countries, showCountry }) => (
	<div>
		{countries.length === 0 ? (
			"No match was found."
		) : countries.length === 1 ? (
			<CountryDetail country={countries[0]} />
		) : countries.length <= 10 ? (
			<CountryList countries={countries} showCountry={showCountry} />
		) : (
			"Too many matches, specify another filter"
		)}
	</div>
);

const CountryList = ({ countries, showCountry }) =>
	countries.map((country) => (
		<Country country={country} showCountry={showCountry} />
	));


export {SearchArea, Result}