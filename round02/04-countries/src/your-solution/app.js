import React, { useEffect, useState } from "react";
import axios from "axios";
import { SearchArea, Result } from "./components.js";

// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE                        //
// ------------------------------------------------------------ //
export const commitSHA = "5596092";
// ------------------------------------------------------------ //

export const App = () => {
	const [countries, setCountries] = useState([]);
	const [searchTerm, setSearchTerm] = useState("");

	// Fetch countries data on page load
	useEffect(() => {
		axios
			.get("https://restcountries.eu/rest/v2/all")
			.then((response) => response.data)
			.then((countries) => {
				setCountries(countries);
			})
			.catch((err) => setCountries([]));
	}, []);

	// Filter the coutry array with the search term
	const foundCountries = countries.filter((country) =>
		country.name.toLowerCase().includes(searchTerm.toLowerCase())
	);

	// Set search term to country name
	const showCountry = (country) => {
		setSearchTerm(country.name);
	};

	return (
		<div>
			<SearchArea searchTerm={searchTerm} setSearchTerm={setSearchTerm} />
			<Result countries={foundCountries} showCountry={showCountry} />
		</div>
	);
};
