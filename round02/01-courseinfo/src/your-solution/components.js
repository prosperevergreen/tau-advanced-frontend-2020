import React from 'react'

const Header = ({ name }) => <h1>{name}</h1>;

const Content = ({ parts }) =>
	parts.map((part) => <Part key={part.id} part={part} />);

const Part = ({ part }) => (
	<p>
		{part.name} {part.exercises}
	</p>
);

const Total = ({ parts }) => {
	const total = parts.reduce((acc, curr) => acc + curr.exercises, 0);
	return <p>Number of exercises {total}</p>;
};

const Course = ({ course }) => {
	return (
		<div>
			<Header name={course.name} />
			<Content parts={course.parts} />
			<Total parts={course.parts} />
		</div>
	);
};

export default Course