import React from "react";
import Course from './components.js'
import courses from './courses-data.js'
// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE                        //
// ------------------------------------------------------------ //
export const commitSHA = "64bb5c3";
// ------------------------------------------------------------ //

export const App = () => {
	return courses.map((course) => <Course course={course} key={course.id} />);
};
