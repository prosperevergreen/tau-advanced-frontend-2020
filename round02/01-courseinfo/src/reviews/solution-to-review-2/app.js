import React from 'react'
import Course from  './course'
import {coursesData} from './courses-data'

// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE                        //
// ------------------------------------------------------------ //
export const commitSHA = 'a0b1444';
// ------------------------------------------------------------ //

export const App = () => {
  const courses = coursesData
  const allData = courses.map((course) => {
    return <Course key={course.id} course={course} />
  })

  return (
    <div>
      <h1>Web development curriculum</h1>
      {allData}
    </div>
  )
}

