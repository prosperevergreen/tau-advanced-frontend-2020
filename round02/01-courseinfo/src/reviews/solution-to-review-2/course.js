import React from 'react'

const Course = ({ course }) => {
  return (
    <div>
      <Header name={course.name} />
      <Content parts={course.parts} />
      <Total parts={course.parts} />
    </div>
  )
}

const Header = ({ name }) => {
  return <h2>{name}</h2>
}

const Content = ({ parts }) => {
  return (
    <div>
      {parts.map(part => 
        <div key={part.id}>
          <Part part={part} />
        </div>
      )}
    </div>
  )
}

const Part = ({ part }) => {
  return <p>{part.name} {part.exercises}</p>
}

const Total = ({ parts }) => {
  const totalCourse = parts.reduce((s, p) => s + p.exercises, 0)
  return <div><b>total of {totalCourse} exercises</b></div>
}

export default Course