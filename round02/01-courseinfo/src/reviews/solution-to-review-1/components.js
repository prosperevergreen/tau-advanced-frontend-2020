const Header = props =>
  <h1>
    {props.course}
  </h1>


const Content = props => {
  return(
    props.parts.map(part => 
      <Part key={part.id} part={part} /> )
  )
}


const Part = props =>
  <p>
    {props.part.name} {props.part.exercises}
  </p>

const Total = props =>{
  const total = props.exercises.reduce((a,b)=>a+b.exercises, 0)
  return(
  <b>
    total of {total} exercises
  </b>
  )
}
const Course = props => {
  return(
    <div>
      <Header course={props.course.name} />
      <Content parts={props.course.parts} />
      <Total exercises={props.course.parts} />
    </div>
  )
}

export { Header, Content, Part, Total, Course }
