import { Course, Header } from './components.js'
import courses from './courses-data.js'
// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE                        //
// ------------------------------------------------------------ //
export const commitSHA = '6102938';
// ------------------------------------------------------------ //

export const App = () => {
  return (
    <div>
      <Header course="Web development curriculum" />
      <Course  course={courses[0]} />
      <Course  course={courses[1]} />
    </div>
  ) 
}
