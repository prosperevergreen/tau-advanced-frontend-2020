import React, { useState } from "react";
import initialPersons from "../initial-persons";
import {PersonForm, Filter, Persons} from "./components.js"
// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE                        //
// ------------------------------------------------------------ //
export const commitSHA = "4fe5e41";
// ------------------------------------------------------------ //


export const App = () => {
	const [persons, setPersons] = useState(initialPersons);
	const [newName, setNewName] = useState("");
	const [newNumber, setNewNumber] = useState("");
	const [filterName, setFilterName] = useState("");

	// Filter the contacts with the search string
	const displayPersons = persons.filter((person) =>
		person.name.toLowerCase().includes(filterName.toLowerCase())
	);

  // Adds new person or modifies existing person
	const addNewPerson = (ev) => {
		// Prevent page reload
		ev.preventDefault();

		// Check if the name is already added
		const index = persons.findIndex((person) => person.name === newName);

		// If the index is -1 then the bname doesnpot exist
		if (index === -1) {
			const newPerson = { name: newName, number: newNumber };
			// add new person to persons array
			setPersons(persons.concat(newPerson));
		} else {
			// If person name exist
			const newPersons = [...persons];
			// Update person number
			newPersons[index].number = newNumber;

			// Update persons array
			setPersons(newPersons);
		}

		// Clear the input
		setNewName("");
		setNewNumber("");
	};

	// Deletes a single person
	const deletePerson = (deleteName) => {
		// Filters all persons that is not the given person
		const newPersons = persons.filter((person) => person.name !== deleteName);

		// Updates the persons array
		setPersons(newPersons);
	};

  // Copys the person's details to input field
	const editPerson = (editPer) => {
		setNewName(editPer.name);
		setNewNumber(editPer.number);
	};

	return (
		<div>
			<h2>Phonebook</h2>
			<Filter filterName={filterName} setFilterName={setFilterName} />

			<h2>add a new</h2>
			<PersonForm
				addNewPerson={addNewPerson}
				newName={newName}
				newNumber={newNumber}
				setNewName={setNewName}
				setNewNumber={setNewNumber}
			/>
			<h2>Numbers</h2>
			<Persons
				persons={displayPersons}
				deletePerson={deletePerson}
				editPerson={editPerson}
			/>
		</div>
	);
};
