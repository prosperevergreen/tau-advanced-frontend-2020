
import React, { useState } from 'react';
import initialPersons from '../initial-persons';
import { Filter, PersonForm, Persons } from './components'

// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE                        //
// ------------------------------------------------------------ //
export const commitSHA = '5eafc6d';
// ------------------------------------------------------------ //


export const App = () => {
  const [ persons, setPersons] = useState(initialPersons) 
  const [ newName, setNewName ] = useState('')
  const [ newNumber, setNewNumber ] = useState('')
  const [ filter, setFilter ] = useState('')

  const addInformation = (event) => {
    event.preventDefault()
    
    if (persons.some(person => person.name === newName)) {
      const copy = persons.map(person => {
        if(person.name == newName) {
          return Object.assign({}, person, {number:newNumber})
        }
        return person
      });
      setPersons(copy)
    } else {
      const personObject = {
        name: newName,
        number: newNumber
      }
      setPersons(persons.concat(personObject))
    }
    setNewName('')
    setNewNumber('')
  }

  const handleFilter = (event) => {
    setFilter(event.target.value)
  }

  const handleNewName = (event) => {
    setNewName(event.target.value)
  }

  const handleNewNumber = (event) => {
    setNewNumber(event.target.value)
  }

  const handleRemove = (event) => {
    const name = event.target.parentNode.getAttribute('data-key')
    const newPersons = persons.filter(person => person.name !== name)
    setPersons(newPersons)
  }

  const personsToShow = () => {
    return persons.filter(person => person.name.toLowerCase().includes(filter.toLowerCase()))
  }

  return (
    <div>
      <h2>Phonebook</h2>
      <Filter value={filter} onChange={handleFilter} />
      <h3>Add a new</h3>
      <PersonForm onSubmit={addInformation} newName={newName} onChangeName={handleNewName} 
      newNumber={newNumber} onChangeNumber={handleNewNumber} />
      <h3>Numbers</h3>
      <Persons persons={personsToShow()} onClickRemove={handleRemove} />
    </div>
  );
}

