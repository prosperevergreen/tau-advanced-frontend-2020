export const Filter = ( {filter, onChange} ) => {
    return (
        <div>
            filter shown with<input value={filter} onChange={onChange} />
        </div>
    )
}
  
const RemoveButton = ( {onClick} ) => (
    <button onClick={onClick}>
        Remove
    </button>
)
  
export const PersonForm = (props) => {
    return (
        <form onSubmit={props.onSubmit}>
            <div>
                name: <input value={props.newName} onChange={props.onChangeName} required />
            </div>
            <div>
                number: <input value={props.newNumber} onChange={props.onChangeNumber} required />
            </div>
            <div>
                <button type="submit">add</button>
            </div>
        </form>
    )
}
  
const Person = ( {person, onClickRemove} ) => {
    return (
        <p data-key={person.name} >{person.name} {person.number} <RemoveButton onClick={onClickRemove} /></p>
    )
}
  
export const Persons = ( {persons, onClickRemove} ) => {
    return (
        <div>
            {persons.map(person =>
                <Person key={person.name} person={person} onClickRemove={onClickRemove} />
            )}
        </div>
    )
}