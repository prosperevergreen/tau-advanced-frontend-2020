import React, { useState, useEffect } from 'react'
import {Filter, PersonForm, Persons, Notification} from '../your-solution/components'
import personService from '../services/person-service'

// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE                        //
// ------------------------------------------------------------ //
export const commitSHA = '436270c0';
// ------------------------------------------------------------ //

export const App = () => {
  
    const [ persons, setPersons] = useState([]) 
    const [ newName, setNewName ] = useState('')
    const [ newNumber, setNewNumber ] = useState('')
    const [ newFilter, setNewFilter] = useState('')  
    const [ notification, setNotification ] =useState(null)
    const [ error, setError ] =useState(false)  
  
    useEffect (() => {
      console.log('efekti')
      personService
        .getAll()
        .then(initialPersons => {
            console.log('promise fulfilled')
            setPersons(initialPersons)
        })
    }, [])
    console.log('render', persons.length, 'persons')
    const addNumber = (event) => {    
      event.preventDefault()
      const person = {
        name: newName,
        number: newNumber
      }
      const found = persons.find(person => person.name.toLocaleLowerCase()===newName.toLocaleLowerCase())
  
      if (found) {
        console.log('nimi oli listassa')
        if (window.confirm(`${newName} is already added to phonebook, replace the old number with a new one?`)){          
          personService
          .update(found.id, person)
          .then(updatedPerson => {
            console.log('updated person: ', updatedPerson)
            // jos persoonan id on eri kuin löydetyn id, laita listaan vanha, muuten laita listaan updeitattu
            setPersons (persons.map(p => p.id!==found.id ? p : updatedPerson))
            setNewName('')
            setNewNumber('')
            setNotification(`Replaced ${found.name}'s old number ${found.number} with ${newNumber}`)
            setTimeout(() => {
            setNotification(null)
          }, 5000)
  
          })
          .catch(error => {
            console.log('error: ', error)          
            setNotification(`${newName}'s information not found on the server`)          
            setError(true)
            setTimeout(() => {
              setNotification(null)
              setError(false)
            }, 5000)
          })
  
        }
      } else {
        personService
        .create(person)
        .then(returnedPerson => {
          console.log(returnedPerson)
          setPersons(persons.concat(returnedPerson))
          setNewName('')
          setNewNumber('')
          console.log('nimi ei ollut listassa, lisätty')
          setNotification(`Added ${newName} to phonebook`)
          setTimeout(() => {
            setNotification(null)
          }, 5000)
        })
        .catch(error=> {
          setNotification(error.response.data.error)
          setError(true)
          setTimeout(() => {
            setNotification(null)
            setError(false)
          }, 5000)
        })
              
      }  
  
    }
    const deleteNumber = (person) => {
            
      if (window.confirm(`Delete ${person.name}?`)){
        personService
        .deletePerson(person.id)
        .then(deletedPerson => {
          console.log('deletoitava person: ',deletedPerson)
          console.log('deletedperson.id tyyppi', typeof deletedPerson.id)
          setPersons(persons.filter(p => p.id!==person.id))   
          setNotification(`Deleted ${person.name} from phonebook`)
          setTimeout(() => {
            setNotification(null)
          }, 5000)     
        })
        .catch(error => {
          console.log(error)
          setNotification(`${person.name}'s information could not be deleted, try again or refresh the page`)
          setError(true)
          setTimeout(() => {
            setNotification(null)
            setError(false)
          }, 5000)
      })}
    }    

    const handleNameChange = (event) => {
      console.log(event.target.value)
      setNewName(event.target.value)
    }
  
    const handleNumberChange = (event) => {
      console.log(event.target.value)
      setNewNumber(event.target.value)
    }
  
    const handleFilterChange = (event) => {
      console.log(event.target.value)
      setNewFilter(event.target.value)
    }
    
    return (
      <div>
        <h1>Phonebook</h1>
        <Notification 
        message={notification}
        error={error}>          
        </Notification>
          <Filter
          value = {newFilter}
          onChange = {handleFilterChange}>
          </Filter>            
        <h3>Add new person:</h3>
          <PersonForm 
          onSubmit={addNumber}
          name={newName}
          onNameChange={handleNameChange}
          number= {newNumber}
          onNumberChange={handleNumberChange}
          >
          </PersonForm>
        <h3>Contacts in phonebook:</h3>
          <Persons
          persons = {newFilter === ''
          ? persons
          : persons.filter(person => person.name.toLocaleLowerCase().includes(newFilter.toLocaleLowerCase()))}
          onClick={deleteNumber}>
          </Persons>        
      </div>
    )       
}

