import React from 'react'

const Notification = ({ message, error }) => {

  if (message === null) {
    return null
  }

  return (
    <div
    className = {error ?  'error': 'success'}>
      {message}
    </div>
  )
}
const Person = ({person, onClick}) => {

    const personDataToApp = (person) => {
        //define person data to send to App component
        onClick(person)
    } 

    return (        
          < div key={person.name}>
              {person.name} {person.number}   
              <Button
              onClick={()=> {
                  console.log(person)
                  personDataToApp(person)
              }}>                
                </Button>           
            </div>        
    )

}

const Persons = ({persons, onClick}) => {
    return (
        <div>
          {persons.map((person =>
            < Person 
            key={person.name}
            person={person}
            onClick={onClick}> 
                                                
            </Person>)
            )}
        </div>
    )
}        
const PersonForm = ({onSubmit, name, onNameChange, number, onNumberChange}) => {

    return (
    <form onSubmit={onSubmit}>
        <div>
          name: <input 
          value={name}
          onChange={onNameChange}/>
        </div>
        <div>
          number: <input
          value= {number}
          onChange={onNumberChange}
          />
        </div>
        <div>
          <button type="submit">add</button>
        </div>
      </form>
    )
}
const Filter = ({value, onChange}) => {

    return (
    <form>
        <div>
          Filter shown persons with <input
          value = {value}
          onChange = {onChange} />
        </div>
    </form>
    )

}
const Button = ({onClick}) => {
       
    return(
    <div>
         <button
         onClick={onClick}
         >
         delete
         </button>
     </div>
     )
 }


export {Filter, PersonForm, Persons, Notification}
  
