import React, { useState, useEffect } from 'react'
import { PersonList, PersonForm, PersonFilter, Notification } from './components'
import PersonService from './person-service'
// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE                        //
// ------------------------------------------------------------ //
export const commitSHA = 'dcc297a';
// ------------------------------------------------------------ //

export const App = () => {

  const [persons, setPersons] = useState([])
  const [newName, setNewName] = useState('')
  const [newNumber, setNewNumber] = useState('')
  const [searchTerm, setSearchTerm] = useState('')
  const [message, setMessage] = useState(null)

  const getPersons = () => {
    PersonService
      .getAll()
      .then(initialPersons => {
        setPersons(initialPersons)
      })
  }

  useEffect(getPersons, [])

  const handleSubmit = (event) => {
    event.preventDefault()
    if (checkIfNameExists(newName) && newNumber.length > 0) {
      updateNumber()
    } else if (checkIfNameExists(newName)) {
      alert(`${newName} is already added to phonebook`)
    }
    else {
      addPerson(persons)
    }
  }

  const addPerson = (persons) => {
    const personObject = {
      name: newName,
      number: newNumber,
    }

    PersonService.create(personObject)
      .then(returnedPerson => {
        setPersons(persons.concat(returnedPerson))
        setNewName('')
        setNewNumber('')
        formatMessage(`Person ${returnedPerson.name} was created`, 'success')
      })
  }

  const updateNumber = () => {
    const personToUpdate = persons.find(p => p.name === newName)
    if (window.confirm(`${personToUpdate.name} is already added to phonebook, replace the old number with the new one?`)) {
      const changedPerson = { ...personToUpdate, number: newNumber }

      PersonService.update(personToUpdate.id, changedPerson)
        .then(changedPerson => {
          setPersons(persons.map(p => p.id !== personToUpdate.id ? p : changedPerson))
          setNewName('')
          setNewNumber('')
          formatMessage(`Person ${changedPerson.name} was updated!`, 'success')
        }).catch(error => {
          formatMessage(`Cannot update person ${personToUpdate.name}, person is already removed from server`, 'error')
          setPersons(persons.filter(p => p.id !== personToUpdate.id))
        })
    }
  }

  const handleNameChange = (event) => {
    setNewName(event.target.value)
  }

  const handleNumberChange = (event) => {
    setNewNumber(event.target.value)
  }

  const handleSearchTermChange = (event) => {
    setSearchTerm(event.target.value)
  }

  const handleDelete = (personToDelete) => {
    if (window.confirm(`Do you really want to delete the person ${personToDelete.name}?`)) {
      PersonService.destroy(personToDelete.id)
        .then(response => {
          const filteredPersons = persons.filter(person => person.id !== personToDelete.id)
          setPersons(filteredPersons)
          formatMessage(`Person ${personToDelete.name} was deleted!`, 'success')

        }).catch(error => {
          formatMessage(`Person ${personToDelete.name} was already removed from server`, 'error')
          setPersons(persons.filter(p => p.id !== personToDelete.id))
        })
    }
  }

  const personsToShow = searchTerm.length > 0
    ? persons.filter(person => person.name.toLowerCase().includes(searchTerm.toLowerCase()))
    : persons

  const checkIfNameExists = (name) => {
    if (persons.map(person => person.name).includes(name)) {
      return true
    }
    return false
  }

  const formatMessage = (message, type) => {
    setMessage({ message, type })
    setTimeout(() => {
      setMessage()
    }, 5000)
  }

  return (
    <div>
      <h2>Phonebook</h2>
      <Notification {...message} />
      <PersonFilter handleSearchTermChange={handleSearchTermChange} />
      <PersonForm
        handleSubmit={handleSubmit}
        newName={newName}
        handleNameChange={handleNameChange}
        newNumber={newNumber}
        handleNumberChange={handleNumberChange}
      />

      <PersonList persons={personsToShow} handleDelete={handleDelete} />
    </div>
  )
}

