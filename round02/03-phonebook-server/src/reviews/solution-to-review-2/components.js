const Person = props => {
    const { person, handleDelete } = props
    return (
        <p>{person.name} {person.number}<button onClick={() => handleDelete(person)}>delete</button>
        </p>
    )
}

export const PersonList = props => {
    const { persons, handleDelete } = props
    return (
        <>
            <h2>Numbers</h2>
            {persons.map((person, index) =>
                <Person person={person}
                    key={person.name}
                    handleDelete={handleDelete}
                />
            )}
        </>
    )
}

export const PersonForm = props => {
    const { handleSubmit, newName, handleNameChange, newNumber, handleNumberChange } = props
    return (
        <>
            <h3>Add new</h3>
            <form onSubmit={handleSubmit}>
                <div>
                    name: <input
                        value={newName}
                        onChange={handleNameChange}
                    />
                </div>
                <div>
                    number: <input
                        value={newNumber}
                        onChange={handleNumberChange}
                    />
                </div>
                <div>
                    <button type="submit">add</button>
                </div>
            </form>
        </>
    )

}

export const PersonFilter = props => {
    const { handleSearchTermChange } = props
    return (
        <div>
            filter shown with <input onChange={handleSearchTermChange} />
        </div>
    )
}

export const Notification = ({ message, type }) => {
    let color = 'red'
    if (type === 'success') {
        color = 'green'
    }
    const NotificationStyle = {
        color: color,
        fontWeight: 'bold',
        border: '2px solid',
        padding: '20px',
        textAlign: 'center',
        margin: '30px auto'

    }
    if (!message && !type) {
        return null
    }

    return (
        <div style={NotificationStyle}>
            {message}
        </div>
    )
}