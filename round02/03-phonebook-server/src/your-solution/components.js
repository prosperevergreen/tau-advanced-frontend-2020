import React from 'react'

const Person = ({ person, deletePerson, editPerson }) => (
	<li>
		{person.name} - {person.number}{" "}
		<input onClick={() => editPerson(person.id)} type="button" value="Edit" />
		<input
			onClick={() => deletePerson(person.id)}
			type="button"
			value="Delete"
		/>
	</li>
);

const Persons = ({ persons, deletePerson, editPerson }) => (
	<ul>
		{persons.map((person) => (
			<Person
				key={person.name}
				person={person}
				deletePerson={deletePerson}
				editPerson={editPerson}
			/>
		))}
	</ul>
);

const Input = ({ label, newValue, setNewValue }) => (
	<div>
		{`${label}:  `}
		<input
			required
			value={newValue}
			onChange={(ev) => setNewValue(ev.target.value)}
		/>
	</div>
);

const PersonForm = ({
	addNewPerson,
	newName,
	newNumber,
	setNewName,
	setNewNumber,
}) => (
	<form onSubmit={addNewPerson}>
		<Input label="name" newValue={newName} setNewValue={setNewName} />
		<Input label="number" newValue={newNumber} setNewValue={setNewNumber} />
		<div>
			<button type="submit">add</button>
		</div>
	</form>
);

const Filter = ({ filterName, setFilterName }) => (
	<Input
		label="filter shown with"
		newValue={filterName}
		setNewValue={setFilterName}
	/>
);

const Notification = ({ notification }) => {
	if (notification.message === "") {
	  return null
	}
  
	return (
	  <div className={notification.type}>
		{notification.message}
	  </div>
	)
}

export {PersonForm, Filter, Persons, Notification}