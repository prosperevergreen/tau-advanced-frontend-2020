import React, { useState, useEffect } from "react";
import personService from "./person-service.js";
import { PersonForm, Filter, Persons, Notification } from "./components.js";
import "./style.css";
// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE                        //
// ------------------------------------------------------------ //
export const commitSHA = "ecf6ce9";
// ------------------------------------------------------------ //

export const App = () => {
	const [persons, setPersons] = useState([]);
	const [newName, setNewName] = useState("");
	const [newNumber, setNewNumber] = useState("");
	const [filterName, setFilterName] = useState("");
	const [notification, setNotification] = useState({
		message: "",
		type: "success",
	});

	useEffect(() => {
		getAllPersons();
	}, []);

	// Filter the contacts with the search string
	const displayPersons = persons.filter((person) =>
		person.name.toLowerCase().includes(filterName.toLowerCase())
	);

	// Adds new person or modifies existing person
	const addNewPerson = (ev) => {
		// Prevent page reload
		ev.preventDefault();
		let newPerson = {};
		// Check if the name is already added
		const index = persons.findIndex((person) => person.name === newName);
		// If the index is -1 then the bname doesnpot exist
		if (index === -1) {
			newPerson = { name: newName, number: newNumber };

			// add new person to persons array
			personService
				.create(newPerson)
				.then((newPerson) => {
					setPersons(persons.concat(newPerson));

					showNotification(`Added ${newPerson.name}`, "success");

					// Clear the input
					setNewName("");
					setNewNumber("");
				})
				.catch((err) => {
					// Notify of error
					showNotification(
						`Adding ${newPerson.name} was not successful`,
						"error"
					);
				});
		} else {
			// If person name exist
			const newPersons = [...persons];

			// Get the person to be modified
			newPerson = newPersons[index];
			if (
				window.confirm(
					`${newPerson.name} is already added to phonebook, replace the old number with a new one`
				)
			) {
				// update person
				newPerson.number = newNumber;

				personService
					.update(newPerson.id, newPerson)
					.then((newPerson) => {
						// Update person number
						newPersons[index] = newPerson;

						// Update persons array
						setPersons(newPersons);

						// Notify of successful completion
						showNotification(`Updated ${newPerson.name}`, "success");

						// Clear the input
						setNewName("");
						setNewNumber("");
					})
					.catch((err) =>
						// Notify of error
						showNotification(
							`Updating ${newPerson.name} was not successful`,
							"error"
						)
					);
			}
		}
	};

	const showNotification = (message, type) => {
		setNotification({ message, type });
		setTimeout(() => {
			setNotification({ ...notification, message: "" });
		}, 5000);
	};
	// Deletes a single person
	const deletePerson = (deleteId) => {
		const personToDelete = persons.find((person) => person.id === deleteId);

		if (window.confirm(`Delete ${personToDelete.name} ?`)) {
			personService
				.deletePerson(deleteId)
				.then(() => {
					showNotification(`Deleted ${personToDelete.name}`, "success");

					// Filters all persons that is not the given person
					const newPersons = persons.filter((person) => person.id !== deleteId);

					// Updates the persons array
					setPersons(newPersons);
				})
				.catch(() => {
					showNotification(
						`Information of ${personToDelete.name} has already been removed from server`,
						"error"
					);

					getAllPersons();
				});
		}
	};

	const getAllPersons = () => {
		personService
			.getAll()
			.then((persons) => setPersons(persons))
			.catch((err) => console.log(err));
	};

	// Copys the person's details to input field
	const editPerson = (editId) => {
		const personToEdit = persons.find((person) => person.id === editId);
		setNewName(personToEdit.name);
		setNewNumber(personToEdit.number);
	};

	return (
		<div>
			<h2>Phonebook</h2>

			<Notification notification={notification} />

			<Filter filterName={filterName} setFilterName={setFilterName} />

			<h2>add a new</h2>
			<PersonForm
				addNewPerson={addNewPerson}
				newName={newName}
				newNumber={newNumber}
				setNewName={setNewName}
				setNewNumber={setNewNumber}
			/>
			<h2>Numbers</h2>
			<Persons
				persons={displayPersons}
				deletePerson={deletePerson}
				editPerson={editPerson}
			/>
		</div>
	);
};
