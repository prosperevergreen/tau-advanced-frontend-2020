import React, { useState } from "react";

import Authors from "./component-authors";
import Books from "./component-books";
import NewBook from "./component-new-book";

// ** enter commit sha of your repository in here **
export const commitSHA = "32b2605";

export const App = () => {
	const [page, setPage] = useState("authors");

	const renderPage = () => {
		switch (page) {
			case "authors": {
				return <Authors />
			}
			case "books": {
				return <Books />
			}
			case "add": {
				return <NewBook />
			}
			default: {
				return <h1>Bad Page</h1>
			}
		}
	};

	return (
		<div>
			<div>
				<button onClick={() => setPage("authors")}>authors</button>
				<button onClick={() => setPage("books")}>books</button>
				<button onClick={() => setPage("add")}>add book</button>
			</div>
			{renderPage()}
		</div>
	);
};
