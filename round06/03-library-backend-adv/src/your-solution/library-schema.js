
const { gql } = require('apollo-server')


const typeDefs = gql`
  type Book {
    title: String!
    published: String!
    author: String!
    genres: [String!]!
    id: ID!
  }

  type Author {
    name: String!
    bookCount: Int!
    born: Int
  }

  type User {
    username: String!
    favoriteGenre: String!
    id: ID!
  }
  
  type Token {
    value: String!
  }

  type Query {
    bookCount: Int!

    authorCount: Int!

    allBooks(
      author: String
      genre: String
      ): [Book!]!

    allAuthors: [Author!]!

    me: User

    allUsers: [User!]!
  }

  type Mutation {
    addBook(
      title: String!
      author: String!
      published: Int!
      genres: [String!]!
      ): Book!
    
    editAuthor(
      name: String!
      setBornTo: Int!
    ): Author

    createUser(
      username: String!
      favoriteGenre: String!
    ): User

    login(
      username: String!
      password: String!
    ): Token
  }

  type Subscription {
    bookAdded: Book!
  }
`


module.exports = { typeDefs }

