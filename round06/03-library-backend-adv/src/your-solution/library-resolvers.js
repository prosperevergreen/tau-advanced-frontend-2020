// == DO NOT CHANGE THESE THREE LINES
const {
	UserInputError,
	AuthenticationError,
	PubSub,
} = require("apollo-server");
const { v1: uuid } = require("uuid");
const { basename } = require("path");
const dataPath = basename(__dirname) === "your-solution" ? "../.." : "../../..";
let { authors, books, users } = require(`${dataPath}/library-data`);
const {
	validateUser,
	validateGenre,
	validateLogin,
	getToken,
	getCurrentUser,
} = require("./utils");
// ==

// == ENTER COMMIT SHA OF YOUR REPO IN HERE
const commitSHA = "035ea0e";
const pubsub = new PubSub();
const TOPIC_BOOK_ADDED = "BOOK_ADDED";
const resolvers = {
	Query: {
		// Return all the books
		bookCount: () => books.length,
		// Return the number of authors available
		authorCount: () => authors.length,
		// Filter the books either by the author or genre or both
		allBooks: (_root, { author, genre }) =>
			books
				.filter((bk) => (author ? author === bk.author : true))
				.filter((bk) => (genre ? bk.genres.includes(genre) : true)),
		// Return all authors
		allAuthors: () => authors,
		// Return loged in user data
		me: (_root, _args, { token }) => {
			return getCurrentUser(token, users);
		},
		// Return all users
		allUsers: () => users,
	},
	Author: {
		// return the number of books by a given author
		bookCount: (root) => books.filter((bk) => bk.author === root.name).length,
	},
	Mutation: {
		// Add a book to the existing book
		addBook: (_root, book, { token }) => {
			// Authenticate the user
			if (!getCurrentUser(token, users)) {
				throw new AuthenticationError("Invalid User");
			}
			// Validate the genre provided by the user
			book.genres.map((genre) => {
				if (!validateGenre(genre)) {
					throw new UserInputError(
						"Genre must be string and more than 3 characters",
						{
							invalidArgs: genre,
						}
					);
				}
			});

			// Validate the title of the book
			if (!validateGenre(book.title)) {
				throw new UserInputError(
					"Invalid title: Title must be more than 3 characters"
				);
			}

			// Assign the book an Id
			const newBook = { ...book, id: uuid() };
			// Add the book to the existing book
			books = books.concat(newBook);

			// Check that the author exists else create a new author with the given name
			if (!authors.find((author) => author.name === book.author)) {
				authors = authors.concat({ name: book.author, id: uuid() });
			}

			// Publish the book for any subscriber
			pubsub.publish(TOPIC_BOOK_ADDED, { bookAdded: newBook });

			// Return the newly create dbook
			return newBook;
		},
		// Set the birth year of an author
		editAuthor: (_root, { name, setBornTo }, { token }) => {
			// Authenticate the user
			if (!getCurrentUser(token, users)) {
				throw new AuthenticationError("Invalid User");
			}

			// Search for the author
			const editAuthor = authors.find((author) => author.name === name);

			// if author doesn't exist, return null
			if (!editAuthor) return null;

			// set the birth year of the author
			editAuthor.born = setBornTo;

			// Update the author details
			authors = authors.map((author) =>
				author.id === editAuthor.id ? editAuthor : author
			);

			// Return the newly created author
			return editAuthor;
		},
		// Create a new user
		createUser: (_root, args) => {
			// Set user id
			const newUser = { ...args, id: uuid() };

			// Validate the username
			if (!validateUser(args.username, users))
				throw new UserInputError("Username is invlaid");

			// Validate the user favorite genre
			if (!validateGenre(args.favoriteGenre))
				throw new UserInputError("favorite genre is invlaid");

			// Add user to users list
			users = users.concat(newUser);

			// return nerwly created user
			return newUser;
		},
		// Log user in and return token
		login: (_root, { username, password }) => {
			// Validate user credentials
			if (!validateLogin(username, password, users)) {
				throw new UserInputError("Invalid Credential");
			}

			// Get user details
			const user = users.find((user) => user.username === username);

			// Generate token
			return { value: getToken(user) };
		},
	},
	Subscription: {
		// Create a pubsub for added book
		bookAdded: {
			subscribe: () => pubsub.asyncIterator(TOPIC_BOOK_ADDED),
		},
	},
};

// == DO NOT CHANGE THIS LINE
module.exports = { resolvers, commitSHA };
// ==
