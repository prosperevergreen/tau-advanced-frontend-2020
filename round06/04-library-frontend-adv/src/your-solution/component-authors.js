import React, { useState } from "react";
import { useQuery, useMutation } from "@apollo/client";
import { ALL_AUTHORS, EDIT_AUTHOR } from "./gql";

const Authors = ({setNotify}) => {
	const [name, setName] = useState("");
	const [born, setBorn] = useState("");

	const { loading, data } = useQuery(ALL_AUTHORS,{
		onError: (error)=>{
			setNotify(error.graphQLErrors[0].message)
		}
	});
	
	const [editAuthor] = useMutation(EDIT_AUTHOR, {
		refetchQueries: [{ query: ALL_AUTHORS }],
		onError: (error)=>{
			setNotify(error.graphQLErrors[0].message)
		}
	});
	if (loading) {
		return <h2>Loading...</h2>;
	}

	const handleSubmit = (e) => {
		e.preventDefault();
		editAuthor({
			variables: { name, born: Number(born) },
		});

		setName("");
		setBorn("");
	};

	const authors = data.allAuthors;

	return (
		<div>
			<h2>authors</h2>
			<table>
				<tbody>
					<tr>
						<th></th>
						<th>born</th>
						<th>books</th>
					</tr>
					{authors.map((a) => (
						<tr key={a.name}>
							<td>{a.name}</td>
							<td>{a.born}</td>
							<td>{a.bookCount}</td>
						</tr>
					))}
				</tbody>
			</table>
			<h2>Set birthyear</h2>
			<form onSubmit={handleSubmit}>
				<div>name{" "}
					<select name="author-name" value={name} onChange={({ target }) => setName(target.value)}>
						{authors.map((a) => (
							<option key={a.name} value={a.name}>
								{a.name}
							</option>
						))}
					</select>
				</div>
				<div>
					born{" "}
					<input
						type="number"
						value={born}
						onChange={({ target }) => setBorn(target.value)}
					/>
				</div>
				<button type="submit">update author</button>
			</form>
		</div>
	);
};

export default Authors;
