
import { gql } from '@apollo/client'

const BOOK_DETAIL = gql`
fragment BookDetail on Book {
    title
    author
    published
    genres
  }
`

export const ALL_AUTHORS = gql`
query {
    allAuthors{
        name
        born
        bookCount
    }
}
`

export const ALL_BOOKS = gql`
query {
    allBooks{
        ...BookDetail
    }
}${BOOK_DETAIL}
`

export const GENRE_BOOKS = gql`
query getBooksbyGenre($genre: String!){
    allBooks(genre: $genre){
        ...BookDetail
    }
}${BOOK_DETAIL}
`

export const ADD_BOOK = gql`
mutation addNewBook($title: String!, $author: String!, $published: Int!, $genres: [String!]!){
    addBook(title: $title, author: $author, published: $published, genres: $genres){
        title
    }
}
`

export const EDIT_AUTHOR = gql`
mutation updateAuthorBirthYear($name: String!, $born: Int!){
    editAuthor(name: $name, setBornTo: $born){
        name
        born
    }
}
`

export const LOGIN = gql`
mutation loginUser($username: String!, $password: String!){
    login( username: $username, password: $password){
        value
    }
}
`

export const ME = gql`
query {
    me{
        username
        favoriteGenre
        id
    }
}
`

export const BOOK_ADDED = gql`
subscription {
    bookAdded{
        ...BookDetail
    }
}${BOOK_DETAIL}
`