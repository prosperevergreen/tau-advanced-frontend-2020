import React, { useState } from "react";

import Authors from "./component-authors";
import Books from "./component-books";
import NewBook from "./component-new-book";
import Login from "./component-login-form";
import Notify from "./component-notify";
import { useApolloClient, useSubscription } from "@apollo/client";
import { BOOK_ADDED } from "./gql";

// ** enter commit sha of your repository in here **
export const commitSHA = "32b2605";

export const App = () => {
	// Track page
	const [page, setPage] = useState("login");
	// Save logged in user details
	const [user, setUser] = useState(null);
	// Save error message
	const [errorMsg, setErrorMsg] = useState(null);
	const client = useApolloClient();

	// Subscribe to add new note
	useSubscription(BOOK_ADDED, {
		onSubscriptionData: ({ subscriptionData }) => {
			const newBook = subscriptionData.data.bookAdded;
			// Set alert notification when a new not is created
			alert(`'${newBook.title}' by '${newBook.author}' was added`);
		},
	});

	// Display components based on page value
	const displayPage = () => {
		switch (page) {
			case "authors": {
				return <Authors setNotify={setNotify} />;
			}
			case "recommend":
			case "books": {
				return <Books page={page} recommended={user.favoriteGenre} />;
			}
			case "add": {
				return <NewBook setNotify={setNotify} />;
			}
			default: {
				return (
					<Login setUser={setUser} setNotify={setNotify} setPage={setPage} />
				);
			}
		}
	};

	// handle logout action
	const handleLogout = () => {
		// Clear users data
		setUser(null);
		// Go to login page
		setPage("login");
		// Clear persistent data
		localStorage.clear();
		// Reset cached data
		client.resetStore();
	};

	// Set notification
	const setNotify = (msg) => {
		setErrorMsg(msg);
		// Clear notification after 5 secs
		setTimeout(() => {
			setErrorMsg(null);
		}, 5000);
	};

	return (
		<div>
			<div>
				<button onClick={() => setPage("authors")}>authors</button>
				<button onClick={() => setPage("books")}>books</button>
				{/* Optionally show ADD button when user is logged in (user data is available) */}
				{user && <button onClick={() => setPage("add")}>add book</button>}
				{/* Optionally show RECOMMEND button when user is logged in (user data is available) */}
				{user && (
					<button onClick={() => setPage("recommend")}>recommend</button>
				)}
				{/* Optionally show LOGIN button when user is not logged in (user data is not available) */}
				{!user && <button onClick={() => setPage("login")}>login</button>}
				{/* Optionally show LOGOUT button when user is logged in (user data is available) */}
				{user && <button onClick={handleLogout}>logout</button>}
			</div>
			<Notify errorMessage={errorMsg} />
			{displayPage()}
		</div>
	);
};
