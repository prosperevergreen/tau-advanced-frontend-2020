
import React, { useState, useEffect } from 'react'
import { useMutation, useLazyQuery } from '@apollo/client'
import { LOGIN, ME } from './gql'

const LoginForm = ({setUser, setNotify, setPage}) => {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')

  const [ login, loginRes ] = useMutation(LOGIN, {
    onError: (error) => {
      setNotify(error.graphQLErrors[0].message)
    }
  })

  const [getUser, userRes] = useLazyQuery(ME,{
		onError: (error)=>{
			setNotify(error.graphQLErrors[0].message)
		}})

    // Check if user is logged in
    useEffect(()=>{
      const token = localStorage.getItem('library-user-token')
      if(token){
        getUser()
      }
    },[getUser])

    // Run if login data is recieved
  useEffect(() => {
    if ( loginRes.data ) {
      const token = loginRes.data.login.value
      localStorage.setItem('library-user-token', token)
      getUser()
    }
  }, [loginRes.data, getUser])

  // Run if user data is recieved
  useEffect(()=>{
    if(userRes.data){
      setUser(userRes.data.me)
      setPage("authors")
    }
  }, [userRes, setPage, setUser])




  const submit = async (event) => {
    event.preventDefault()
    login({ variables: { username, password } })
    setUsername('')
    setPassword('')     
  }



  return (
    <div>

      <h2>login</h2>

      <form onSubmit={submit}>
        <div>
          username <input
            value={username}
            onChange={({ target }) => setUsername(target.value)}
            required
          />
        </div>
        <div>
          password <input
            type='password'
            value={password}
            onChange={({ target }) => setPassword(target.value)}
            required
          />
        </div>
        <button type='submit'>login</button>
      </form>
    </div>
  )
}

export default LoginForm
