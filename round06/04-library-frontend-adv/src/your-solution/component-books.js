import React, { useState, useEffect } from "react";
import { useQuery } from "@apollo/client";
import { ALL_BOOKS } from "./gql";

const Books = ({ page, recommended, setNotify }) => {

	const { loading, data } = useQuery(ALL_BOOKS, {
		onError: (error) => {
			setNotify(error.graphQLErrors[0].message);
		},
	});
	const [genres, setGenres] = useState([]);
	const [books, setBooks] = useState([]);
	const [genre, setGenre] = useState("");

	// Check that the page is books
	const isBooksPage = page === "books";

	// Run every time page changes (books or recommend)
	useEffect(() => {
		// Set the filter based on the page
		if (recommended && !isBooksPage) setGenre(recommended);
		else {
			setGenre("");
		}
	}, [recommended, isBooksPage]);

	// Run if books data is available
	useEffect(() => {
		if (data) {
			setBooks(data.allBooks);
			let allGenres = [];
			// Loop through the books, check if they were already added to the list if not add else skip
			books.map((bk) =>
				// Loop through the gengers
				bk.genres.map(
					(genre) =>
						// Check if genre is already added in the list
						allGenres.includes(genre) ||
						// If not add it otherwise skip
						allGenres.push(genre)
				)
			);
			// Set the list of all unique genres
			setGenres(allGenres);
		}
	}, [data, books]);

	// Show loading if data is no present
	if (loading) {
		return <h2>Loading...</h2>;
	}

	// Get the book data
	const filteredBooks = books.filter((bk) =>
		genre === "" ? true : bk.genres.includes(genre)
	);

	return (
		<div>
			<h2>{isBooksPage ? "books" : "recommendation"}</h2>

			<table>
				<tbody>
					<tr>
						<th></th>
						<th>author</th>
						<th>published</th>
					</tr>
					{filteredBooks.map((bk) => (
						<tr key={bk.title}>
							<td>{bk.title}</td>
							<td>{bk.author}</td>
							<td>{bk.published}</td>
						</tr>
					))}
				</tbody>
			</table>
			{isBooksPage && (
				<div>
					{genres.map((genre) => (
						<button key={genre} onClick={() => setGenre(genre)}>
							{genre}
						</button>
					))}
					<button onClick={() => setGenre("")}>all genres</button>
				</div>
			)}
		</div>
	);
};

export default Books;
