// == DO NOT CHANGE THESE THREE LINES
const { basename } = require("path");
const dataPath = basename(__dirname) === "your-solution" ? "../.." : "../../..";
let { authors, books } = require(`${dataPath}/library-data`);
// ==

// == ENTER COMMIT SHA OF YOUR REPO IN HERE
const commitSHA = "035ea0e";
const { v1: uuid } = require("uuid");
const resolvers = {
	Query: {
		bookCount: () => books.length,
		authorCount: () => authors.length,
		allBooks: (_root, { author, genre }) =>
			books
				.filter((bk) => (author ? author === bk.author : true))
				.filter((bk) => (genre ? bk.genres.includes(genre) : true)),
		allAuthors: () => authors,
	},
	Author: {
		bookCount: (root) => books.filter((bk) => bk.author === root.name).length,
	},
	Mutation: {
		addBook: (_root, book) => {
			const newBook = { ...book, id: uuid() };
			books = books.concat(newBook);
			if (!authors.find((author) => author.name === book.author)) {
				authors = authors.concat({ name: book.author, id: uuid() });
			}
			return newBook;
		},
		editAuthor: (_root, { name, setBornTo }) => {
			const editAuthor = authors.find((author) => author.name === name);
			if (!editAuthor) return null;
			editAuthor.born = setBornTo;
			authors = authors.map((author) =>
				author.id === editAuthor.id ? editAuthor : author
			);
			return editAuthor;
		},
	},
};

// == DO NOT CHANGE THIS LINE
module.exports = { resolvers, commitSHA };
// ==
