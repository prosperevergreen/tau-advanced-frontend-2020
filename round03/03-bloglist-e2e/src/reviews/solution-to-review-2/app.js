import React, { useState, useEffect, useRef } from 'react'
import { Blog, LoginForm, CreatePostForm, Notification, Togglable } from './components'
import blogService from './services'

// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE                        //
// ------------------------------------------------------------ //
export const commitSHA = 'a1d42eb';
// ------------------------------------------------------------ //


export const App = () => {
  const [blogs, setBlogs] = useState([])
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [user, setUser] = useState(null);

  const [noti, setNoti] = useState({message: "", success: true});

  const createBlogFormRef = useRef();

  const handleLogin = e => {
    e.preventDefault();

    blogService
      .loginBlog({username, password})
      .then(res => {
        console.log(res);
        window.localStorage.setItem(
          "loggedNoteappUser", JSON.stringify(res)
        )
        blogService.setToken(res.token);
        setUser(res);
        setNoti({...noti, message: `Welcome back ${res.username}`, success: true})
        setTimeout(()=>setNoti({...noti, message:''}),1500)
        setUsername("");
        setPassword("");
      }).catch(err => {
        console.log(err)
        setNoti({...noti, message: "Wrong username or password", success: false})
        setTimeout(()=>setNoti({...noti, message:''}),1500)
      })
  }

  const handleLogout = e => {
    e.preventDefault();
    setUser(null);
    window.localStorage.removeItem("loggedNoteappUser");
  }

  const handleCreatePost = newBlogObj => {
    if (newBlogObj.author === "") {
      setNoti({...noti, message: "Author is missing", success: false})
      setTimeout(()=>setNoti({...noti, message:''}),1500)
    } else if (newBlogObj.url === "") {
        setNoti({...noti, message: "Url is missing", success: false})
        setTimeout(()=>setNoti({...noti, message:''}),1500)
      } else {
      createBlogFormRef.current.toggleVisibility();
      blogService.createBlog(newBlogObj).then(res => {
        setBlogs(blogs.concat(res));
        setNoti({...noti, message: `A new blog ${newBlogObj.title} by ${newBlogObj.author} added`, success: true})
        setTimeout(()=>setNoti({...noti, message:''}),1500)
      }).catch(err => {
        console.log(err);
        setNoti({...noti, message: "Errors! Please do again.", success: false})
        setTimeout(()=>setNoti({...noti, message:''}),1500)
      })
    }
  }

  const handleLike = e => {
    const tempBlog = blogs.find(item => item.id === parseInt(e.target.value, 10) );
    blogService
      .updateLike({...tempBlog, likes: tempBlog.likes + 1})
      .then(res => setBlogs(blogs.map(blog => blog.id === res.id ? res : blog)))
      .catch(err => console.log(err))
  }

  const handleDeleteBlog = e => {
    const tempBlog = blogs.find(item => item.id === parseInt(e.target.value, 10) );
    if (window.confirm(`Remove ${tempBlog.title} by ${tempBlog.author}?`)) {
      blogService
      .deleteBlog(e.target.value)
      .then(() => {
        setBlogs(blogs.filter(blog => blog.id !== parseInt(e.target.value, 10)))
        setNoti({...noti, message: `Remove ${tempBlog.title} by ${tempBlog.author}`, success: false})
        setTimeout(()=>setNoti({...noti, message:''}),1500)
      })
      .catch(err => {
        console.log(err)
        setNoti({...noti, message: `${tempBlog.title} by ${tempBlog.author} has already been removed`, success: false})
        setTimeout(()=>setNoti({...noti, message:''}),1500)
      })
    }
  }

  useEffect(() => {
    const loggedUser = window.localStorage.getItem("loggedNoteappUser");
    if (loggedUser) {
      setUser(JSON.parse(loggedUser));
      blogService.setToken(JSON.parse(loggedUser).token);
    }
  },[]);

  useEffect(() => {
    blogService.getAll().then(blogs =>
      setBlogs(blogs)
    )
  }, [])

  return (
    <div>
      <h2>blogs</h2>
      <Notification noti={noti} />
      {!user &&
        <div>
          <LoginForm
            handleLogin={handleLogin}
            username={username}
            setUsername={setUsername}
            password={password}
            setPassword={setPassword} 
          />
        </div>
      }
      {user &&
        <div>
          <h3>{user.name} logged in 
            <button key={user.token} onClick={handleLogout}>
              Logout
            </button>
          </h3>
          <Togglable buttonLabel="New Post" ref={createBlogFormRef}>
            <CreatePostForm handleCreatePost={handleCreatePost} />
          </Togglable>
          {blogs.sort((a,b) => b.likes - a.likes).map(blog =>
            <Blog 
              key={blog.id} 
              blog={blog} 
              handleLike={handleLike}
              handleDeleteBlog={handleDeleteBlog} 
              user={user}
            />
          )}
      </div>
      }
    </div>
  )
}
