import axios from 'axios'
const baseUrl = '/api/blogs'

let token = null;

const setToken = newToken => {
  token = `bearer ${newToken}`;
}

const createBlog = async newBlogObject => {
  const config = {
    headers: { Authorization: token },
  }
  const response = await axios.post(baseUrl, newBlogObject, config);
  return response.data;
}

const getAll = () => {
  const request = axios.get(baseUrl)
  return request.then(response =>  response.data)
}

const loginBlog = async credential => {
  const response = await axios.post("/api/login", credential);
  return response.data;
}

const updateLike = async newBlog => {
  const response = await axios.put(`${baseUrl}/${newBlog.id}`, newBlog);
  return response.data;
}

const deleteBlog = async blogId => {
  const config = {
    headers: { Authorization: token },
  }
  await axios.delete(`${baseUrl}/${blogId}`, config);
}

const blogService = { getAll, loginBlog, setToken, createBlog, updateLike, deleteBlog }

export default blogService
