import React, { useState, useImperativeHandle } from 'react';
import PropTypes from "prop-types";

export const Blog = ({blog, handleLike, handleDeleteBlog, user}) => {
  const [visible, setVisible] = useState(false);

  const blogStyle = {
    paddingTop: 10,
    paddingLeft: 2,
    border: 'solid',
    borderWidth: 1,
    marginBottom: 5
  }

return(
  <div style={blogStyle} className="blog">
    <span>{blog.title} {blog.author}</span> <button value={blog.id} onClick={() => setVisible(!visible)}>
      {visible ? "hide" : "show"}    
    </button>
    {visible && 
      <div>
        <span>{blog.url}</span><br/>
        <span>Likes: {blog.likes}</span> <button value={blog.id} onClick={handleLike}>like</button><br/>
        <span>{blog.user.name}</span><br/>
        {blog.user._id === user.id &&
        <button value={blog.id} onClick={handleDeleteBlog}>Remove</button>}
      </div>
    }  
  </div>  
)}

export const LoginForm = ({
  handleLogin, 
  username, 
  setUsername, 
  password, 
  setPassword
  }) => (
  <form onSubmit={handleLogin} className="login-form">
    <div>
      Username
        <input
          type="text"
          value={username}
          name="Username"
          id="username"
          onChange={e => setUsername(e.target.value)}
        />
    </div>
    <div>
      Password
        <input
          type="password"
          value={password}
          name="Password"
          id="password"
          onChange={e => setPassword(e.target.value)}
        />
    </div>
    <button type="submit" id="login-button">Login</button>
  </form>
)

export const CreatePostForm = ({
  handleCreatePost,
}) => {
  const [title, setTitle] = useState("");
  const [author, setAuthor] = useState("");
  const [url, setUrl] = useState("");

  const createNewBlog = e => {
    e.preventDefault();
    const newBlogObj = {
      title: title.trim(),
      author: author.trim(),
      url: url.trim(),
      likes: 0
    }
    handleCreatePost(newBlogObj);
    setTitle("");
    setAuthor("")
    setUrl("");
  }

  return (
  <form onSubmit={createNewBlog} className="create-blog-form">
    <div>
      Title
        <input
          type="text"
          value={title}
          name="Title"
          id="title"
          onChange={e => setTitle(e.target.value)}
        />
    </div>
    <div>
      Author
        <input
          type="text"
          value={author}
          name="Author"
          id="author"
          onChange={e => setAuthor(e.target.value)}
        />
    </div>
    <div>
      Url
        <input
          type="text"
          value={url}
          placeholder="https://example.com"
          name="Url"
          id="url"
          onChange={e => setUrl(e.target.value)}
        />
    </div>
    <button type="submit" id="create-blog-button">Create</button>
  </form>
)}

export const Notification = ({noti}) => {
  if (noti.message === "") return null

  const NotiStyle = {
    background: '#fff0f2',
    color: !noti.success ? 'red':'green',
    fontSize: 20,
    borderStyle: 'solid',
    borderRadius: 5,
    padding:10,
    marginBottom: 10
  }
  return <div className="notification" style={NotiStyle}>{noti.message}</div>;
}

export const Togglable = React.forwardRef((props, ref) => {
  const [visible, setVisible] = useState(false);

  const hide = {display: visible ? "none" : ""};
  const show = {display: visible ? "" : "none"};

  const toggleVisibility = () => {
    setVisible(!visible);
  }

  useImperativeHandle(ref, () => {
    return {
      toggleVisibility
    }
  });

  return (
    <div>
      <div style={hide}>
        <button onClick={toggleVisibility}>{props.buttonLabel}</button>
      </div>
      <div style={show}>
        {props.children}
        <button onClick={toggleVisibility}>Cancel</button>
      </div>
    </div>
  );});

CreatePostForm.propTypes = {
  handleCreatePost: PropTypes.func
}

Notification.prototype = {
  noti: PropTypes.object
}
