
// *** ENTER COMMIT SHA OF YOUR REPO IN HERE ***
const commitSHA = 'a1d42eb'

// *** DO NOT REMOVE OR CHANGE THIS LINE ***
describe(`\nEND TO END TESTS: ${Cypress.env('SOLUTION') || 'your-solution'} [ ${commitSHA} ]\n`, function () {

  describe('Blog app', function() {
    beforeEach(function() {
      cy.request('POST', 'http://localhost:3003/api/testing/reset')
      const testUser = {
        username: "test-username",
        password: "test-password", 
        name: "test-name"
      }
      cy.request('POST', 'http://localhost:3003/api/users/', testUser)
      cy.visit('http://localhost:3000')
    })
  
    it('Login form is shown', function() {
      cy.get(".login-form").should("be.visible");
      cy.get("#username").should("be.visible");
      cy.get("#password").should("be.visible");
      cy.contains("Login")
    })

    describe('Login',function() {
      it('succeeds with correct credentials', function() {
        cy.get("#username").type("test-username");
        cy.get("#password").type("test-password");
        cy.get(".login-form").get("button").click()

        cy.contains("test-name logged in");
      })
  
      it('fails with wrong credentials', function() {
        cy.get("#username").type("test");
        cy.get("#password").type("test");
        cy.get(".login-form").get("button").click()

        cy.get(".notification").contains("Wrong username or password");
      })
    })

    describe('When logged in', function() {
      beforeEach(function() {
        cy.request('POST', 'http://localhost:3003/api/login', {
          username: "test-username", password: "test-password"
        }).then(response => {
          localStorage.setItem('loggedNoteappUser', JSON.stringify(response.body))
          cy.visit('http://localhost:3000')
          })
      })
  
      it('A blog can be created', function() {
        cy.contains("New Post").click();
        cy.get("#title").type("new title");
        cy.get("#author").type("new author");
        cy.get("#url").type("https://example.com");
        cy.get("#create-blog-button").click();

        cy.contains("New Post");
        cy.contains("new title new author")
      })

      describe("and a blog exist", function() {
        beforeEach(function() {
          cy.request({
            method: "POST", 
            url: 'http://localhost:3003/api/blogs',
            body: {
              title: "new title", author: "new author", url: "https://example.com", likes: 0
            },
            headers: {
              'Authorization': `bearer ${JSON.parse(localStorage.getItem('loggedNoteappUser')).token}`
            }
          })
          cy.visit('http://localhost:3000')
        })

        it("blog can be liked", function() {
          cy.contains("new title new author").parent().contains("show").click();
          cy.contains("like").click();
          cy.contains("1");
        })
        it("blog can be deleted", function() {
          cy.contains("new title new author").parent().contains("show").click();
          cy.contains("Remove").click();
          cy.get("new title new author").should("not.exist");
        })
      })

      describe("Many blogs added", function() {
        beforeEach(function() {
          [0,1,2,3].forEach(num => cy.request({
            method: "POST", 
            url: 'http://localhost:3003/api/blogs',
            body: {
              title: "new title", author: "new author", url: "https://example.com", likes: num
            },
            headers: {
              'Authorization': `bearer ${JSON.parse(localStorage.getItem('loggedNoteappUser')).token}`
            }
          }))
          cy.visit('http://localhost:3000')
          it("blogs be in likes order", function() {
            cy.get(".blog").then(blogs => {
              for(let i = 0; i < blogs.length; i++) {
                cy.wrap(blogs[i]).contains("show").click();
                cy.wrap(blogs[i]).contains((3-i).toString());
              }
            })
          })
        })
      })
    })

  })
  
})
