import React, { useState } from 'react'
import PropTypes from 'prop-types'
import blogService from './services'
import loginService from './services'

export const blogsSorter = (blogs) => { return blogs.sort((a, b) => b.likes - a.likes) }

export const Blog = ({ blog, likeBlog, deleteBlog }) => {

    return (
        <ul className='blog'>
            <li>{blog.title}</li>
            <li>{blog.author}</li>
            <Togglable buttonLabel='Show' secondButtonLabel='Hide'>
                <li>{blog.url}</li>
                <li>
                    <span id='likes'>{blog.likes}</span>
                    <button
                        name='like'
                        onClick={likeBlog}
                    >Like</button>
                </li>
                <button
                    name='delete'
                    onClick={deleteBlog}
                >Delete</button>
            </Togglable>
        </ul>
    )
}

export const CreateBlog = ({ blogs, setBlogs, setNotification }) => {
    const [title, setTitle] = useState('')
    const [author, setAuthor] = useState('')
    const [url, setUrl] = useState('')

    const createBlog = async (event) => {
        event.preventDefault()

        const blog = {
            title: title,
            author: author,
            url: url
        }

        try {
            const response = await blogService.create(blog)
            setBlogs(blogs.concat(response))
            setNotification('Blog added succesfully.')
            setTitle('')
            setAuthor('')
            setUrl('')
            setTimeout(() => {
                setNotification('')
            }, 5000)
        } catch (e) {
            setNotification('Missing required information about blog')
        }
        setTimeout(() => {
            setNotification('')
        }, 5000)
    }

    return (
        <div className='create-blog'>
            <h2>Create new blog</h2>
            <form className='create-blog-form' onSubmit={createBlog}>
                <div className='input-blog'>
                    Title:
                    <input
                        id='title'
                        type='text'
                        value={title}
                        name='Title'
                        onChange={({ target }) => setTitle(target.value)}
                    />
                </div>
                <div className='input-blog'>
                    Author:
                    <input
                        id='author'
                        type='text'
                        value={author}
                        name='Author'
                        onChange={({ target }) => setAuthor(target.value)}
                    />
                </div>
                <div className='input-blog'>
                    Url:
                    <input
                        id='url'
                        type='text'
                        value={url}
                        name='Url'
                        onChange={({ target }) => setUrl(target.value)}
                    />
                </div>
                <button id='create-blog' type='submit'>Create</button>
            </form>
        </div>
    )
}


export const LoginForm = ({ setUser, setNotification }) => {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')

    const handleLogin = async (event) => {
        event.preventDefault()

        try {
            const user = await loginService.login({
                username, password
            })

            window.localStorage.setItem(
                'loggedBlogilistaUser', JSON.stringify(user)
            )
            blogService.setToken(user.token)
            setUser(user)
            setUsername('')
            setPassword('')
        } catch (e) {
            setNotification('Username or password wrong')
            setUsername('')
            setPassword('')
        }
        setTimeout(() => {
            setNotification('')
        }, 5000)
    }

    return (
        <div className="login-form">
            <h2>Log in to application</h2>
            <form onSubmit={handleLogin}>
                <div>
                    Username
                    <input
                        id='username'
                        type="text"
                        value={username}
                        name="Username"
                        onChange={({ target }) => setUsername(target.value)}
                    />
                </div>
                <div>
                    Password
                    <input
                        id='password'
                        type="password"
                        value={password}
                        name="Password"
                        onChange={({ target }) => setPassword(target.value)}
                    />
                </div>
                <button id='login-button' type="submit">Login</button>
            </form>
        </div>
    )
}

export const LogoutForm = ({ user }) => {


    const handleLogout = (event) => {
        event.preventDefault()
        window.localStorage.removeItem('loggedBlogilistaUser')
        window.location.reload()
    }

    return (
        <div className="logout-form">
            Logged in as {user.name}
            <button onClick={handleLogout}>Logout</button>
        </div>
    )
}


export const Notification = ({ message }) => {
    if (message === null) {
        return null
    }

    return (
        <div className='notification'>
            {message}
        </div>
    )
}



export const ShowBlogs = ({ blogs, setBlogs, setNotification }) => {

    const likeBlog = async (blog) => {
        const updatedBlog = {
            ...blog,
            likes: blog.likes + 1
        }
        try {
            const returnedBlog = await blogService.update(updatedBlog, updatedBlog.id)
            setBlogs(
                blogsSorter(
                    blogs.map(b => b.id !== blog.id ? b : returnedBlog)
                ))
            setNotification('Blog updated succesfully')
            setTimeout(() => {
                setNotification('')
            }, 5000)
        } catch (error) {
            setNotification('update failed', error)
        } setTimeout(() => {
            setNotification('')
        }, 5000)
    }

    const deleteBlog = async (id) => {
        const blogToDelete = blogs.find(b => b.id === id)
        const loggedUser = JSON.parse(localStorage.getItem('loggedBlogilistaUser')).username
        if (loggedUser === blogToDelete.user.username) {
            const result = window.confirm(`Delete ${blogToDelete.title}?`)
            if (result === true) {
                try {
                    await blogService.remove(id)
                    setBlogs(
                        blogsSorter(
                            blogs.filter(b => b.id !== id)
                        ))
                    setNotification('Blog deleted succesfully')
                    setTimeout(() => {
                        setNotification('')
                    }, 5000)
                } catch (error) {
                    setNotification('Blog was already deleted')
                } setTimeout(() => {
                    setNotification('')
                }, 5000)
            }
        } else {
            setNotification('Cannot delete blogs saved by another user')
        } setTimeout(() => {
            setNotification('')
        }, 5000)
    }



    return (
        <div className="blogs">
            <h2>Blogs</h2>
            {blogs.map(blog =>
                <Blog
                    key={blog.id}
                    blog={blog}
                    likeBlog={() => likeBlog(blog)}
                    deleteBlog={() => deleteBlog(blog.id)}
                />
            )}
        </div>
    )
}

export const Togglable = (props) => {
    const [visible, setVisible] = useState(false)

    const hideWhenVisible = { display: visible ? 'none' : '' }
    const showWhenVisible = { display: visible ? '' : 'none' }

    const toggleVisibility = () => {
        setVisible(!visible)
    }

    Togglable.propTypes = {
        buttonLabel: PropTypes.string.isRequired
    }

    Togglable.displayName = 'Togglable'

    return (
        <div>
            <div style={hideWhenVisible}>
                <button onClick={toggleVisibility}>{props.buttonLabel}</button>
            </div>
            <div style={showWhenVisible} className='hiddenStuff'>
                {props.children}
                <button onClick={toggleVisibility}>{props.secondButtonLabel}</button>
            </div>
        </div>
    )
}

