import axios from 'axios'
const baseUrlBlog = '/api/blogs'
const baseUrlLogin = '/api/login'

let token = null

const setToken = newToken => {
    token = `bearer ${newToken}`
}

const getAll = async () => {

    const response = await axios.get(baseUrlBlog)
    return response.data
}

const create = async (newBlog) => {

    const config = { headers: { Authorization: token }
    }

    const response = await axios.post(baseUrlBlog, newBlog, config)
    return response.data
}

const update = async (updatedBlog, id) => {

    const config = { headers: { Authorization: token }
    }

    const response = await axios.put(`${baseUrlBlog}/${id}`, updatedBlog, config)
    return response.data
}

const remove = async (id) => {

    const config = { headers: { Authorization: token } }
    const request = axios.delete(`${baseUrlBlog}/${id}`,config)
    const response = await request
    return response.data
}

const login = async credentials => {

    const response = await axios.post(baseUrlLogin, credentials)
    return response.data
}

export default { getAll, create, update, setToken, remove, login }
