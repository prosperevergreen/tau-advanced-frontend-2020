import React, { useState, useEffect } from 'react'
import { Notification, Togglable, LoginForm, ShowBlogs, LogoutForm, CreateBlog, blogsSorter } from './components'
import blogService from './services.js'

// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE                        //
// ------------------------------------------------------------ //
export const commitSHA = '1ca2c5f'
// ------------------------------------------------------------ //


export const App = () => {
    const [blogs, setBlogs] = useState([])
    const [user, setUser] = useState(null)
    const [notification, setNotification] = useState('')

    useEffect(() => {
        blogService
            .getAll()
            .then(blogs =>
                setBlogs( blogsSorter(blogs) )
            )
    },[blogsSorter(blogs)])

    useEffect(() => {
        const loggedUserJSON = window.localStorage.getItem('loggedBlogilistaUser')
        if (loggedUserJSON) {
            const user = JSON.parse(loggedUserJSON)
            setUser(user)
            blogService.setToken(user.token)
        }
    }, [])

    if (user === null) {
        return (
            <div className="login-form">

                <div className='header'>
                    <h1>Blogilista</h1>
                    <Notification message={notification} />
                </div>

                <div className='container'>
                    <LoginForm
                        user={user}
                        setUser={setUser}
                        setNotification={setNotification}
                    />
                </div>

            </div>
        )
    }

    return (
        <div>
            <div className='header'>

                <h1>Blogilista</h1>

                <LogoutForm user={user} />

            </div>

            <Notification message={notification} />

            <div className='container'>
                <Togglable buttonLabel='Create blog' secondButtonLabel='Cancel'>
                    <CreateBlog
                        blogs={blogs}
                        setBlogs={setBlogs}
                        setNotification={setNotification}
                    />
                </Togglable>

                <ShowBlogs
                    blogs={blogs}
                    setBlogs={setBlogs}
                    setNotification={setNotification}
                />
            </div>

        </div>
    )
}

