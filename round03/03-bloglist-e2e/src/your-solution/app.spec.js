// *** ENTER COMMIT SHA OF YOUR REPO IN HERE ***
const commitSHA = "96911b2";

// *** DO NOT REMOVE OR CHANGE THIS LINE ***
describe(`\nEND TO END TESTS: ${
	Cypress.env("SOLUTION") || "your-solution"
} [ ${commitSHA} ]\n`, function () {
	describe("Blog app", function () {
		beforeEach(function () {
			cy.request("POST", "http://localhost:3003/api/testing/reset");
			cy.visit("http://localhost:3000");
		});

		it("Login form is shown", function () {
			cy.get("form").should("have.class", "login-form");
			cy.contains("log in to application");
			cy.get("input").should("have.length", 2);
			cy.get("#password").should("exist");
			cy.get("#username").should("exist");
			cy.contains("login");
		});

		describe("Login", function () {
			beforeEach(function () {
				const testUser = {
					name: "John Doe",
					username: "test",
					password: "test",
				};
				cy.request("POST", "http://localhost:3003/api/users", testUser);
			});

			it("succeeds with correct credentials", function () {
				cy.get("#username").type("test");
				cy.get("#password").type("test");
				cy.contains("login").click();

				cy.contains("John Doe logged in");
			});

			it("fails with wrong credentials", function () {
				cy.get("#username").type("hello");
				cy.get("#password").type("world");
				cy.contains("login").click();

				cy.contains("wrong username or password");
				cy.contains("wrong username or password").should(
					"have.css",
					"color",
					"rgb(255, 0, 0)"
				);
			});

			describe("When logged in", function () {
				beforeEach(function () {
					cy.get("#username").type("test");
					cy.get("#password").type("test");
					cy.get("#login-btn").click();
				});

				it("A blog can be created", function () {
					cy.contains("new note").click();
					cy.contains("create new");
					cy.get("#title").type("Hello World!!!");
					cy.get("#author").type("Nerd");
					cy.get("#url").type("www.helloworld.com");
					cy.get("#create-blog-btn").click();
					cy.contains("a new blog Hello World!!! by Nerd added");
					cy.contains("Hello World!!! Nerd");
				});

				describe("Blogs can be liked, deleted and sorted", function () {
					beforeEach(function () {
						cy.contains("new note").click();
						cy.get("#title").type("Hello World!!!");
						cy.get("#author").type("Nerd");
						cy.get("#url").type("www.helloworld.com");
						cy.get("#create-blog-btn").click();
						cy.contains("Hello World!!! Nerd").contains("view").click();
					});

					it("A blog can be liked", function () {
						cy.contains("likes 0");
						cy.get("button").contains("like").click();
						cy.contains("likes 1");
					});

					it("A blog can be deleted", function () {
						cy.contains("Hello World!!! Nerd");
						cy.contains("remove").click();
						cy.on("window:confirm", () => true);
						cy.contains("Hello World!!! Nerd").should("not.exist");
					});

					it("A blog cannot be deleted by other user", function () {
						cy.contains("logout").click();

						const testUser = {
							name: "Jane Doe",
							username: "test2",
							password: "test2",
						};
						cy.request("POST", "http://localhost:3003/api/users", testUser);

						cy.get("#username").type("test2");
						cy.get("#password").type("test2");
						cy.contains("login").click();

						cy.contains("Jane Doe logged in");
						cy.contains("Hello World!!! Nerd").contains("view").click();
						cy.contains("remove").should("not.exist");
					});
				});

				describe("Blogs are sorted", function () {
					beforeEach(function () {
						const posts = [
							{
								author: "Arto Hellas",
								title: "Ohjelmoinnin MOOC 2019",
								url: "www.helloworld.com",
								likes: 3,
								userId: 1,
							},
							{
								author: "Ada Lovelace",
								title: "Kääntäjän merkintöjä",
								url: "www.helloworld.com",
								likes: 1,
								userId: 1,
							},
							{
								author: "Dan Abramov",
								title: "Fundamentals of Redux",
								url: "www.helloworld.com",
								likes: 2,
								userId: 1,
							},
						];

						posts.map((post) =>
							cy.request("POST", "http://localhost:3004/blogs", post)
						);
						cy.reload();
					});

					it("blogs are sorted", function () {
						cy.contains("Ohjelmoinnin MOOC 2019 Arto Hellas");
						cy.contains("Kääntäjän merkintöjä Ada Lovelace");
						cy.contains("Fundamentals of Redux Dan Abramov");
						cy.get(".blog").should("have.length", 3);

						const likes = [3, 2, 1];
						cy.get(".blog").each(($el, index, $list) => {
							// $el is a wrapped jQuery element
							// wrap this element so we can
							// use cypress commands on it
							cy.wrap($el).contains(`view`).click();
							cy.wrap($el).contains(`likes ${likes[index]}`);
						});
					});
				});
			});
		});
	});
});
