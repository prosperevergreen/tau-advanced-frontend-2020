import axios from 'axios'
const baseUrl = '/api/blogs'

const getAllBlogs = () => {
  const request = axios.get(baseUrl)
  return request.then((response) => response.data)
}

const getAllUsers = () => {
  const request = axios.get(baseUrl)
  return request.then((response) => response.data)
}

const login = (data) => {
  const request = axios.post('api/login', data)
  return request.then((response) => response.data)
}

const postBlog = (data, token) => {
  const request = axios.post('api/blogs', data, {
    headers: { Authorization: `bearer ${token}` },
  })
  return request.then((response) => response.data)
}

const updateBlog = (id, data, token) => {
  const request = axios.put(`api/blogs/${id}`, data, {
    headers: { Authorization: `bearer ${token}` },
  })
  return request.then((response) => response.data)
}

const deleteBlog = (id, token) => {
  const request = axios.delete(`api/blogs/${id}`, {
    headers: { Authorization: `bearer ${token}` },
  })
  return request.then((response) => response.data)
}



export default { getAllBlogs, getAllUsers, login, postBlog, updateBlog, deleteBlog }
