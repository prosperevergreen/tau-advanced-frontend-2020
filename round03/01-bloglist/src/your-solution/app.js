import React, { useState, useEffect } from 'react'
import { BlogSection, LoginForm } from './components'
import blogService from './services'

// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE                        //
// ------------------------------------------------------------ //
export const commitSHA = '2de9a25'
// ------------------------------------------------------------ //

export const App = () => {
  const [blogs, setBlogs] = useState([])
  const [user, setUser] = useState(null)
  const [notification, setNotification] = useState(null)

  useEffect(() => {
    blogService.getAllBlogs().then((blogs) => setBlogs(blogs.sort(sortBlogs)))
  }, [])

  useEffect(() => {
    const loggedUser = getItem('user')
    if (loggedUser) {
      setUser(loggedUser)
    }
  }, [])

  const sortBlogs = (blog1, blog2) => blog2.likes - blog1.likes

  const setItem = (key, value) => {
    const item = JSON.stringify(value)
    window.localStorage.setItem(key, item)
  }

  const removeItem = (key) => {
    window.localStorage.removeItem(key)
  }

  const getItem = (key) => {
    const item = window.localStorage.getItem(key)
    const value = JSON.parse(item)
    return value
  }

  const handleUpdateBlog = async (id, updatedBlog) => {
    try {
      const savedBlog = await blogService.updateBlog(
        id,
        updatedBlog,
        user.token
      )

      const updatedBlogs = blogs.map((blog) =>
        blog.id === id ? { ...blog, likes: savedBlog.likes } : blog
      )

      setBlogs(updatedBlogs.sort(sortBlogs))

      // Clear previous notification
      setNotification(null)
    } catch (exception) {
      notify('error', 'Error on updating post')
    }
  }

  const handleDeleteBlog = async (id) => {
    try {
      await blogService.deleteBlog(id, user.token)

      const updatedBlogs = blogs.filter((blog) => blog.id !== id)

      setBlogs(updatedBlogs)

      // Clear previous notification
      setNotification(null)
    } catch (exception) {
      notify('error', 'Error on deleting post')
    }
  }

  const handleLogin = async (data) => {
    try {
      const user = await blogService.login(data)

      setItem('user', user)
      setUser(user)
      // Clear previous notification
      setNotification(null)
    } catch (exception) {
      notify('error', 'wrong username or password')
    }
  }

  const notify = (type, message) => {
    const newNotification = { type, message }

    // Create notification
    setNotification(newNotification)
    // Remove notifications after 5 seconds
    setTimeout(() => {
      setNotification(null)
    }, 5000)
  }

  const handleLogout = () => {
    setUser(null)
    setNotification(null)
    removeItem('user')
  }

  const handlePostBlog = async (blog) => {
    try {
      const addedBlog = await blogService.postBlog(blog, user.token)
      setBlogs(blogs.concat(addedBlog))
      notify(
        'success',
        `a new blog ${addedBlog.title} by ${addedBlog.author} added`
      )
    } catch (exception) {
      notify('error', 'blog could not be added')
    }
  }

  return (
    <div>
      {user === null ? (
        <LoginForm handleLogin={handleLogin} notification={notification} />
      ) : (
        <BlogSection
          blogs={blogs}
          user={user}
          logout={handleLogout}
          postBlog={handlePostBlog}
          notification={notification}
          updateBlog={handleUpdateBlog}
          deleteBlog={handleDeleteBlog}
        />
      )}
    </div>
  )
}
