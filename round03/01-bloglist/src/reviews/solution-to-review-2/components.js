import React, { useState, useImperativeHandle } from 'react'


export const Blog = ({blog, blogs, createLike, createDelete}) => {

  const addLikes = (event) => {
    event.preventDefault()
    let toBeUpdated = blogs.find(blog => blog.title === event.target.value)
    toBeUpdated.likes = (toBeUpdated.likes +1)
    const newBlogLike = ({user: toBeUpdated.id, likes: toBeUpdated.likes, author: toBeUpdated.author, title: toBeUpdated.title, url: toBeUpdated.url})
    createLike(newBlogLike)
  }

  const deleteBlog = (event) => {
    console.log(event.target.value)
    event.preventDefault()
    let toBeDeleted = blogs.find(blog => blog.title === event.target.value)
    const newDeleteList = { ...toBeDeleted, important: !toBeDeleted.important }
    createDelete(newDeleteList)
  }

  return(
    <div style={blogStyle}>
        {blog.title} {blog.author}
        <Togglable buttonLabel="view details"
                    cancelLabel="hide">
          <p> {blog.url}</p>
          <p> likes: {blog.likes} <button value={blog.title}type="button" onClick={addLikes}>like</button></p>
          <p> {blog.user.name} </p>
          <button type="button" value={blog.title} onClick={deleteBlog}>delete</button>
        </Togglable>
    </div>
  )
}


const blogStyle = {
  paddingTop: 10,
  paddingLeft: 2,
  border: 'solid',
  borderWidth: 1,
  marginBottom: 10
}

export const Notification = ({message, styleMessage}) => {
  return(
   <h3 style={styleMessage}>{message}</h3>
  )
}

export const LoginForm = ({
  handleSubmit,
  handleUsernameChange,
  handlePasswordChange,
  username,
  password
 }) => {
   return(
    <div>
      <h2>Login</h2>
      <form onSubmit={handleSubmit}>
          <div>
            username
            <input
            type="text"
            value={username}
            name="Username"
            onChange={handleUsernameChange}
            />
          </div>
          <div>
            password
            <input
            type="password"
            value={password}
            name="Password"
            onChange={handlePasswordChange}/>
          </div>
          <button type="submit">log in</button>
        </form>
      </div>
  )
}

export const BlogForm = ({ createBlog, blogs }) => {
  const [newBlogTitle, setNewBlogTitle] = useState('')
  const [newBlogAuthor, setNewBlogAuthor] = useState('')

  const addNewBlog = (event) => {
    event.preventDefault()
    let urlEnd = (blogs.length + 1)
    let newBlog = ({title: newBlogTitle, author: newBlogAuthor, url: "http://host/" + urlEnd, likes: 0})
    createBlog(newBlog)
    setNewBlogAuthor("")
    setNewBlogTitle("")
  }

  return (
  <form onSubmit={addNewBlog}>
    <p>title</p>
    <input
    value={newBlogTitle}
      onChange={({ target }) => setNewBlogTitle(target.value)}
    />
    <p>author</p>
    <input
      value={newBlogAuthor}
      onChange={({ target }) => setNewBlogAuthor(target.value)}
    />
    <button type="submit">save</button>
  </form>  

)
  }


export const Togglable = React.forwardRef((props, ref) => {
  const [visible, setVisible] = useState(false)

  const hideWhenVisible = { display: visible ? 'none' : '' }
  const showWhenVisible = { display: visible ? '' : 'none' }

  const toggleVisibility = () => {
    setVisible(!visible)
  }
  
  useImperativeHandle(ref, () => {
    return {
      toggleVisibility    
    }  
  })

  return (
    <div>
      <div style={hideWhenVisible}>
        <button onClick={toggleVisibility}>{props.buttonLabel}</button>
      </div>
      <div style={showWhenVisible}>
        {props.children}
        <button onClick={toggleVisibility}>{props.cancelLabel}</button>
      </div>
    </div>
  )
})


