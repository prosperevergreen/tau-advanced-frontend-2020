import React, { useState, useEffect, useRef } from 'react'
import { Blog } from './components'
import blogService from './services.js'
import { Notification } from './components'
import { LoginForm } from './components'
import { Togglable } from './components'
import { BlogForm } from './components'


// ------------------------------------------------------------ //
// ENTER COMMIT SHA OF YOUR REPO IN HERE                        //
// ------------------------------------------------------------ //
export const commitSHA = 'e84bfcb'
// ------------------------------------------------------------

export const App = () => {
  const [blogs, setBlogs] = useState([])
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [user, setUser] = useState(null)
  const [errorMessage, setErrorMessage] = useState(null)
  const [successMessage, setSuccessMessage] = useState(null)

  const noteFormRef = useRef()



  useEffect(() => {
    blogService.getAll().then(blogs =>
      setBlogs(blogs)
    )
  }, [])


  useEffect(() => {
    const loggedUserJSON = window.localStorage.getItem('loggedNoteappUser')
    if (loggedUserJSON) {
      const user = JSON.parse(loggedUserJSON)
      setUser(user)
      blogService.setToken(user.token)
    }
  },
  [])


  const handleLogin = async (event) => {
    event.preventDefault()
    try {
      const user = await blogService.login({ username, password })
      blogService.setToken(user.token)
      window.localStorage.setItem(
        'loggedNoteappUser', JSON.stringify(user)
      )
      setUser(user)
      setUsername('')
      setPassword('')
    }
    catch (exception) {
      setErrorMessage('Wrong credentials')
      setTimeout(() => {
        setErrorMessage(null)
      }, 5000)
    }
    setPassword('')
    setUsername('')
  }

  const handleLogOut = () => {
    window.localStorage.clear()
    window.location.reload()
  }


  const blogForm = () => (
    <Togglable buttonLabel="new note" ref={noteFormRef}
      cancelLabel="cancel">
      <BlogForm createBlog={addNewBlog}
        blogs={blogs} />
    </Togglable>
  )


  const loginForm = () => (
    <Togglable buttonLabel="log in"
      cancelLabel="cancel">
      <LoginForm
        username={username}
        password={password}
        handleUsernameChange={({ target }) => setUsername(target.value)}
        handlePasswordChange={({ target }) => setPassword(target.value)}
        handleSubmit={handleLogin}
      />
    </Togglable>
  )


  const addNewBlog = (blogObject) => {
    noteFormRef.current.toggleVisibility()
    blogService
      .create(blogObject)
      .then(response => {
        setBlogs(blogs.concat(response))
        setSuccessMessage('A new blog ' + blogObject.title + ' by ' + blogObject.author + ' added successfully!')
        setTimeout(() => {
          setSuccessMessage(null)
        }, 5000) }
      )
  }

  const addLikes = (blogObject) => {
    blogService
      .update(blogObject.user, blogObject)
      .then(response => {
        setBlogs(blogs.map(blog => blog.id !== blogObject.id? blog : response.data))
        setTimeout(() => {
          setSuccessMessage(null)
        }, 5000) }
      )
  }

  const deleteBlog = (blogObject) => {
    if(window.confirm('Are you sure you want to delete ' + blogObject.title + '?')){
      blogService
        .deleteBlog(blogObject.id, blogObject)
        .then(() => {
          setBlogs(blogs.filter(blog => blog.id !== blogObject.id))
          setSuccessMessage(blogObject.title + ' deleted successfully!')
          setTimeout(() => {
            setSuccessMessage(null)
          }, 5000) }
        )
        .catch((error) => {
          console.log(error)
          setErrorMessage(blogObject.title + ' was already removed from server')
          setTimeout(() => {
            setErrorMessage(null)
          }, 5000)

          setBlogs(blogs.filter(blog => blog.id !== blogObject.id))
        })
    }
  }

  // Handle bloglist sort
  const sortedBlogs = blogs.sort(function(a, b) {
    var likesA = a.likes
    var likesB = b.likes
    if (likesA < likesB) {
      return 1
    }
    if (likesA > likesB) {
      return -1
    }
    return 0
  })


  if (user === null) {
    return (
      <div>
        <Notification message={errorMessage}
          styleMessage={{ color : 'red' }} />
        <Notification message={successMessage}
          styleMessage={{ color : 'green' }}  />
        {loginForm()}
      </div>
    )
  }
  return(
    <div>
      <div>
        <h2>blogs</h2>
        <Notification message={errorMessage}
          styleMessage={{ color : 'red' }} />
        <Notification message={successMessage}
          styleMessage={{ color : 'green' }} />
        <p>{user.name} logged-in <button type="button" onClick={handleLogOut}>log out</button></p>
      </div>
      <div>
        {sortedBlogs.map(blog =>
          <Blog key={blog.id} blog={blog}
            createDelete={deleteBlog}
            createLike={addLikes}
            blogs={blogs}
          />
        )}
        {blogForm()}
      </div>
    </div>
  )
}

