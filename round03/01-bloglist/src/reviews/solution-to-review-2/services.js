import axios from 'axios'
const baseUrl = '/api/blogs'
const logUrl = 'api/login'

let token = null

const getAll = () => {
  const request = axios.get(baseUrl)
  return request.then(response =>  response.data)
}

const login = async (credentials)  => {
  const response = await axios.post(logUrl, credentials)
  return response.data
}

const setToken = newToken => {
  token = `bearer ${newToken}`
}

const create = async newObject => {
  const config = {    
    headers: { Authorization: token },  
}

  const response = await axios.post(baseUrl, newObject, config)
  return response.data
}

const deleteBlog = async (id, newObject) => {
  const response = await axios.delete(`${baseUrl}/${id}`, newObject)
  return response.data
}


const update = async (id, newObject) => {
  const request = axios.put(`${baseUrl}/${id}`, newObject)
  const response = await request
  return response.data
}

const toBeExported =  { getAll, login, update, create, setToken, deleteBlog }

export default toBeExported

