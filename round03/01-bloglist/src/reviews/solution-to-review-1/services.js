import axios from 'axios'
const blogUrl = '/api/blogs'
const loginUrl = '/api/login'

let token = null

const getAll = async () => {
  const response = await axios.get(blogUrl)
  response.data.sort((a, b) => {
    if (a.likes > b.likes) return -1
    if (b.likes > a.likes) return 1
    return 0
  })
  return response
}

const createBlog = async (blog) => {
  const config = {
    headers: { Authorization: token },
  }

  blog['likes'] = 0

  const response = await axios.post(blogUrl, blog, config)
  return response
}

const addLike = async (blog) => {
  const config = {
    headers: { Authorization: token },
  }

  blog.likes += 1

  const response = await axios.put(blogUrl + '/' + blog.id, blog, config)
  return response
}

const deleteBlog = async (blog) => {

  const config = {
    headers: { Authorization: token },
  }

  const response = await axios.delete(blogUrl + '/' + blog.id, config)
  return response
}

const login = async (credentials) => {
  const response = await axios.post(loginUrl, credentials)
  return response.data
}

const setToken = newToken => {
  token = `bearer ${newToken}`
}

export default {
  getAll,
  login,
  setToken,
  createBlog,
  deleteBlog,
  addLike
}
