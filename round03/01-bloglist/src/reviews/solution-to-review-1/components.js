import React, { useState, useEffect } from 'react'
import blogService from './services'
import PropTypes from 'prop-types'

export const Blog = ({
  blog,
  user,
  removeBlog,
}) => {

  const blogStyle = {
    paddingTop: 10,
    paddingLeft: 2,
    border: 'solid',
    borderWidth: 1,
    marginBottom: 5
  }

  const [showDetails, setShowDetails] = useState(false)
  const [likes, setLikes] = useState(blog.likes)

  const toggleBlogDetails = async (event) => {
    event.preventDefault()
    setShowDetails(!showDetails)
  }

  const increaseLikes = async (event) => {
    event.preventDefault()

    try {
      const response = await blogService.addLike(blog)

      if (response.status === 200) {
        setLikes(likes + 1)
      } else {
        console.log('failed to add a like')
      }

    } catch (exception) {
      console.log('failed to add a like')
    }
  }

  const deleteBlog = (event) => {
    event.preventDefault()
    removeBlog(blog)
  }

  if (showDetails) {
    return (
      <div style={blogStyle}>
        <p>
          {blog.title} {blog.author}
          <button onClick={toggleBlogDetails}>hide</button>
        </p>

        {blog.url}

        <p>likes {likes} <button onClick={increaseLikes}>like</button></p>

        {blog.user.name}

        {user.name === blog.user.name ?
          <button onClick={deleteBlog}>remove</button> :
          ''
        }

      </div>
    )
  } else {
    return (
      <div style={blogStyle}>
        <p>
          {blog.title} {blog.author}
          <button onClick={toggleBlogDetails}>view</button>
        </p>
      </div>
    )
  }
}

export const Notification = (props) => {

  const notification = props.message

  const notificationStyle = {
    color: notification.error ? 'red' : 'green',
    background: 'lightgrey',
    fontSize: '20px',
    borderStyle: 'solid',
    borderRadius: '5px',
    padding: '10px',
    marginBottom: '10px',
  }

  if (notification.text === '') {
    return null
  }

  return (
    <div style={notificationStyle}>
      {notification.text}
    </div>
  )
}

export const BlogForm = ({
  setBlogs,
  setCreateNew
}) => {
  const [blog, setBlog] = useState({ title: '', author: '', url: '' })
  const [notificationMessage, setNotificationMessage] = useState({ text: '', error: false })

  const showNotification = (text, error) => {
    setNotificationMessage({ text: text, error: error })
    setTimeout(() => setNotificationMessage({ text: '', error: false }), 2000)
  }

  const handleNewBlog = async (event) => {
    event.preventDefault()

    try {
      const response = await blogService.createBlog(blog)

      if (response.status === 201) {
        showNotification(`a new blog ${response.data.title} by ${response.data.author} added`, false)
        let blogs = await blogService.getAll()
        setBlogs(blogs.data)
        setBlog({ title: '', author: '', url: '' })
      } else {
        showNotification('failed to create a blog')
      }
    } catch (exception) {
      showNotification('wrong credentials', true)
    }
  }

  const cancelBlogCreate = (event) => {
    event.preventDefault()
    setBlog({ title: '', author: '', url: '' })
    setCreateNew(false)
  }

  return(
    <div>
      <Notification message={notificationMessage} />

      <form onSubmit={handleNewBlog}>
        <div>
          title
          <input
            type="text"
            value={blog.title}
            name="Title"
            onChange={({ target }) => setBlog(
              {
                title: target.value,
                author: blog.author,
                url: blog.url
              }
            )}
          />
        </div>
        <div>
          author
          <input
            type="text"
            value={blog.author}
            name="Author"
            onChange={({ target }) => setBlog(
              {
                title: blog.title,
                author: target.value,
                url: blog.url
              }
            )}
          />
        </div>
        <div>
          url
          <input
            type="text"
            value={blog.url}
            name="URL"
            onChange={({ target }) => setBlog(
              {
                title: blog.title,
                author: blog.author,
                url: target.value
              }
            )}
          />
        </div>
        <button type="submit">create</button>
        <button onClick={cancelBlogCreate}>cancel</button>
      </form>
    </div>
  )
}

export const BlogContainer = ({
  user,
  userHandler
}) => {

  const [blogs, setBlogs] = useState([])
  const [createNew, setCreateNew] = useState(false)

  useEffect(async () => {
    let blogs = await blogService.getAll()
    setBlogs(blogs.data)
  }, [])

  const handleLogout = (event) => {
    event.preventDefault()
    window.localStorage.removeItem('loggedBlogappUser')
    userHandler(null)
  }

  const toggleBlogForm = () => {
    setCreateNew(!createNew)
  }

  const removeBlog = async (blog) => {
    if (window.confirm(`Remove blog ${blog.title} by ${blog.author}`)) {
      try {
        const response = await blogService.deleteBlog(blog)
        if (response.status === 204) {
          window.location.reload()
        }
      } catch (exception) {
        console.log('removing blog failed')
      }
    }
  }

  const blogList = () => (
    <div>
      <button onClick={toggleBlogForm}>new note</button>

      {blogs.map(blog =>
        <Blog key={blog.id} blog={blog} user={user} removeBlog={removeBlog}/>
      )}
    </div>
  )

  return (
    <div>
      <h2>blogs</h2>

      <p>
        {user.name} logged in
        <button onClick={handleLogout}>logout</button>
      </p>

      {createNew === true?
        <BlogForm setBlogs={setBlogs} setCreateNew={setCreateNew}/> :
        blogList()
      }

    </div>
  )
}

BlogContainer.propTypes = {
  user: PropTypes.object.isRequired,
  userHandler: PropTypes.func.isRequired
}