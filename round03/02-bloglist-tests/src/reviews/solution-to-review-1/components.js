import React, { useState, useEffect, useImperativeHandle } from 'react'
import PropTypes from 'prop-types'
import blogService from './services.js'

const BlogDetails = ({ blog, addLike, user, deleteBlog }) => {
  const isOwnedByUser = user.username === blog.user.username

  let likes = 0
  if (!isNaN(blog.likes))
    likes = blog.likes

  return (
    <div className="blog-details">
      <p>{blog.url}</p>
      <p className="likes">
        {likes} {likes === 1 ? 'like' : 'likes'}
        <button onClick={addLike}>Add</button>
      </p>
      <p>{blog.user.name}</p>
      {isOwnedByUser && <DeleteButton deleteBlog={deleteBlog} />}
    </div>
  )
}

const DeleteButton = ({ deleteBlog }) => <button onClick={deleteBlog}>delete</button>

const Blog = ({ blog, user, updateBlog, deleteBlog }) => {
  const blogStyle = {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 2,
    border: 'solid',
    borderWidth: 1,
    marginTop: 2,
    marginBottom: 2,
  }

  const [showDetails, setShowDetails] = useState(false)

  const handleDelete = () => {
    if (window.confirm(`Remove blog ${blog.title} by ${blog.author}?`)) {
      deleteBlog(blog.id)
    }
  }

  const handleAddLike = () => {
    if (isNaN(blog.likes)) {
      blog.likes = 0
    }
    const updatedBlog = { ...blog, likes: blog.likes + 1 }
    updateBlog(blog.id, updatedBlog)
  }

  return (
    <div className="blog" style={blogStyle}>
      <span className="blog-title">
        <b>{blog.title}</b>
      </span>{' '}
      <span className="blog-author">{blog.author}</span>{' '}
      <button onClick={() => setShowDetails(!showDetails)}>{showDetails ? 'hide' : 'view'}</button>
      {showDetails ? (
        <BlogDetails className="blogDetails" blog={blog} addLike={handleAddLike} user={user} deleteBlog={handleDelete} />
      ) : null}
    </div>
  )
}

Blog.propTypes = {
  blog: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  updateBlog: PropTypes.func.isRequired,
  deleteBlog: PropTypes.func.isRequired,
}

BlogDetails.propTypes = {
  blog: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  addLike: PropTypes.func.isRequired,
  deleteBlog: PropTypes.func.isRequired,
}

DeleteButton.propTypes = {
  deleteBlog: PropTypes.func.isRequired,
}

const Header = ({ title, notification }) => (
  <div>
    <h1>{title}</h1>
    <Notification notification={notification} />
  </div>
)

Header.propTypes = {
  title: PropTypes.string.isRequired,
  notification: PropTypes.object.isRequired
}

const Notification = ({ notification }) => {
  const { message, type } = notification

  if (!message) {
    return null
  }

  return <div className={type}>{message}</div>
}

Notification.propTypes = {
  notification: PropTypes.object.isRequired,
}

const LoginForm = ({ login, notify }) => {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')

  const handleLogin = async (event) => {
    event.preventDefault()

    try {
      const user = await blogService.login({ username, password })
      login(user)

      notify({
        message: 'Log in successful',
        type: 'success',
      })
    } catch (error) {
      notify({
        message: 'Wrong username or password',
        type: 'error',
      })
    }
  }

  return (
    <div>
      <form onSubmit={handleLogin}>
        <div>
          Username:{' '}
          <input
            id="username"
            type="text"
            value={username}
            name="username"
            onChange={({ target }) => setUsername(target.value)}
          />
        </div>
        <div>
          Password:{' '}
          <input
            id="password"
            type="password"
            value={password}
            name="password"
            onChange={({ target }) => setPassword(target.value)}
          />
        </div>
        <button id="login-btn" type="submit">Login</button>
      </form>
    </div>
  )
}

LoginForm.propTypes = {
  login: PropTypes.func.isRequired,
  notify: PropTypes.func.isRequired,
}

const Bloglist = ({ user, notify }) => {
  const [blogs, setBlogs] = useState([])
  const blogFormRef = React.useRef()

  useEffect(() => {
    const fetchAllBlogs = async () => {
      const blogsFromApi = await blogService.getAll()
      setBlogs(blogsFromApi.sort((a, b) => b.likes - a.likes))
    }
    fetchAllBlogs()
  }, [])

  const createNewBlog = async (blogObject) => {
    try {
      const returnedBlog = await blogService.create(blogObject)
      // Update list of blogs
      setBlogs(blogs.concat(returnedBlog))

      // Hide new blog form
      blogFormRef.current.toggleVisibility()

      notify({
        message: `New blog created: ${returnedBlog.title} by ${returnedBlog.author}`,
        type: 'success',
      })
    } catch (error) {
      notify({
        message: 'Could not create a new blog entry',
        type: 'error',
      })
    }
  }

  const updateBlog = async (blogId, blogObject) => {
    try {
      const updatedBlog = await blogService.update(blogId, blogObject)

      //Update blog list
      const updatedBlogs = blogs.map((b) => (b.id === updatedBlog.id ? updatedBlog : b))

      //Sort by likes
      setBlogs(updatedBlogs.sort((a, b) => b.likes - a.likes))

      notify({
        message: `Added a like for ${updatedBlog.title}`,
        type: 'success',
      })
    } catch (error) {
      notify({
        message: 'Could not update blog',
        type: 'error',
      })
    }
  }

  const deleteBlog = async (blogId) => {
    try {
      const response = await blogService.remove(blogId)

      if (response.status === 204) {
        // If removed on the server, remove blog from the list of blogs
        setBlogs(blogs.filter((b) => b.id !== blogId))

        notify({
          message: 'Successfully removed blog',
          type: 'success',
        })
      } else {
        notify({
          message: 'Could not delete blog',
          type: 'error',
        })
      }
    } catch (error) {
      notify({
        message: 'Could not delete blog',
        type: 'error',
      })
    }
  }

  return (
    <div>
      <Togglable buttonLabel="Create new blog" ref={blogFormRef}>
        <CreateBlogForm createNewBlog={createNewBlog} />
      </Togglable>
      <div>
        {blogs.map((blog) => (
          <Blog
            key={blog.id}
            blog={blog}
            user={user}
            updateBlog={updateBlog}
            deleteBlog={deleteBlog}
          />
        ))}
      </div>
    </div>
  )
}

Bloglist.propTypes = {
  user: PropTypes.object.isRequired,
  notify: PropTypes.func.isRequired,
}

const CreateBlogForm = ({ createNewBlog }) => {
  const [title, setTitle] = useState('')
  const [author, setAuthor] = useState('')
  const [url, setUrl] = useState('')

  const addNewBlog = (event) => {
    event.preventDefault()
    createNewBlog({
      title,
      author,
      url,
    })

    setTitle('')
    setAuthor('')
    setUrl('')
  }

  return (
    <div>
      <h2>Create new</h2>
      <form id="create-blog-form" onSubmit={addNewBlog}>
        <div>
          Title:{' '}
          <input
            id="title"
            type="text"
            value={title}
            name="Title"
            onChange={({ target }) => setTitle(target.value)}
          />
        </div>
        <div>
          Author:{' '}
          <input
            id="author"
            type="text"
            value={author}
            name="Author"
            onChange={({ target }) => setAuthor(target.value)}
          />
        </div>
        <div>
          URL:{' '}
          <input
            id="url"
            type="text"
            value={url}
            name="url"
            onChange={({ target }) => setUrl(target.value)}
          />
        </div>
        <button type="submit">Create</button>
      </form>
    </div>
  )
}

CreateBlogForm.propTypes = {
  createNewBlog: PropTypes.func.isRequired,
}

const Togglable = React.forwardRef((props, ref) => {
  const [visible, setVisible] = useState(false)

  const hideWhenVisible = { display: visible ? 'none' : '' }
  const ShowWhenVisible = { display: visible ? '' : 'none' }

  const toggleVisibility = () => setVisible(!visible)

  useImperativeHandle(ref, () => {
    return {
      toggleVisibility
    }
  })

  return (
    <div>
      <div style={hideWhenVisible}>
        <button onClick={toggleVisibility}>{props.buttonLabel}</button>
      </div>
      <div style={ShowWhenVisible}>
        {props.children}
        <button onClick={toggleVisibility}>Cancel</button>
      </div>
    </div>
  )
})

Togglable.propTypes = {
  buttonLabel: PropTypes.string.isRequired
}

Togglable.displayName = 'Toggleable'

export { Blog, Bloglist, CreateBlogForm, Header, Notification, LoginForm, Togglable }



