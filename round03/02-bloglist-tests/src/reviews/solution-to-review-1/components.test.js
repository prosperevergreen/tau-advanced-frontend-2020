
import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { render, fireEvent } from '@testing-library/react'

import { Blog, BlogForm } from './components'

// *** ENTER COMMIT SHA OF YOUR REPO IN HERE ***
const commitSHA = '44ac42f'

// *** DO NOT REMOVE OR CHANGE THIS LINE ***
describe(`\nCOMPONENT TESTS ${process.env.SOLUTION || 'your-solution'} [ ${commitSHA} ]\n`, () => {


  describe('Blog', () => {

    let component;
    const blog = {id: 1}
    const user = {name: "test"}
    const updateBlog = 'updateBlog'
    const deleteBlog = 'deleteBlog'

    beforeEach(() => {
      component = render(
        <Blog
        key={blog.id}
        blog={blog}
        user={user}
        updateBlog={updateBlog}
        deleteBlog={deleteBlog}
        />
      );
    });

    test("Blog details aren't shown by default", () => {
      const div = component.container.querySelector(".blogDetails");
      expect(div).toBe(null);
    });
  
    test("Blog details are shown if view button is clicked", () => {
      const button = component.getByText("view");
      fireEvent.click(button);
      const div = component.container.querySelector(".blogDetails");
      expect(div).not.toHaveStyle("display: none");
    });
  })

  describe('BlogForm', () => {

    test('true is true (dummy test)', () => { expect(true).toBe(true) })

  })


})

