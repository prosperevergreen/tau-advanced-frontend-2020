import axios from 'axios'
const blogsUrl = '/api/blogs'
const loginUrl = '/api/login'

const login = async credentials => {
  const response = await axios.post(loginUrl, credentials)
  return response.data
}

let token = null

const setToken = (newToken) => {
  token = `bearer ${newToken}`
}

const clearToken = () => (token = null)

const getAll = async () => {
  const response = await axios.get(blogsUrl)
  return response.data
}

const create = async (newObject) => {
  const config = {
    headers: { Authorization: token },
  }

  const response = await axios.post(blogsUrl, newObject, config)

  return response.data
}

const update = async (blogId, newObject) => {
  const config = {
    headers: { Authorization: token },
  }

  const response = await axios.put(`${blogsUrl}/${blogId}`, newObject, config)

  return response.data
}

const remove = async (blogId) => {
  const config = {
    headers: { Authorization: token },
  }

  const response = await axios.delete(`${blogsUrl}/${blogId}`, config)
  return response
}

export default { login, getAll, create, update, remove, setToken, clearToken }


