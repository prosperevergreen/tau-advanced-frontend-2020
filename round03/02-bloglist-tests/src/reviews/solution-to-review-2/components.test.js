
import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { render, fireEvent } from '@testing-library/react'

import {Blog, BlogForm, CreateNew} from './components'

// *** ENTER COMMIT SHA OF YOUR REPO IN HERE ***
const commitSHA = '0cffbfa'

// *** DO NOT REMOVE OR CHANGE THIS LINE ***
describe(`\nCOMPONENT TESTS ${process.env.SOLUTION || 'your-solution'} [ ${commitSHA} ]\n`, () => {
  let likeCount = 0
  let component
  const blog = {
    id:1,
    title: "test title",
    author: "test author",
    url: "test url",
    likes: 0
  }

  beforeEach(() => {
    component = render(
      <Blog
        blog={blog}
        key={blog.id}
        handleDelete={()=>{}}
        handleLike={()=>{likeCount += 1}}
      />
    )
  })

  test('renders title', () => {

    expect(component.container).toHaveTextContent(
      'test title'
    )
  })

  test('renders author', () => {

    expect(component.container).toHaveTextContent(
      'test author'
    )
  })

  test('does not render url', () => {

    const div = component.container.getElementsByClassName(".toggleable-content")[0]
    expect(div).toHaveStyle('display: none')
  })

  test('after clicking the button, children are displayed', () => {
    const button = component.getByText('show')
    fireEvent.click(button)

    const div = component.container.getElementsByClassName(".toggleable-content")[0]
    expect(div).not.toHaveStyle('display: none')
  })

  test('like button is functioning', () => {
    const button = component.getByText('show')
    fireEvent.click(button)

    const likeButton = component.getByText('Like')
    fireEvent.click(likeButton)
    fireEvent.click(likeButton)
    expect(likeCount).toBe(2)
  })

})

describe(`\nCOMPONENT TESTS2 ${process.env.SOLUTION || 'your-solution'} [ ${commitSHA} ]\n`, () => {
  let component

  const testSubmit = jest.fn()

  beforeEach(() => {
    component = render(
      <BlogForm
        handleSubmit={testSubmit}
      />
    )
  })

  test('check submit values', () => {
    const form = component.container.querySelector('form')
    const author = component.container.querySelector('#author')
    const title = component.container.querySelector('#title')
    const url = component.container.querySelector('#url')

    fireEvent.change(author, {
      target: { value: 'new author' }
    })
    fireEvent.change(title, {
      target: { value: 'new title' }
    })
    fireEvent.change(url, {
      target: { value: 'new url' }
    })
    fireEvent.submit(form)

    expect(testSubmit.mock.calls[3][0]).toHaveProperty('author', 'new author')
    expect(testSubmit.mock.calls[3][0]).toHaveProperty('url', 'new url')
    expect(testSubmit.mock.calls[3][0]).toHaveProperty('title', 'new title')
  })
})

