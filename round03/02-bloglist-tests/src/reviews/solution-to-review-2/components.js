import React, {useState} from 'react'
import PropTypes from 'prop-types'

export const Blog = (props, {key}) => {
  const [blogVisible, setBlogVisible] = useState(false);
  const hideWhenVisible = {display: blogVisible ? 'none' : ''};
  const showWhenVisible = {display: blogVisible ? '' : 'none'};
  const blogStyle = {
    paddingTop: 10,
    paddingLeft: 2,
    border: 'solid',
    borderWidth: 1,
    marginBottom: 5
  };

  return (
    <div style={blogStyle} key={key} className=".blog-header">
      <p>{props.blog.title}</p>
      <p>Author : {props.blog.author}</p>
      <button style={hideWhenVisible} onClick={() => setBlogVisible(true)}>show</button>
      <button style={showWhenVisible} onClick={() => setBlogVisible(false)}>hide</button>
      <div style={showWhenVisible} className=".toggleable-content">
        <p>URL : {props.blog.url}</p>
        <p>Likes : {props.blog.likes}</p>
        <button onClick={() => props.handleLike(props.blog)}>Like</button>
        <button onClick={() => props.handleDelete(props.blog)}>Delete</button>
      </div>
    </div>
  )
};

export const Login = (props) => {
  return (
    <form onSubmit={props.handleLogin}>
      <div>
        username
        <input
          type="text"
          value={props.username}
          name="Username"
          onChange={({target}) => props.setUsername(target.value)}
        />
      </div>
      <div>
        password
        <input
          type="password"
          value={props.password}
          name="Password"
          onChange={({target}) => props.setPassword(target.value)}
        />
      </div>
      <button type="submit">login</button>
    </form>
  )
};

export const Blogs = (props) => {
  props.blogs.sort((a, b) => a.likes < b.likes ? 1 : -1);
  return (
    <div>
      <CreateNew
        handleFormChange={props.handleFormChange}
        newBlog={props.newBlog}
        handleSubmit={props.handleSubmit}
      />
      <h2>blogs</h2>
      {props.blogs.map(blog =>
        <Blog
          key={blog.id}
          blog={blog}
          handleLike={props.handleLike}
          handleDelete={props.handleDelete}
        />
      )}
    </div>
  )
};

export const BlogForm = (props) => {
  const [newBlogVisible, setNewBlogVisible] = useState(false)
  const hideWhenVisible = {display: newBlogVisible ? 'none' : ''};
  const showWhenVisible = {display: newBlogVisible ? '' : 'none'};
  const [newBlog, setNewBlog] = useState({
    title: '',
    author: '',
    url: '',
    likes: 0
  });

  const handleFormChange = (event) => {
    const value = event.target.value;
    setNewBlog({
      ...newBlog,
      [event.target.name]: value
    });
  };

  return (
    <div>
      <div style={hideWhenVisible}>
        <button onClick={() => setNewBlogVisible(true)}>show</button>
      </div>
      <div style={showWhenVisible}>
        <h2>Create new blog</h2>
        <form onSubmit={props.handleSubmit(newBlog)} id="create-new-blog">
          <label htmlFor="title">Title</label>
          <input
            type="text"
            id="title"
            name="title"
            value={newBlog.title}
            onChange={handleFormChange}
          /><br/>
          <label htmlFor="author">Author</label>
          <input
            type="text"
            id="author"
            name="author"
            value={newBlog.author}
            onChange={handleFormChange}
          /><br/>
          <label htmlFor="url">Url</label>
          <input
            type="text"
            id="url"
            name="url"
            value={newBlog.url}
            onChange={handleFormChange}
          /><br/>
          <button type="submit">Create</button>
        </form>
        <button onClick={() => setNewBlogVisible(false)}>cancel</button>
      </div>
    </div>
  )
};

export const Notification = (props) => {
  let styles;
  if (props.error === "true") {
    styles = {
      color: "red"
    }
  } else {
    styles = {
      color: "green"
    }
  }
  return (
    <div style={styles}>
      <p>{props.message}</p>
    </div>
  )
};

Login.propTypes = {
  handleLogin: PropTypes.func.isRequired,
  setUsername: PropTypes.func.isRequired,
  setPassword: PropTypes.func.isRequired,
  username: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired
};