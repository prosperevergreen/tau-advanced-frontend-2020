
import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { render, fireEvent } from '@testing-library/react'

import { Blog, BlogForm } from './components'

// *** ENTER COMMIT SHA OF YOUR REPO IN HERE ***
const commitSHA = 'fb8bf39'

// *** DO NOT REMOVE OR CHANGE THIS LINE ***
describe(`\nCOMPONENT TESTS ${process.env.SOLUTION || 'your-solution'} [ ${commitSHA} ]\n`, () => {


  describe('Blog', () => {

    const blog = {
			author: "John Doe",
			id: 10,
			likes: 2,
			title: "Hello World!!!",
			url: "helloworld.com",
			user: {
				id: 1,
				name: "Ned Flanders",
				passwordHash:
					"$2b$10$7.63CIQ07aDwSGEincfU0ub2eoojzQLUJeOpdfLYrcuFkpDwamkW6",
				username: "ned",
			},
		};

    const mockUpdateBlog = jest.fn()
    const mockDeleteBlog = jest.fn()
    let component;
    beforeEach(() => {
      component = render(
        <Blog blog={blog} updateBlog={mockUpdateBlog} deleteBlog={mockDeleteBlog} />
      )
    })

    test('blog renders the blog\'s title and author only but not url or likes', () => { 
      const blogPost = component.container.querySelector('.blog-post')
      const toggleShow = component.container.querySelector('.blog-post .toggle-show')


      expect(blogPost).toHaveTextContent("Hello World!!! John Doe") 
      expect(toggleShow).toHaveTextContent("view")
      expect(toggleShow).not.toHaveTextContent("hide")
      expect(blogPost).not.toHaveTextContent("likes: 2")
      expect(blogPost).not.toHaveTextContent("helloworld.com")
    })

    test('checks that the blog\'s url and number of likes are shown when the button controlling the shown details has been clicked', () => { 
      const blogPost = component.container.querySelector('.blog-post')
      const toggleShow = component.container.querySelector('.blog-post .toggle-show')

      fireEvent.click(toggleShow)

      expect(toggleShow).toHaveTextContent("hide")
      expect(blogPost).toHaveTextContent("likes 2")
      expect(blogPost).toHaveTextContent("helloworld.com")
      expect(toggleShow).not.toHaveTextContent("view")
    })


    test('ensures that if the like button is clicked twice, the event handler the component received as props is called twice', () => { 
      const toggleShow = component.container.querySelector('.blog-post .toggle-show')
      fireEvent.click(toggleShow)

      const likeButton = component.container.querySelector('.blog-post .likes')
      fireEvent.click(likeButton)
      fireEvent.click(likeButton)

      expect(mockUpdateBlog.mock.calls).toHaveLength(2)
    })
  })


  describe('BlogForm', () => {


    test("check that the form calls the event handler it received as props with the right details when a new blog is created", () => {
			const mockPostBlogHandler = jest.fn();

			const component = render(<BlogForm postBlog={mockPostBlogHandler} />);

      const toggleButton = component.getByText("new note")
      fireEvent.click(toggleButton);

			const formField = component.container.querySelector(".form");
			const titleField = component.container.querySelector("#title");
			const authorField = component.container.querySelector("#author");
			const urlField = component.container.querySelector("#url");

			fireEvent.change(titleField, {
				target: { value: "Hello World!!!" },
			});
			fireEvent.change(authorField, {
				target: { value: "John Doe" },
			});
			fireEvent.change(urlField, {
				target: { value: "www.helloworld.com" },
			});

			fireEvent.submit(formField);

			expect(mockPostBlogHandler.mock.calls).toHaveLength(1);
			expect(mockPostBlogHandler.mock.calls[0][0].title).toBe("Hello World!!!");
			expect(mockPostBlogHandler.mock.calls[0][0].author).toBe("John Doe");
			expect(mockPostBlogHandler.mock.calls[0][0].url).toBe("www.helloworld.com");
			expect(mockPostBlogHandler.mock.calls[0][0].likes).toBe(0);
		});

  })


})

