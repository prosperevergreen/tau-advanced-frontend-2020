import React, { useState, useRef, useImperativeHandle } from 'react'
import PropTypes from 'prop-types'

const Blog = ({ blog, updateBlog, deleteBlog }) => {
  const [showFull, setShowFull] = useState(false)
  const blogStyle = {
    border: 'solid black 2px',
    margin: '5px 0',
    padding: '3px',
  }

  const likeBlog = () => {
    const update = {
      user: blog.user.id,
      likes: blog.likes + 1,
      author: blog.author,
      title: blog.title,
      url: blog.url,
    }
    updateBlog(blog.id, update)
  }

  const removeBlog = () => {
    if (window.confirm(`Remove blog ${blog.title} by ${blog.author}`)) {
      deleteBlog(blog.id)
    }
  }
  return (
    <div style={blogStyle} className="blog-post">
      {blog.title} {blog.author}{' '}
      <button className="toggle-show" onClick={() => setShowFull(!showFull)}>
        {showFull ? 'hide' : 'view'}
      </button>
      {showFull && (
        <>
          <br />
          {blog.url}
          <br />
					likes {blog.likes} <button className="likes" onClick={likeBlog}>like</button>
          <br />
          {blog.user.name}
          <br />
          <button  onClick={removeBlog}>remove</button>
        </>
      )}
    </div>
  )
}

Blog.propTypes = {
  blog: PropTypes.object.isRequired,
  updateBlog: PropTypes.func.isRequired,
  deleteBlog: PropTypes.func.isRequired
}

const BlogForm = ({ postBlog }) => {
  const [author, setAuthor] = useState('')
  const [title, setTitle] = useState('')
  const [url, setUrl] = useState('')
  const newBlogRef = useRef()

  const createBlog = (event) => {
    event.preventDefault()
    postBlog({ author, title, url, likes: 0 })
    setTitle('')
    setAuthor('')
    setUrl('')
    newBlogRef.current.setShow(false)
  }

  return (
    <Togglable buttonLabel="new note" ref={newBlogRef}>
      <h2>create new</h2>
      <form onSubmit={createBlog} className="form">
        <InputField
          label="title"
          type="text"
          value={title}
          setValue={setTitle}
          id="title"
        />
        <InputField
          label="author"
          type="text"
          value={author}
          setValue={setAuthor}
          id="author"
        />
        <InputField label="url" type="text" value={url} setValue={setUrl}  id="url"/>
        <button type="submit">create</button>
      </form>
    </Togglable>
  )
}

BlogForm.propTypes = {
  postBlog: PropTypes.func.isRequired
}

const Blogs = ({ blogs, updateBlog, deleteBlog }) => {

  return (
    <div>
      {blogs.map((blog) => (
        <Blog
          key={blog.id}
          blog={blog}
          updateBlog={updateBlog}
          deleteBlog={deleteBlog}
        />
      ))}
    </div>
  )
}

Blogs.propTypes = {
  blogs: PropTypes.array.isRequired,
  updateBlog: PropTypes.func.isRequired,
  deleteBlog: PropTypes.func.isRequired
}

const InputField = ({ label, type, value, setValue, id }) => (
  <div>
    {label}:
    <input
      type={type}
      value={value}
      name={label}
      onChange={({ target }) => setValue(target.value)}
      id={id}
      required
    />
  </div>
)

InputField.propTypes = {
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  setValue: PropTypes.func.isRequired
}

const LoginForm = ({ handleLogin, notification }) => {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')

  const doLogin = async (event) => {
    event.preventDefault()
    handleLogin({ username, password })
    setUsername('')
    setPassword('')
  }

  return (
    <form onSubmit={doLogin}>
      <h2>log in to application</h2>
      {notification && <Notification notification={notification} />}
      <InputField
        label="username"
        type="text"
        value={username}
        setValue={setUsername}
      />
      <InputField
        label="password"
        type="password"
        value={password}
        setValue={setPassword}
      />
      <button type="submit">login</button>
    </form>
  )
}

LoginForm.propTypes = {
  handleLogin: PropTypes.func.isRequired,
  notification: PropTypes.object,
}

const BlogSection = ({
  blogs,
  user,
  logout,
  postBlog,
  notification,
  updateBlog,
  deleteBlog,
}) => {
  return (
    <div>
      <h2>blogs</h2>
      {notification && <Notification notification={notification} />}
      <p>
        {user} logged in <button onClick={logout}>logout</button>
      </p>
      <div>
        <BlogForm postBlog={postBlog} />
        <Blogs blogs={blogs} updateBlog={updateBlog} deleteBlog={deleteBlog} />
      </div>
    </div>
  )
}

BlogSection.propTypes = {
  blogs: PropTypes.array.isRequired,
  user: PropTypes.string.isRequired,
  logout: PropTypes.func.isRequired,
  postBlog: PropTypes.func.isRequired,
  notification: PropTypes.any,
  updateBlog: PropTypes.func.isRequired,
  deleteBlog: PropTypes.func.isRequired,
}

const Notification = ({ notification }) => {
  const color = notification.type === 'error' ? 'red' : 'green'
  const notificationStyle = {
    backgroundColor: 'lightgray',
    fontSize: '20px',
    borderStyle: 'solid',
    borderRadius: '5px',
    padding: '10px',
    marginBottom: '10px',
    color: color,
    borderColor: color,
  }

  if (notification.message === '') {
    return null
  }

  return <div style={notificationStyle}>{notification.message}</div>
}

Notification.propTypes = {
  notification: PropTypes.any
}

const Togglable = React.forwardRef(({ children, buttonLabel }, ref) => {
  const [show, setShow] = useState(false)

  useImperativeHandle(ref, () => {
    return { setShow }
  })

  return (
    <div>
      {show ? (
        <>
          {children}
          <button onClick={() => setShow(!show)}>cancel</button>
        </>
      ) : (
        <button onClick={() => setShow(!show)}>{buttonLabel}</button>
      )}
    </div>
  )
})

Togglable.displayName = 'Togglable'
Togglable.propTypes = {
  buttonLabel: PropTypes.string.isRequired,
}

export {Blog, BlogForm, BlogSection, LoginForm }
