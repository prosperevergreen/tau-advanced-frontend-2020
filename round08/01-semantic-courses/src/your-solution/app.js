import { Container, Divider, Header} from "semantic-ui-react";

import { Course } from "./components";
import courses from "./courses-data";
import "semantic-ui-css/semantic.min.css";

// ** ENTER COMMIT SHA OF YOUR REPO IN HERE **
export const commitSHA = "c86549f";

export const App = () => {
	return (
		<Container>
			<Divider hidden />
			<Header as="h1" disabled textAlign="center">
				Web development curriculum
			</Header>
			<Divider />
			{courses.map((course) => (
				<Course key={course.id} course={course} />
			))}
		</Container>
	);
};
