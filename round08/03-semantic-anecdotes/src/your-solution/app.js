import React from "react";
import { AnecdoteList, Notification, AnecdoteHeader } from "./components";
import "semantic-ui-css/semantic.min.css";
import { Divider, Container } from "semantic-ui-react";
export { default as store } from "../redux/store";

// ** enter commit sha of your repository in here **
export const commitSHA = "c3553f7";

export const App = () => {
	return (
		<Container>
			<Divider hidden/>
			<AnecdoteHeader />
			<Notification />
			<AnecdoteList />
			<Divider hidden/>
		</Container>
	);
};
