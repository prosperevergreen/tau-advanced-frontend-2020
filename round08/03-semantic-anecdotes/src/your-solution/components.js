import React from "react";
import { useSelector, useDispatch } from "react-redux";

import { addAnecdote, voteAnecdote } from "../redux/reducer-anecdote";
import {
	setNotification,
	clearNotification,
} from "../redux/reducer-notification";
import { setFilter } from "../redux/reducer-filter";

import {
	Icon,
	Card,
	Input,
	Button,
	Menu,
	Modal,
	TextArea,
	Message,
} from "semantic-ui-react";

//
// Notification
//

export const Notification = () => {
	const notification = useSelector((state) => state.notification);
	if (notification === "") return null;

	const [you, did, ...that] = notification.split(" ");
	return <Message header={`${you} ${did}`} content={that.join(" ")} />;
};

//
// AnecdoteForm
//

export const AnecdoteForm = () => {
	const dispatch = useDispatch();
	const [open, setOpen] = React.useState(false);
	const [content, setContent] = React.useState("");

	const handleSubmit = () => {
		if (content === "") return;
		dispatch(addAnecdote(content));
		dispatch(setNotification(`You added "${content}"`));
		setTimeout(() => dispatch(clearNotification()), 5000);
		setOpen(false);
		setContent("");
	};

	const handleCancel = () => {
		setOpen(false);
		setContent("");
	};

	const textAreaStyle = { width: "100%", padding: 8 };

	return (
		<Modal
			onClose={() => setOpen(false)}
			onOpen={() => setOpen(true)}
			open={open}
			trigger={
				<Button basic color="green">
					New
				</Button>
			}
		>
			<Modal.Header>New Anecdote</Modal.Header>
			<Modal.Content>
				<TextArea
					placeholder="Enter your anecdote content here..."
					name="content"
					value={content}
					onChange={({ target }) => setContent(target.value)}
					style={textAreaStyle}
					rows={5}
				/>
			</Modal.Content>
			<Modal.Actions>
				<Button basic onClick={handleCancel}>
					<Icon name="x" />
					Cancel
				</Button>
				<Button
					icon="checkmark"
					basic
					color="green"
					onClick={handleSubmit}
					disabled={content === ""}
					content="Create"
				/>
			</Modal.Actions>
		</Modal>
	);
};

//
// AnecdoteList
//

export const AnecdoteList = () => {
	const filter = useSelector((state) => state.filter).toLowerCase();

	const anecdotes = useSelector((state) => state.anecdotes).filter((anecdote) =>
		anecdote.content.toLowerCase().includes(filter)
	);

	const dispatch = useDispatch();

	const notify = (anecdote) => {
		dispatch(setNotification(`You voted "${anecdote}"`));
		setTimeout(() => dispatch(clearNotification()), 5000);
	};

	const handleVoteClick = (id, anecdote) => () => {
		dispatch(voteAnecdote(id));
		notify(anecdote);
	};

	return anecdotes.map((anecdote) => (
		<Card fluid key={anecdote.id}>
			<Card.Content description={anecdote.content} />
			<Card.Content extra>{anecdote.votes} Votes</Card.Content>
			<Card.Content extra>
				<Button
					basic
					color="green"
					onClick={handleVoteClick(anecdote.id, anecdote.content)}
				>
					Vote
				</Button>
			</Card.Content>
		</Card>
	));
};

//
// Filter
//

const Filter = () => {
	const dispatch = useDispatch();

	const handleChange = (event) => {
		dispatch(setFilter(event.target.value));
	};

	return (
		<Input onChange={handleChange} icon="search" placeholder="Search..." />
	);
};

export const AnecdoteHeader = () => {
	return (
		<Menu borderless>
			<Menu.Item header>Anecdotes</Menu.Item>
			<Menu.Item>
				<AnecdoteForm />
			</Menu.Item>
			<Menu.Item position="right">
				<Filter />
			</Menu.Item>
		</Menu>
	);
};
