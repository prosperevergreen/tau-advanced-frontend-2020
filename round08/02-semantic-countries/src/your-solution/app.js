import React, { useState, useEffect } from "react";
import axios from "axios";
import { Filter, Countries } from "./components";
import "semantic-ui-css/semantic.min.css";
import {
	Divider,
	Grid,
	Header,
	Icon,
	Container,
	Segment,
} from "semantic-ui-react";

// *** ENTER COMMIT SHA OF YOUR REPO IN HERE
export const commitSHA = "e23b61f";

export const App = () => {
	const [countries, setCountries] = useState([]);
	const [filter, setFilter] = useState("");

	useEffect(() => {
		axios.get("https://restcountries.eu/rest/v2/all").then((response) => {
			setCountries(response.data);
		});
	}, []);

	const handleFilterChange = ({ target }) => {
		setFilter(target.value);
	};

	const handleCountryClick = (country) => {
		setFilter(country);
	};

	const countriesToShow = filter.length
		? countries.filter((country) => country.name.match(new RegExp(filter, "i")))
		: countries;

	return (
		<Container>
      <Divider hidden />
			<Segment placeholder>
				<Grid columns={2} stackable textAlign="center">
					<Grid.Row verticalAlign="middle">
						<Grid.Column>
							<Header icon>
								<Icon name="search" />
								Find Country
							</Header>
							<Divider className="hidden"></Divider>
							<Filter value={filter} handleChange={handleFilterChange} />
						</Grid.Column>
						<Grid.Column>
							<Countries
								countries={countriesToShow}
								handleCountryClick={handleCountryClick}
								filter={filter}
							/>
						</Grid.Column>
					</Grid.Row>
				</Grid>
			</Segment>
		</Container>
	);
};
