import {
	Header,
	Segment,
	Input,
	Message,
	Image,
	Button,
	Container,
} from "semantic-ui-react";

export const Filter = ({ value, handleChange }) => (
	<Input
		icon={{ name: "search", circular: true, link: true }}
		placeholder="Search..."
		value={value}
		onChange={handleChange}
	/>
);

export const Countries = ({ countries, handleCountryClick, filter }) => {
	if (filter === "") return <Message>Enter Search criteria</Message>;
	if (countries.length > 10) {
		return <Message>Too many matches, specify another filter</Message>;
	}

	if (countries.length === 1) {
		return <CountryDetails country={countries[0]} />;
	}

	return (
		<Button.Group basic vertical>
			{countries.map((country) => (
				<CountryName
					key={country.alpha2Code}
					country={country}
					handleClick={handleCountryClick}
				/>
			))}
		</Button.Group>
	);
};

const CountryDetails = ({ country }) => (
	<Segment>
		<Container textAlign="left">
			<Header as="h2" textAlign="center">
				<Image src={country.flag} size="mini" /> {country.name}
			</Header>
			<Header as="h5" textAlign="center" style={{ margin: 0 }}>
				Capital
			</Header>
			<Segment>{country.capital}</Segment>
			<Header as="h5" textAlign="center" style={{ margin: 0 }}>
				Population
			</Header>
			<Segment>{country.population}</Segment>
			<Header as="h5" textAlign="center" style={{ margin: 0 }}>
				Languages
			</Header>
			<Segment>
				{country.languages.map((language) => language.name).join(", ")}
			</Segment>
		</Container>
	</Segment>
);

const CountryName = ({ country, handleClick }) => (
	<Button secondary onClick={() => handleClick(country.name)}>
		{country.name}
	</Button>
);
