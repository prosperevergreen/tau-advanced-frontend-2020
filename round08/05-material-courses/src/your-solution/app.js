import { Course } from "./components";
import courses from "./courses-data";
import { Container, Typography, Divider, Box } from "@material-ui/core";

// ** ENTER COMMIT SHA OF YOUR REPO IN HERE **
export const commitSHA = "1158add";

export const App = () => {
	return (
		<Container>
			<Box my={4}>
				<Typography variant="h4" gutterBottom align="center">
					Web development curriculum
				</Typography>
        <Divider/>
				{courses.map((course) => (
					<Course key={course.id} course={course} />
				))}
			</Box>
		</Container>
	);
};
