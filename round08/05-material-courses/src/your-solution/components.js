import {
	Paper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableFooter,
	TableHead,
	TableRow,
	Typography,
	Box,
} from "@material-ui/core";

import { Code, DeveloperMode } from "@material-ui/icons";

const CourseHeader = () => (
	<TableRow>
		<TableCell>Part</TableCell>
		<TableCell align="right">Exercises</TableCell>
	</TableRow>
);

const CourseTitle = ({ course, icon }) => {
	const setIcon = (iconName) => {
		switch (iconName) {
			case "code":
				return <Code />;
			case "developer mode":
				return <DeveloperMode />;
			default:
				return null;
		}
	};

	return <Typography gutterBottom variant="h5">{setIcon(icon)} {course}</Typography>;
};

const Content = ({ parts }) => {
	return (
		<>
			{parts.map((part) => (
				<Part key={part.id} part={part} />
			))}
		</>
	);
};

const Part = ({ part }) => (
	<TableRow>
		<TableCell component="th" scope="row">
			{part.name}
		</TableCell>
		<TableCell align="right">{part.exercises}</TableCell>
	</TableRow>
);

const Total = ({ parts }) => {
	const total = parts.reduce((acc, cur) => acc + cur.exercises, 0);
	return (
		<TableRow>
			<TableCell>Exercises total</TableCell>
			<TableCell align="right">{total}</TableCell>
		</TableRow>
	);
};

export const Course = ({ course }) => {
	return (
		<Box my={4}>
			<CourseTitle course={course.name} icon={course.icon} />
			<TableContainer component={Paper}>
				<Table aria-label="Course details">
					<TableHead>
						<CourseHeader />
					</TableHead>
					<TableBody>
						<Content parts={course.parts} />
					</TableBody>
					<TableFooter>
						<Total parts={course.parts} />
					</TableFooter>
				</Table>
			</TableContainer>
		</Box>
	);
};
